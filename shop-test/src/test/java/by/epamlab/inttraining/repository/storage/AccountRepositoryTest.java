package by.epamlab.inttraining.repository.storage;

import by.epamlab.inttraining.beans.Role;
import by.epamlab.inttraining.configuration.JPAConfig;
import by.epamlab.inttraining.entity.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfig.class})
@WebAppConfiguration
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void findByLoginAndPassword() {
        Account account = accountRepository.findByLoginAndPassword("tempuser", "tempuser");
        assertEquals("tempuser", account.getLogin());
    }

    @Test
    public void findById() {
        Account account = accountRepository.findById(1);
        assertEquals("admin", account.getLogin());
    }

    @Test
    public void saveUser() {
        Account account = new Account();
        account.setLogin("newUser");
        account.setPassword("12345");
        account.setRole(Role.USER.name());
        account.setEmail("temp@temp.com");
        accountRepository.save(account);
        Account newUser = accountRepository.findByLoginAndPassword("newUser", "12345");
        assertEquals("newUser", newUser.getLogin());

    }

}