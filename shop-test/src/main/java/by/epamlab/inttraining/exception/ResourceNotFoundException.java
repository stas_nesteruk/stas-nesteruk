package by.epamlab.inttraining.exception;

import javax.servlet.http.HttpServletResponse;

public class ResourceNotFoundException extends AbstractException {
    private static final long serialVersionUID = 1205071265012663055L;

    public ResourceNotFoundException(String msg) {
        super(msg, HttpServletResponse.SC_NOT_FOUND);
    }
}
