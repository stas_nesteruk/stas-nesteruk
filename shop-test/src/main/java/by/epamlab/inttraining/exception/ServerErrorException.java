package by.epamlab.inttraining.exception;

import javax.servlet.http.HttpServletResponse;

public class ServerErrorException extends AbstractException {
    private static final long serialVersionUID = 7358651852621812080L;

    public ServerErrorException(String message) {
        super(message, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

    public ServerErrorException(String message, Throwable cause) {
        super(message, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
}
