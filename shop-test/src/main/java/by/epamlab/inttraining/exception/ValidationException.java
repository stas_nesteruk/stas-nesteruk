package by.epamlab.inttraining.exception;

import javax.servlet.http.HttpServletResponse;

public class ValidationException extends AbstractException {
    private static final long serialVersionUID = -8666091003508790319L;

    public ValidationException(String message) {
        super(message, HttpServletResponse.SC_BAD_REQUEST);
    }
}
