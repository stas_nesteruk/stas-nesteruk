package by.epamlab.inttraining.exception;

public class AbstractException extends IllegalArgumentException {
    private static final long serialVersionUID = -4253812654638965087L;
    private final int errorCode;

    public AbstractException(int errorCode) {
        this.errorCode = errorCode;
    }

    public AbstractException(String s, int errorCode) {
        super(s);
        this.errorCode = errorCode;
    }

    public AbstractException(String message, Throwable cause, int errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public AbstractException(Throwable cause, int errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
