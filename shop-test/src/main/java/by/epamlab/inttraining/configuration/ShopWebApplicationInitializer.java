package by.epamlab.inttraining.configuration;

import by.epamlab.inttraining.filter.AuthenticationFilter;
import by.epamlab.inttraining.filter.ShopFilter;
import org.h2.server.web.WebServlet;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

public class ShopWebApplicationInitializer implements WebApplicationInitializer {
    private static final String[] MAPPING_URLS_AUTHENTICATION = {
            "/products",
            "/edit",
            "/add-product",
            "/delete",
            "/edit-product",
            "/order",
            "/my-orders",
            "/search",
            "/product/*",
            "/products/*",
            "/shopping-cart",
            "/product/remove/cart"
    };
    private static final int SESSION_TIMEOUT = 3 * 60;

    @Override
    public void onStartup(ServletContext servletContext) {
        WebApplicationContext webApplicationContext = createWebApplicationContext(servletContext);
        servletContext.addFilter("ShopFilter", ShopFilter.class)
                .addMappingForUrlPatterns(null, false, "/*");
        servletContext.addFilter("AuthenticationFilter", AuthenticationFilter.class)
                .addMappingForUrlPatterns(null, false, MAPPING_URLS_AUTHENTICATION);
        servletContext.setSessionTimeout(SESSION_TIMEOUT);
        registerSpringMVCDispatcherServlet(servletContext, webApplicationContext);
        ServletRegistration.Dynamic servlet = servletContext.addServlet(
                "h2-console", new WebServlet());
        servlet.setLoadOnStartup(2);
        servlet.addMapping("/console/*");
    }

    private WebApplicationContext createWebApplicationContext(ServletContext servletContext) {
        AnnotationConfigWebApplicationContext configWebApplicationContext = new AnnotationConfigWebApplicationContext();
        configWebApplicationContext.scan("by.epamlab.inttraining.configuration");
        configWebApplicationContext.setServletContext(servletContext);
        return configWebApplicationContext;
    }

    private void registerSpringMVCDispatcherServlet(ServletContext servletContext, WebApplicationContext context) {
        ServletRegistration.Dynamic registration = servletContext.addServlet("dispatcher", new DispatcherServlet(context));
        registration.setLoadOnStartup(1);
        registration.addMapping("/");
    }
}
