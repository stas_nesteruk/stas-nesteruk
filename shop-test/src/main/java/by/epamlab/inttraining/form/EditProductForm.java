package by.epamlab.inttraining.form;

import org.springframework.web.multipart.MultipartFile;

public class EditProductForm extends BaseProductForm {
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
