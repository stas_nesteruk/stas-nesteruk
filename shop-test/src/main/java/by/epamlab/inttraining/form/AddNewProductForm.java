package by.epamlab.inttraining.form;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


public class AddNewProductForm extends BaseProductForm {
    @Valid
    @NotNull(message = "File is required")
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
