package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Product;
import by.epamlab.inttraining.repository.storage.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public long countProductByNameContainsOrDescriptionContains(String query, String query2) {
        return productRepository.countProductByNameContainsOrDescriptionContains(query, query2);
    }

    @Override
    public List<Product> findProductByNameContainsOrDescriptionContains(String query, String query2) {
        return productRepository.findProductByNameContainsOrDescriptionContains(query, query2);
    }

    @Override
    public Page<Product> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Override
    public List<Product> findAll(Sort sort) {
        return productRepository.findAll(sort);
    }

    @Override
    public Product findProductById(int id) {
        return productRepository.findProductById(id);
    }

    @Override
    public List<Product> findProductByCategoryUrl(String url) {
        return productRepository.findProductByCategory_Url(url);
    }

    @Override
    public void save(Product product) {
        productRepository.save(product);
    }

    @Override
    public void delete(Product product) {
        productRepository.delete(product);
    }
}
