package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Producer;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface ProducerService {
    Producer findByName(String name);

    List<Producer> findAll(Sort sort);
}
