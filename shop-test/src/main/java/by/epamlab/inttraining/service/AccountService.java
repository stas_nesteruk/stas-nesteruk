package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Account;

public interface AccountService {
    Account findByLoginAndPassword(String login, String password);

    Account findById(int id);

    void save(Account account);
}
