package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface ProductService {
    long countProductByNameContainsOrDescriptionContains(String query, String query2);

    List<Product> findProductByNameContainsOrDescriptionContains(String query, String query2);

    Page<Product> findAll(Pageable pageable);

    List<Product> findAll(Sort sort);

    Product findProductById(int id);

    List<Product> findProductByCategoryUrl(String url);

    void save(Product product);

    void delete(Product product);
}
