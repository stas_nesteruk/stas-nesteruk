package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Category;
import by.epamlab.inttraining.repository.storage.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll(Sort sort) {
        return categoryRepository.findAll(sort);
    }

    @Override
    public Category findByName(String name) {
        return categoryRepository.findByName(name);
    }
}
