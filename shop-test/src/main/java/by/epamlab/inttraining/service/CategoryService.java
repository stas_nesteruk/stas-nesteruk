package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Category;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface CategoryService {
    List<Category> findAll(Sort sort);

    Category findByName(String name);
}
