package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Order;
import by.epamlab.inttraining.entity.OrderItem;
import by.epamlab.inttraining.repository.storage.OrderItemRepository;
import by.epamlab.inttraining.repository.storage.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;

    @Override
    public Order findById(int id) {
        return orderRepository.findById(id);
    }

    @Override
    public Order findByAccount_IdAndCreated(int id, Timestamp created) {
        return orderRepository.findByAccount_IdAndCreated(id, created);
    }

    @Override
    public List<Order> findByAccountId(int id) {
        return orderRepository.findByAccount_Id(id);
    }

    @Override
    public void save(OrderItem orderItem) {
        orderItemRepository.save(orderItem);
    }

    @Override
    public void save(Order order) {
        orderRepository.save(order);
    }
}
