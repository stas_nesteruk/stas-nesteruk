package by.epamlab.inttraining.service;

public interface CurrentUser {
    Integer getId();

    String getDescription();

    String getRole();
}
