package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Order;
import by.epamlab.inttraining.entity.OrderItem;

import java.sql.Timestamp;
import java.util.List;

public interface OrderService {
    Order findById(int id);

    Order findByAccount_IdAndCreated(int id, Timestamp created);

    List<Order> findByAccountId(int id);

    void save(OrderItem orderItem);

    void save(Order order);
}
