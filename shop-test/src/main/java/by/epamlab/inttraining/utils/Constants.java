package by.epamlab.inttraining.utils;

public class Constants {
    public static final int MAX_PRODUCT_COUNT_PER_PAGE = 12;
    public static final String CURRENT_SHOPPING_CART = "CURRENT_SHOPPING_CART";
    public static final String CURRENT_USER = "CURRENT_USER";
    public static final String EDIT_MODE = "EDIT_MODE";
}
