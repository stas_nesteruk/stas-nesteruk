package by.epamlab.inttraining.utils;

import by.epamlab.inttraining.service.CurrentUser;

import javax.servlet.http.HttpServletRequest;

public class SessionUtils {
    private SessionUtils() {
    }

    public static void setEditMode(HttpServletRequest req, String status) {
        req.getSession().setAttribute(Constants.EDIT_MODE, status);
    }

    public static String getEditMode(HttpServletRequest req) {
        return (String) req.getSession().getAttribute(Constants.EDIT_MODE);
    }

    public static void removeEditMode(HttpServletRequest req) {
        req.getSession().removeAttribute(Constants.EDIT_MODE);
    }

    public static void clearCurrentShoppingCart(HttpServletRequest req) {
        req.getSession().removeAttribute(Constants.CURRENT_SHOPPING_CART);
    }

    public static boolean isEditMode(HttpServletRequest req) {
        return getEditMode(req) != null;
    }

    public static CurrentUser getCurrentUser(HttpServletRequest req) {
        return (CurrentUser) req.getSession().getAttribute(Constants.CURRENT_USER);
    }

    public static void setCurrentUser(HttpServletRequest req, CurrentUser currentUser) {
        req.getSession().setAttribute(Constants.CURRENT_USER, currentUser);
    }

    public static boolean isCreatedCurrentUser(HttpServletRequest req) {
        return getCurrentUser(req) != null;
    }

}
