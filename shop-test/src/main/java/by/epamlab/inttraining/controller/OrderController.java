package by.epamlab.inttraining.controller;

import by.epamlab.inttraining.beans.OrderDetails;
import by.epamlab.inttraining.beans.ShoppingCart;
import by.epamlab.inttraining.beans.ShoppingCartItem;
import by.epamlab.inttraining.entity.Account;
import by.epamlab.inttraining.entity.Order;
import by.epamlab.inttraining.entity.OrderItem;
import by.epamlab.inttraining.entity.Product;
import by.epamlab.inttraining.exception.AccessDeniedException;
import by.epamlab.inttraining.exception.ResourceNotFoundException;
import by.epamlab.inttraining.exception.ServerErrorException;
import by.epamlab.inttraining.service.*;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Controller
public class OrderController {
    private static final Logger LOGGER = Logger.getLogger(OrderController.class);
    public static final String ORDER_SUCCESS = "ORDER_SUCCESS";
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public String addOrder(HttpServletRequest req) {
        ShoppingCart shoppingCart = (ShoppingCart)
                req.getSession().getAttribute(Constants.CURRENT_SHOPPING_CART);
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
            req.getSession().setAttribute(Constants.CURRENT_SHOPPING_CART, shoppingCart);
        }
        CurrentUser currentUser = (CurrentUser) req.getSession().getAttribute(Constants.CURRENT_USER);
        Account account = accountService.findById(currentUser.getId());
        Timestamp created = new Timestamp(System.currentTimeMillis());
        Order order = new Order();
        order.setAccount(account);
        order.setCreated(created);
        orderService.save(order);
        Order createdOrder = orderService.findByAccount_IdAndCreated(account.getId(), created);
        for (ShoppingCartItem item : shoppingCart.getItems()) {
            OrderItem orderItem = new OrderItem();
            orderItem.setIdProduct(item.getProduct().getId());
            orderItem.setCount(item.getCount());
            orderItem.setOrder(order);
            orderService.save(orderItem);
        }
        updateCountAfterMakeOrder(shoppingCart);

        LOGGER.info(String.format("User: %s, %s; make order: id: %s, cost: %s",
                account.getId(),
                account.getLogin(),
                order.getId(),
                shoppingCart.getTotalCost()));
        req.getSession().removeAttribute(Constants.CURRENT_SHOPPING_CART);
        req.getSession().setAttribute(ORDER_SUCCESS, "The order is waiting for confirmation.");
        return "redirect: /order?id=" + createdOrder.getId();
    }

    private void updateCountAfterMakeOrder(ShoppingCart shoppingCart) {
        for (ShoppingCartItem item : shoppingCart.getItems()) {
            Product product = item.getProduct();
            if (product.getCountStock() >= item.getCount()) {
                product.setCountStock(product.getCountStock() - item.getCount());
                productService.save(product);
            } else {
                throw new ServerErrorException("Cant execute sql query not enough count");
            }
        }
    }

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public String getOrder(HttpServletRequest req, Model model) {
        String msq = (String) req.getSession().getAttribute(ORDER_SUCCESS);
        req.getSession().removeAttribute(ORDER_SUCCESS);
        req.setAttribute(ORDER_SUCCESS, msq);
        CurrentUser currentUser = (CurrentUser) req.getSession().getAttribute(Constants.CURRENT_USER);
        Account account = accountService.findById(currentUser.getId());
        int id = Integer.parseInt(req.getParameter("id"));
        Order order = orderService.findById(id);
        if (order == null) {
            throw new ResourceNotFoundException("Order not found by id: " + id);
        }
        if (order.getAccount().getId() != account.getId()) {
            throw new AccessDeniedException("Account by id: " + currentUser.getId() +
                    " dont have order by id" + id);
        }
        List<OrderItem> orderItems = order.getOrderItems();
        List<OrderDetails> orderDetailsList = new ArrayList<>();
        BigDecimal totalCost = BigDecimal.ZERO;
        for (OrderItem x : orderItems) {
            OrderDetails orderDetails = new OrderDetails();
            Product product = productService.findProductById(x.getIdProduct());
            orderDetails.setCount(x.getCount());
            orderDetails.setProduct(product);
            orderDetailsList.add(orderDetails);
            totalCost = totalCost.add(product.getPrice().multiply(BigDecimal.valueOf(x.getCount())));
        }
        model.addAttribute("categories", categoryService.findAll(new Sort("name")));
        model.addAttribute("order", orderDetailsList);
        model.addAttribute("totalCost", totalCost);
        model.addAttribute("currentPage", "page/orders.jsp");
        return "page-template";
    }

    @RequestMapping(value = "/my-orders", method = RequestMethod.GET)
    public String getUserOrders(HttpServletRequest req, Model model) {
        CurrentUser user = (CurrentUser) req.getSession().getAttribute(Constants.CURRENT_USER);
        List<Order> orders = orderService.findByAccountId(user.getId());
        model.addAttribute("categories", categoryService.findAll(new Sort("name")));
        model.addAttribute("orders", orders);
        model.addAttribute("currentPage", "page/user-orders.jsp");
        return "page-template";
    }

}
