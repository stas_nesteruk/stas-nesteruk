package by.epamlab.inttraining.controller;

import by.epamlab.inttraining.beans.Role;
import by.epamlab.inttraining.entity.Product;
import by.epamlab.inttraining.exception.AccessDeniedException;
import by.epamlab.inttraining.form.BaseProductForm;
import by.epamlab.inttraining.service.CategoryService;
import by.epamlab.inttraining.service.CurrentUser;
import by.epamlab.inttraining.service.ProducerService;
import by.epamlab.inttraining.service.ProductService;
import by.epamlab.inttraining.utils.Constants;
import by.epamlab.inttraining.utils.SessionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;

@Controller
public class EditController {
    private static final Logger LOGGER = Logger.getLogger(EditController.class);
    @Autowired
    private ProducerService producerService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;
    @Autowired
    ServletContext context;

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String switchEditMode(HttpServletRequest req, Model model) {
        CurrentUser user = (CurrentUser) req.getSession().getAttribute(Constants.CURRENT_USER);
        checkRole(user);
        if (!SessionUtils.isEditMode(req)) {
            SessionUtils.setEditMode(req, "ON");
        } else {
            switch (SessionUtils.getEditMode(req)) {
                case "ON":
                    SessionUtils.setEditMode(req, "OFF");
                    break;
                case "OFF":
                    SessionUtils.setEditMode(req, "ON");
                    break;
            }
        }
        return "redirect: /products";
    }

    private void checkRole(CurrentUser user) {
        if (!user.getRole().equalsIgnoreCase(Role.ADMIN.name())) {
            throw new AccessDeniedException("Your account don't have enough permissions to edit mode");
        }
    }

    @RequestMapping(value = "/add-product", method = RequestMethod.GET)
    public String getAddProductPage(HttpServletRequest req, Model model) {
        CurrentUser user = (CurrentUser) req.getSession().getAttribute(Constants.CURRENT_USER);
        checkRole(user);
        model.addAttribute("categories", categoryService.findAll(new Sort("name")));
        model.addAttribute("producers", producerService.findAll(new Sort("name")));
        model.addAttribute("currentPage", "page/addNewProduct.jsp");
        model.addAttribute("addNewProductForm", new BaseProductForm());
        return "page-template";
    }

    @RequestMapping(value = "/add-product", method = RequestMethod.POST)
    public String saveNewProduct(@Valid @ModelAttribute("addNewProductForm") BaseProductForm form,
                                 BindingResult bindingResult,
                                 HttpServletRequest req) throws IOException {
        if (form.getFile().isEmpty()) {
            bindingResult.rejectValue("file", "NotNull");
        }
        if (bindingResult.hasErrors()) {
            req.setAttribute("categories", categoryService.findAll(new Sort("name")));
            req.setAttribute("producers", producerService.findAll(new Sort("name")));
            req.setAttribute("currentPage", "page/addNewProduct.jsp");
            return "page-template";
        }
        Product product = new Product();
        MultipartFile file = form.getFile();
        String root = context.getRealPath("/");
        File path = new File(root + "media/" + File.separator + file.getOriginalFilename());
        FileCopyUtils.copy(form.getFile().getBytes(), path);

        product.setImageLink("/media/" + file.getOriginalFilename());
        product.setName(form.getNameProduct());
        product.setCategory(categoryService.findByName(form.getNameCategory()));
        product.setProducer(producerService.findByName(form.getNameProducer()));
        product.setCountStock(form.getNameCount());
        product.setDescription(form.getNameDescription());
        product.setPrice(form.getNamePrice());

        productService.save(product);
        loggingAction("Added", product);
        return "redirect: /products";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteProduct(HttpServletRequest req) {
        CurrentUser user = (CurrentUser) req.getSession().getAttribute(Constants.CURRENT_USER);
        checkRole(user);
        int id = Integer.parseInt(req.getParameter("id"));
        Product product = productService.findProductById(id);
        productService.delete(product);
        loggingAction("Delete", product);
        return "redirect: /products";
    }

    @RequestMapping(value = "/edit-product", method = RequestMethod.GET)
    public String getEditProductPage(HttpServletRequest req, Model model) {
        CurrentUser user = (CurrentUser) req.getSession().getAttribute(Constants.CURRENT_USER);
        checkRole(user);
        int id = Integer.parseInt(req.getParameter("id"));
        Product product = productService.findProductById(id);
        BaseProductForm form = new BaseProductForm();
        form.setNameDescription(product.getDescription());
        form.setNameCount(product.getCountStock());
        form.setNameProducer(product.getProducer().getName());
        form.setNameCategory(product.getCategory().getName());
        form.setNameProduct(product.getName());
        form.setNamePrice(product.getPrice());


        model.addAttribute("categories", categoryService.findAll(new Sort("name")));
        model.addAttribute("producers", producerService.findAll(new Sort("name")));
        req.getSession().setAttribute("ID_PRODUCT", id);
        req.getSession().setAttribute("imageLink", product.getImageLink());
        model.addAttribute("currentPage", "page/editProduct.jsp");
        model.addAttribute("editProductForm", form);
        return "page-template";
    }

    @RequestMapping(value = "/edit-product", method = RequestMethod.POST)
    public String saveEditProduct(@Valid @ModelAttribute("editProductForm") BaseProductForm form,
                                  BindingResult bindingResult,
                                  HttpServletRequest req) throws IOException {
        if (bindingResult.hasErrors()) {
            req.setAttribute("categories", categoryService.findAll(new Sort("name")));
            req.setAttribute("producers", producerService.findAll(new Sort("name")));
            req.setAttribute("currentPage", "page/editProduct.jsp");
            return "page-template";
        }
        int id = (int) req.getSession().getAttribute("ID_PRODUCT");
        Product product = new Product();
        boolean isImage = false;
        if (!form.getFile().isEmpty()) {
            MultipartFile file = form.getFile();
            String root = context.getRealPath("/");
            File path = new File(root + "media/" + File.separator + file.getOriginalFilename());
            FileCopyUtils.copy(form.getFile().getBytes(), path);
            product.setImageLink("/media/" + file.getOriginalFilename());
            isImage = true;
        }
        if (!isImage) {
            product.setImageLink(productService.findProductById(id).getImageLink());
        }
        product.setId(id);
        product.setName(form.getNameProduct());
        product.setCategory(categoryService.findByName(form.getNameCategory()));
        product.setProducer(producerService.findByName(form.getNameProducer()));
        product.setCountStock(form.getNameCount());
        product.setDescription(form.getNameDescription());
        product.setPrice(form.getNamePrice());
        productService.save(product);
        loggingAction("Update", product);
        req.getSession().removeAttribute("imageLink");
        req.getSession().removeAttribute("ID_PRODUCT");
        return "redirect: /products";
    }

    private void loggingAction(String action, Product product) {
        LOGGER.info(action + " product: " +
                product.getName() + ";" +
                product.getPrice() + ";" +
                product.getCategory() + ";" +
                product.getProducer() + ";" +
                product.getImageLink() + ";" +
                product.getCountStock() + ";" +
                product.getDescription());
    }
}
