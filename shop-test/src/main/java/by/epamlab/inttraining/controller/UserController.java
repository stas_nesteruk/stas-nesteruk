package by.epamlab.inttraining.controller;

import by.epamlab.inttraining.entity.Account;
import by.epamlab.inttraining.exception.ValidationException;
import by.epamlab.inttraining.form.LoginForm;
import by.epamlab.inttraining.form.SignUpForm;
import by.epamlab.inttraining.service.AccountService;
import by.epamlab.inttraining.service.CurrentUser;
import by.epamlab.inttraining.utils.SessionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class UserController {
    private static final Logger LOGGER = Logger.getLogger(UserController.class);

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage(Model model) {
        model.addAttribute("loginForm", new LoginForm());
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String authorization(@Valid @ModelAttribute("loginForm") LoginForm form, BindingResult bindingResult, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        Account user = accountService.findByLoginAndPassword(form.getLogin(), form.getPassword());
        CurrentUser currentUser = (CurrentUser) request.getSession().getAttribute("CURRENT_USER");
        if (currentUser != null) {
            return "redirect: /products";
        } else {
            if (user == null) {
                throw new ValidationException("User don't exist in database");
            } else {
                SessionUtils.setCurrentUser(request, user);
                LOGGER.info("User sign in: " + user.getId() + " " + user.getLogin() + user.getRole());
                return "redirect: /products";
            }
        }
    }

    @RequestMapping(value = "/sign-out", method = RequestMethod.GET)
    public String signOut(HttpServletRequest request) {
        CurrentUser currentUser = (CurrentUser) request.getSession().getAttribute("CURRENT_USER");
        Account user = accountService.findById(currentUser.getId());
        request.getSession().invalidate();
        LOGGER.info("User sign out: " + user.getId() + " " + user.getLogin() + user.getRole());
        return "redirect: /main";
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.GET)
    public String getSignUpPage(HttpServletRequest req, Model model) {
        if (SessionUtils.isCreatedCurrentUser(req)) {
            return "redirect: /products";
        } else {
            model.addAttribute("signUpForm", new SignUpForm());
            return "signup";
        }
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public String signUp(@Valid @ModelAttribute("signUpForm") SignUpForm signUpForm, BindingResult bindingResult, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return "signup";
        }
        Account user = new Account();
        String login = signUpForm.getLogin();
        String password = signUpForm.getPassword();
        user.setLogin(login);
        user.setEmail(signUpForm.getEmail());
        user.setPassword(password);
        Account isHaveAccount = accountService.findByLoginAndPassword(login, password);
        if (isHaveAccount == null) {
            accountService.save(user);
            SessionUtils.setCurrentUser(request, user);
            LOGGER.info("User sign up: " + user.getId() + " " + user.getLogin() + user.getRole());
            return "redirect: /products";
        } else {
            return "redirect: /error";
        }
    }
}
