package by.epamlab.inttraining.controller;

import by.epamlab.inttraining.form.ShoppingCartDetailsForm;
import by.epamlab.inttraining.utils.Constants;

import javax.servlet.http.HttpServletRequest;

public class BaseCotroller {
    public long getTotalPage(long countProducts) {
        double count = (countProducts * 1.0) / Constants.MAX_PRODUCT_COUNT_PER_PAGE;
        return (long) Math.ceil(count);
    }

    public int getPageNumber(HttpServletRequest req) {
        try {
            return Integer.parseInt(req.getParameter("page"));
        } catch (NumberFormatException e) {
            return 1;
        }
    }

    public void setPaginationParam(HttpServletRequest req, long countProducts, int currentPageNumber) {
        long countPages = getTotalPage(countProducts);
        long begin = Math.max(1, getPageNumber(req) - 1);
        long end = Math.min(begin + 3, countPages);
        req.setAttribute("countPages", countPages);
        req.setAttribute("begin", begin);
        req.setAttribute("end", end);
        req.setAttribute("currentPageNumber", currentPageNumber);
    }

    public final ShoppingCartDetailsForm createProductForm(HttpServletRequest req) {
        return new ShoppingCartDetailsForm(
                Integer.parseInt(req.getParameter("idProduct")),
                Integer.parseInt(req.getParameter("count")));
    }
}
