package by.epamlab.inttraining.controller;


import by.epamlab.inttraining.entity.Account;
import by.epamlab.inttraining.form.SignUpForm;
import by.epamlab.inttraining.service.AccountService;
import by.epamlab.inttraining.service.CurrentUser;
import by.epamlab.inttraining.utils.SessionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class MainController {
    private static final Logger LOGGER = Logger.getLogger(MainController.class);

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String getMainPage(HttpServletRequest request, Model model) {
        CurrentUser currentUser = (CurrentUser) request.getSession().getAttribute("CURRENT_USER");
        if (currentUser != null) {
            return "redirect: /products";
        }
        model.addAttribute("signUpForm", new SignUpForm());
        return "main";
    }

    @RequestMapping(value = "/main", method = RequestMethod.POST)
    public String signUp(@Valid @ModelAttribute("signUpForm") SignUpForm signUpForm, BindingResult bindingResult, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return "main";
        }
        Account account = new Account();
        String login = signUpForm.getLogin();
        String password = signUpForm.getPassword();
        account.setLogin(login);
        account.setEmail(signUpForm.getEmail());
        account.setPassword(password);
        Account isHaveAccount = accountService.findByLoginAndPassword(login, password);
        if (isHaveAccount == null) {
            accountService.save(account);
            SessionUtils.setCurrentUser(request, account);
            LOGGER.info("User sign up: " + account.getId() + " " + account.getLogin());
            return "redirect: /products";
        } else {
            return "redirect: /error";
        }
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String getErrorPage() {
        return "error";
    }
}
