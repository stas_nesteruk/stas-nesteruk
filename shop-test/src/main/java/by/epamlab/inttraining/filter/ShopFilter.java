package by.epamlab.inttraining.filter;

import by.epamlab.inttraining.exception.AbstractException;
import by.epamlab.inttraining.exception.ResourceNotFoundException;
import by.epamlab.inttraining.exception.ServerErrorException;
import by.epamlab.inttraining.exception.ValidationException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.nio.file.AccessDeniedException;

public class ShopFilter extends AbstractFilter {
    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {
        try {
            chain.doFilter(req, new ThrowExceptionOnSendErrorResponse(resp));
        } catch (Throwable th) {
            String requestUrl = req.getRequestURI();
            LOGGER.error("Request " + requestUrl + " failed: " + th.getMessage(), th);
            req.setAttribute("errorCode", getErrorCode(th));
            req.getRequestDispatcher("/WEB-INF/JSP/error.jsp").forward(req, resp);
        }
    }

    private int getErrorCode(Throwable th) {
        if (th.getCause() instanceof AbstractException) {
            return ((AbstractException) th.getCause()).getErrorCode();
        }
        if (th instanceof AbstractException) {
            return ((AbstractException) th).getErrorCode();
        } else {
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        }
    }

    private static class ThrowExceptionOnSendErrorResponse extends HttpServletResponseWrapper {

        public ThrowExceptionOnSendErrorResponse(HttpServletResponse response) {
            super(response);
        }

        @Override
        public void sendError(int sc) throws IOException {
            sendError(sc, "INTERNAL_ERROR");
        }

        @Override
        public void sendError(int sc, String msg) throws IOException {
            switch (sc) {
                case 400:
                    throw new ValidationException(msg);
                case 403:
                    throw new AccessDeniedException(msg);
                case 404:
                    throw new ResourceNotFoundException(msg);
                default:
                    throw new ServerErrorException(msg);
            }
        }
    }
}
