package by.epamlab.inttraining.filter;

import by.epamlab.inttraining.utils.SessionUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticationFilter extends AbstractFilter {
    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {
        if (SessionUtils.isCreatedCurrentUser(req)) {
            chain.doFilter(req, resp);
        } else {
            resp.sendRedirect("/login");
        }
    }
}
