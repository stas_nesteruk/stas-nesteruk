package by.epamlab.inttraining.beans;

public enum Role {
    ADMIN, USER;
}
