package by.epamlab.inttraining.repository.storage;

import by.epamlab.inttraining.entity.Category;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer> {
    @Override
    List<Category> findAll(Sort sort);

    Category findByName(String name);
}
