package by.epamlab.inttraining.repository.storage;

import by.epamlab.inttraining.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {
    long countProductByNameContainsOrDescriptionContains(String query, String query2);

    Page<Product> findProductByNameContainsOrDescriptionContains(String query, String query2, Pageable pageable);

    List<Product> findProductByNameContainsOrDescriptionContains(String query, String query2);

    @Override
    Page<Product> findAll(Pageable pageable);

    @Override
    List<Product> findAll(Sort sort);

    Product findProductById(int id);

    List<Product> findProductByCategory_Url(String url);

}
