package by.epamlab.inttraining.repository.storage;

import by.epamlab.inttraining.entity.Order;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Integer> {
    Order findById(int id);

    Order findByAccount_IdAndCreated(int id, Timestamp created);

    List<Order> findByAccount_Id(int id);
}
