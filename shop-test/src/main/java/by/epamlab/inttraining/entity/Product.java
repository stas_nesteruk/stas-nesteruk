package by.epamlab.inttraining.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The persistent class for the PRODUCT database table.
 */
@Entity
@Table(name = "product")
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Lob
    private String description;

    @Column(name = "IMAGE_LINK")
    private String imageLink;

    @Size(min = 4, message = "Product name should be more 4 characters")
    private String name;

    private BigDecimal price;

    @Column(name = "COUNT_STOCK")
    private int countStock;

    //bi-directional many-to-one association to Category
    @ManyToOne
    @JoinColumn(name = "ID_CATEGORY")
    private Category category;

    //bi-directional many-to-one association to Producer
    @ManyToOne
    @JoinColumn(name = "ID_PRODUCER")
    private Producer producer;

    public Product() {
    }

    public int getCountStock() {
        return countStock;
    }

    public void setCountStock(int countStock) {
        this.countStock = countStock;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageLink() {
        return this.imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Category getCategory() {
        return this.category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Producer getProducer() {
        return this.producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

}