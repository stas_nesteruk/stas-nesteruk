package by.epamlab.inttraining.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the ORDER_ITEM database table.
 */
@Entity
@Table(name = "ORDER_ITEM")
public class OrderItem implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int count;

    @Column(name = "ID_PRODUCT")
    private int idProduct;

    //bi-directional many-to-one association to Order
    @ManyToOne
    @JoinColumn(name = "ID_ORDERS")
    private Order order;

    public OrderItem() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getIdProduct() {
        return this.idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public Order getOrder() {
        return this.order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

}