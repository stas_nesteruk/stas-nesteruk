<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div id="productList">
    <c:if test="${EDIT_MODE == 'ON'}">
        <div id="addNewProduct">
            <a type="button" href="/add-product" class="btn btn-default">
                <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add product
            </a>
        </div>
    </c:if>
    <div class="row">
        <c:forEach var="product" items="${products}">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div id="product${product.id}" class="panel panel-default product">
                    <div class="panel-body">
                        <div class="thumbnail">
                            <img src="${product.imageLink}" alt="${product.name}">
                            <div class="desc">
                                <div class="cell">
                                    <p class="description text-center">${product.description}</p>
                                </div>
                            </div>
                        </div>
                        <h4 class="name">${product.name}</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <small>Category:</small>
                                <span class="category">${product.category.name}</span></li>
                            <li class="list-group-item">
                                <small>Producer:</small>
                                <span class="producer">${product.producer.name}</span></li>
                            <li class="list-group-item">
                                <small>Count:</small>
                                <span class="count">${product.countStock}</span></li>
                        </ul>
                        <span>$</span>
                        <div class="price">${product.price}</div>
                        <c:choose>
                            <c:when test="${EDIT_MODE == 'ON'}">
                                <a href="/delete?id=${product.id}" class="btn btn-default pull-right"
                                   data-id-product="${product.id}">Delete</a>

                                <a href="/edit-product?id=${product.id}" class="btn btn-default pull-right"
                                   data-id-product="${product.id}"
                                   data-command="EDIT"><span class="glyphicon glyphicon-edit"
                                                             aria-hidden="true"></span>
                                    Edit</a>
                            </c:when>
                            <c:otherwise>
                                <a class="btn btn-default pull-right buy-btn" data-id-product="${product.id}">Buy</a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
    <c:if test="${countPages > 1}">
        <nav aria-label="Page navigation" class="text-center">
            <ul class="pagination">
                <li ${currentPageNumber == 1 ? 'class="disabled"' : ''}>
                    <a href="?page=${currentPageNumber-1}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <c:forEach var="i" begin="${begin}" end="${end}">
                    <li ${currentPageNumber == i ? 'class="active"' : ''}><a
                            href="${not empty searchQuery ? "?query=" : ''}${searchQuery}${not empty searchQuery ? '&': '?'}page=${i}">${i}</a>
                    </li>
                </c:forEach>
                <li ${currentPageNumber == countPages ? 'class="disabled"' : ''}>
                    <a href="?page=${currentPageNumber+1}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </c:if>
</div>


<div id="addProductModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add product to shopping cart</h4>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12 col-sm-6">
                    <div class="thumbnail">
                        <img class="product-image" src="" alt="Product image">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <h4 class="name text-center">Product name</h4>
                    <div class="list-group hidden-xs">
                        <span class="list-group-item"><small>Category: </small><span class="category">?</span></span>
                        <span class="list-group-item"><small>Producer: </small><span class="producer">?</span></span>
                    </div>
                    <div class="list-group">
                        <span class="list-group-item"><small>Price: </small><span class="price">0</span></span>
                        <span class="list-group-item"><small>Count: </small><input type="number" class="count" value="1"
                                                                                   min="1" max="10"></span>
                        <span class="list-group-item"><small>Cost: </small><span class="cost">0</span></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="addToCart" type="button" class="btn btn-primary">Add</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="addNewProductModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add product</h4>
            </div>
            <div class="modal-body row">
                <form:form method="post" action="/edit" id="form-add-product" class="form-add-product"
                           commandName="addNewProductForm"
                           enctype="multipart/form-data">
                    <div class="col-xs-12 col-sm-6">
                        <div class="thumbnail image-preview" id="image-preview">
                            <img class="product-image" src="" alt="Product preview">
                            <span class="image-preview-default-text">Image preview</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="nameProduct" class="form-control name-product"
                               placeholder="Product name">
                        <form:errors path="nameProduct" cssClass="errorMsg pull-right"/>
                        <form:errors path="nameCategory" cssClass="errorMsg pull-right"/>
                        <form:errors path="nameProducer" cssClass="errorMsg pull-right"/>
                        <form:errors path="namePrice" cssClass="errorMsg pull-right"/>
                        <form:errors path="nameCount" cssClass="errorMsg pull-right"/>
                        <div class="list-group">
                            <select name="nameCategory" class="form-control name-category"
                                    id="addNewProductSelectCategory" style="margin-bottom: 20px;">
                                <c:forEach var="category" items="${categories}">
                                    <option>${category.name}</option>
                                </c:forEach>
                            </select>
                            <select name="nameProducer" class="form-control name-producer"
                                    id="addNewProductSelectProducer" style="margin-bottom: 20px;">
                                <c:forEach var="producer" items="${producers}">
                                    <option>${producer.name}</option>
                                </c:forEach>
                            </select>
                            <input type="number" name="namePrice" class="list-group-item form-control name-price"
                                   placeholder="Price">
                            <input type="number" name="nameCount" class="list-group-item form-control name-count"
                                   placeholder="Count">
                        </div>
                        <div class="list-group">
                            <form:errors path="nameDescription" cssClass="errorMsg pull-right"/>
                            <textarea name="nameDescription" class="list-group-item form-control name-description"
                                      placeholder="Description" rows="2"></textarea>
                        </div>
                    </div>
                    <div class="input-group">
                        <form:errors path="sampleFile" cssClass="errorMsg pull-right"/>
                        <input type='file' id="add-product-image-file" name="sampleFile">
                    </div>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary pull-right">Add</button>
                </form:form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<div id="editProductModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit product</h4>
            </div>
            <div class="modal-body row">
                <form id="form-edit-product" class="form-edit-product" method="post">
                    <div class="col-xs-12 col-sm-6">
                        <div class="thumbnail">
                            <img class="product-edit-image" src="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="nameProduct" class="form-control name-product"
                               placeholder="Product name" required="required">
                        <div class="list-group">
                            <select name="nameCategory" class="form-control name-category"
                                    id="addEditProductSelectCategory" style="margin-bottom: 20px;">
                                <c:forEach var="category" items="${categories}">
                                    <option>${category.name}</option>
                                </c:forEach>
                            </select>
                            <select name="nameProducer" class="form-control name-producer"
                                    id="addEditProductSelectProducer" style="margin-bottom: 20px;">
                                <c:forEach var="producer" items="${producers}">
                                    <option>${producer.name}</option>
                                </c:forEach>
                            </select>
                            <input type="number" name="namePrice" class="list-group-item form-control name-price"
                                   placeholder="Price" required="required">
                            <input type="number" name="nameCount" class="list-group-item form-control name-count"
                                   placeholder="Count" required="required">
                        </div>
                        <div class="list-group">
                            <textarea name="nameDescription" class="list-group-item form-control name-description"
                                      placeholder="Description" rows="2" required="required"></textarea>
                        </div>
                    </div>
                    <div class="input-group">
                        <input type="file" class="custom-file-input" id="edit-product-image-file"
                               name="edit-product-image-file"
                               aria-describedby="inputGroupFileAddon01" lang="en">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="saveEditProduct" type="button" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="addCategoryModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add new category</h4>
            </div>
            <div class="modal-body row">
                <form class="form-add-category" method="post">
                    <label>Enter a category name:</label>
                    <input type="text" name="nameCategory" class="form-control name-category">
                </form>
            </div>
            <div class="modal-footer">
                <button id="addCategory" type="submit" class="btn btn-primary">Add</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>