<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="user-orders">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Order</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="item" items="${orders}" varStatus="loop">
            <tr id="${item.id}" class="item">
                <td class="order-number"><a href="/order?id=${item.id}">Order # ${item.id}</a></td>
                <td class="created"><fmt:formatDate value="${item.created}" pattern="dd-MM-YYYY HH:mm"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>