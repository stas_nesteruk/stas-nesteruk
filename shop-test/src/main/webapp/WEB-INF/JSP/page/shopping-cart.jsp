<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="shoppingCartProducts">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Count</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="item" items="${CURRENT_SHOPPING_CART.items}" varStatus="loop">
            <tr id="product${item.product.id}" class="item">
                <td class="text-center"><img class="small" src="${item.product.imageLink}"
                                             alt="${item.product.name}">${item.product.name}</td>
                <td class="price">$ ${item.product.price}</td>
                <td class="count">${item.count}</td>
                <td>
                    <c:choose>
                        <c:when test="${item.count > 1}">
                            <a class="btn btn-default remove-product" data-id-product="${item.product.id}"
                               data-count="1">Remove</a>
                            <a class="btn btn-default remove-product all" data-id-product="${item.product.id}"
                               data-count="${item.count}">Remove all</a>
                        </c:when>
                        <c:otherwise>
                            <a class="btn btn-default remove-product" data-id-product="${item.product.id}"
                               data-count="1">Remove</a>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="2" class="text-right"><strong>Total:</strong></td>
            <td colspan="2" class="total">$ ${CURRENT_SHOPPING_CART.totalCost}</td>
        </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-lg-2 col-lg-offset-5">
            <form action="/order" class="form-order" method="post">
                <button class="btn btn-primary btn-block" type="submit">Make order</button>
            </form>
        </div>
    </div>
</div>