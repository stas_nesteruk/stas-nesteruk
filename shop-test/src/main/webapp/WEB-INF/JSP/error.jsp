<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Shop</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/static/css/bootstrap.css">
    <link rel="stylesheet" href="/static/css/bootstrap-theme.css">
    <link rel="stylesheet" href="/static/css/font-awesome.css">
    <link rel="stylesheet" href="/static/css/app.css">
</head>
<body>
<header>
    <jsp:include page="fragment/header.jsp"/>
</header>
<div class="container-fluid">
    <div class="row">
        <div id="error">
            <div class="alert alert-danger" role="alert">
                <c:choose>
                    <c:when test="${errorCode == 400}">User don't found in database.</c:when>
                    <c:when test="${errorCode == 403}">You don't have enough permissions to view the page.</c:when>
                    <c:when test="${errorCode == 404}">Resource not found.</c:when>
                    <c:otherwise>
                        <h2>Oops, something went wrong</h2>
                        <h3>Whatever happened, it was probably our fault. Please try again later.</h3>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <jsp:include page="fragment/footer.jsp"/>
</footer>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/bootstrap.js"></script>
<script src="/static/js/app.js"></script>
</body>
</html>