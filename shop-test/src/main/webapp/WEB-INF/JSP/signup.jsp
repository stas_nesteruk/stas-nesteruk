<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Shop</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/static/css/bootstrap.css">
    <link rel="stylesheet" href="/static/css/bootstrap-theme.css">
    <link rel="stylesheet" href="/static/css/font-awesome.css">
    <link rel="stylesheet" href="/static/css/app.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="wrapper">
            <form:form action="/sign-up" class="form-signup" method="post" commandName="signUpForm">
                <label>Username</label><form:errors path="login" cssClass="errorMsg pull-right"/>
                <form:input path="login" type="text" name="login" class="form-control"/>
                <label>Email</label><form:errors path="email" cssClass="errorMsg pull-right"/>
                <form:input path="email" type="text" name="email" class="form-control"/>
                <label>Password</label><form:errors path="password" cssClass="errorMsg pull-right"/>
                <input type="Password" name="password" class="form-control">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
            </form:form>
        </div>
    </div>
</div>
<footer class="footer">
    <jsp:include page="fragment/footer.jsp"/>
</footer>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/bootstrap.js"></script>
<script src="/static/js/app.js"></script>
</body>
</html>