package by.epamlab.inttraining;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class OperationsOnSetsTest {

    @org.junit.Test
    public void testUnionInt() {
        OperationsOnSets<Integer> set = new OperationsOnSets<>(Arrays.asList(1,2,4,5,6,8));
        Set<Integer> actual           = set.union(Arrays.asList(0,3,4,5,6,8,0,11,15));
        Set<Integer> expected         = new HashSet<>(Arrays.asList(0,1,2,3,4,5,6,8,11,15));
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testUnionString() {
        OperationsOnSets<String> set = new OperationsOnSets<>();
        Set<String> actual           = set.union(Arrays.asList("aaa","bbb", "ccc" ,"ddd"),
                                                 Arrays.asList("eee","bbb", "fff" ,"ddd", "ggg"));
        Set<String> expected         = new HashSet<>(Arrays.asList("aaa", "bbb", "ccc" ,
                                                    "ddd", "eee", "fff", "ggg"));
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testIntersectionInt() {
        OperationsOnSets<Integer> set = new OperationsOnSets<>(Arrays.asList(8, 7, 12 , 34, 98, 101));
        Set<Integer> actual           = set.intersection(Arrays.asList(5, 12, 37, 64, 8, 99, 101));
        Set<Integer> expected         = new HashSet<>(Arrays.asList(8, 12, 101));
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testIntersectionString() {
        OperationsOnSets<String> set = new OperationsOnSets<>(Arrays.asList("aaa", "bbb", "ccc"));
        Set<String> actual           = set.intersection(Arrays.asList("ddd","eee", "fff" ,"bbb", "aaa"));
        Set<String> expected         = new HashSet<>(Arrays.asList("aaa", "bbb"));
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testDifferenceInt() {
        OperationsOnSets<Integer> set = new OperationsOnSets<>(Arrays.asList(1,2,4));
        Set<Integer> actual           = set.difference(Arrays.asList(3,4,5,6));
        Set<Integer> expected         = new HashSet<>(Arrays.asList(1,2));
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testDifferenceCharacter() {
        OperationsOnSets<Character> set = new OperationsOnSets<>();
        Set<Character> actual           = set.difference(Arrays.asList('a','b','c', 'd', 'e'),
                                                         Arrays.asList('b', 'e', 'f', 'a'));
        Set<Character> expected         = new HashSet<>(Arrays.asList('c', 'd'));
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testSymmetricDifferenceInt() {
        OperationsOnSets<Integer> set = new OperationsOnSets<>(Arrays.asList(1,2,4));
        Set<Integer> actual           = set.symmetricDifference(Arrays.asList(3,4,5,6));
        Set<Integer> expected         = new HashSet<>(Arrays.asList(1,2,3,5,6));
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testSymmetricDifferenceCharacter() {
        OperationsOnSets<Character> set = new OperationsOnSets<>();
        Set<Character> actual           = set.symmetricDifference(Arrays.asList('a','b','c', 'd', 'e'),
                                                                  Arrays.asList('b', 'e', 'f', 'a'));
        Set<Character> expected         = new HashSet<>(Arrays.asList('c', 'd', 'f'));
        assertEquals(expected, actual);
    }
}