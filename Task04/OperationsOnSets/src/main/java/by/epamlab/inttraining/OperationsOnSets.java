package by.epamlab.inttraining;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class OperationsOnSets<E> {
    private Set<E> set = new HashSet<>();

    OperationsOnSets(){
    }

    OperationsOnSets(Collection<? extends E> values){
        set.addAll(values);
    }

    public Set<E> union(Collection<? extends E> values){
        for (E e : values) {
            if(!contains(e)){
                set.add(e);
            }
        }
        return set;
    }

    public Set<E> union(Collection<? extends E> firstValues, Collection<? extends E> secondValues){
        Set<E> result = new HashSet<>(firstValues);
        for (E e : secondValues) {
            if(!contains(result, e)){
                result.add(e);
            }
        }
        return result;
    }

    public Set<E> intersection(Collection<? extends E> values){
        Set<E> toRemove = new HashSet<>();
        for(E e : set){
            if(!contains(values, e)){
                toRemove.add(e);
            }
        }
        set.removeAll(toRemove);
        return set;
    }

    public Set<E> difference(Collection<? extends E> values){
        Set<E> toRemove = new HashSet<>();
        for(E e : set){
            if(contains(values, e)){
                toRemove.add(e);
            }
        }
        set.removeAll(toRemove);
        return set;
    }

    public Set<E> difference(Collection<? extends E> firstValues, Collection<? extends E> secondValues){
        Set<E> result = new HashSet<>();
        for(E e : firstValues){
            if(!contains(secondValues, e)){
                result.add(e);
            }
        }
        return result;
    }

    public Set<E> symmetricDifference(Collection<? extends E> values){
        return union(difference(set, values), difference(values, set));
    }

    public Set<E> symmetricDifference(Collection<? extends E> firstValues, Collection<? extends E> secondValues){
        return union(difference(firstValues, secondValues), difference(secondValues, firstValues));
    }

    private boolean contains(E e){
        for (E number : set){
            if(number.equals(e)){
                return true;
            }
        }
        return false;
    }

    private boolean contains(Collection<? extends E> values, E e){
        for (E number : values){
            if(number.equals(e)){
                return true;
            }
        }
        return false;
    }
}
