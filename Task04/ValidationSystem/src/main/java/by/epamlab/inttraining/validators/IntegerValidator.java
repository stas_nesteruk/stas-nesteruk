package by.epamlab.inttraining.validators;

import by.epamlab.inttraining.exceptions.ValidationFailedException;
import org.apache.log4j.Logger;

public class IntegerValidator implements Validator<Integer>{
    private static final Logger LOGGER = Logger.getLogger(IntegerValidator.class);

    private static final int FIRST_LOWER_LIMIT  = 1;
    private static final int FIRST_UPPER_LIMIT  = 10;
    private static final int SECOND_LOWER_LIMIT = 15;
    private static final int SECOND_UPPER_LIMIT = 30;

    private static final String SUCCESS_MSG = "Validation successful.";
    private static final String ERROR_MSG   = "Validation error - %d: the number " +
            "must belong to the segments [0:10] and [15:30]";

    @Override
    public void execute(Integer number) throws ValidationFailedException{
        if((number >= FIRST_LOWER_LIMIT && number <= FIRST_UPPER_LIMIT) ||
                (number >= SECOND_LOWER_LIMIT && number <= SECOND_UPPER_LIMIT)){
            LOGGER.info(SUCCESS_MSG);
        }else {
            LOGGER.error(String.format(ERROR_MSG, number));
            throw new ValidationFailedException(String.format(ERROR_MSG, number));
        }
    }
}
