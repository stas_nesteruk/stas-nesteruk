package by.epamlab.inttraining.validators;

import by.epamlab.inttraining.exceptions.ValidationFailedException;

public interface Validator <T> {
    void execute(T t) throws ValidationFailedException;
}
