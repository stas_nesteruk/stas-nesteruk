package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.Person;
import by.epamlab.inttraining.exceptions.ValidationFailedException;
import by.epamlab.inttraining.validators.IntegerValidator;
import by.epamlab.inttraining.validators.PersonValidator;
import by.epamlab.inttraining.validators.StringValidator;
import by.epamlab.inttraining.validators.Validator;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;


public class ValidationSystem {
    private static final Logger LOGGER = Logger.getLogger(ValidationSystem.class);

    private static Map<Class<?>, Validator> validators = new HashMap<>();

    private static final String ERROR_MSG = "Validation error: not found " +
            "validator to work with the current data type: ";

    static {
        validators.put(Integer.class, new IntegerValidator());
        validators.put(String.class, new StringValidator());
        validators.put(Person.class, new PersonValidator());
    }

    private ValidationSystem(){
        throw new IllegalStateException("Utility class");
    }

    public static void validate(Object o) throws ValidationFailedException {
        if(validators.containsKey(o.getClass())) {
            Validator validator = validators.get(o.getClass());
            validator.execute(o);
        }else {
            LOGGER.error(ERROR_MSG + o.getClass().getSimpleName());
            throw new ValidationFailedException(ERROR_MSG + o.getClass().getSimpleName());
        }
    }

}
