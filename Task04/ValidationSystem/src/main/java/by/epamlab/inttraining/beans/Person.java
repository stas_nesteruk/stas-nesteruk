package by.epamlab.inttraining.beans;

public class Person {
    private int age;
    private int height;

    public Person(){}
    public Person(int age, int height) {
        this.age = age;
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Person: ");
        sb.append("age - ").append(age).append(";");
        sb.append("height - ").append(height);
        return sb.toString();
    }
}
