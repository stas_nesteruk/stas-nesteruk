package by.epamlab.inttraining.validators;

import by.epamlab.inttraining.beans.Person;
import by.epamlab.inttraining.exceptions.ValidationFailedException;
import org.apache.log4j.Logger;

public class PersonValidator implements Validator<Person>{
    private static final Logger LOGGER = Logger.getLogger(PersonValidator.class);

    private static final int AGE_LIMIT    = 16;
    private static final int HEIGHT_LIMIT = 160;

    private static final String SUCCESS_MSG = "Validation successful.";
    private static final String ERROR_MSG   = "Validation error - %s: a person must " +
            "be over 16 years old and taller than 160 cm.";

    @Override
    public void execute(Person person) throws ValidationFailedException {
        if(person.getAge() >= AGE_LIMIT && person.getHeight() >= HEIGHT_LIMIT){
            LOGGER.info(SUCCESS_MSG);
        }else{
            LOGGER.error(String.format(ERROR_MSG, person));
            throw new ValidationFailedException(String.format(ERROR_MSG, person));
        }
    }
}
