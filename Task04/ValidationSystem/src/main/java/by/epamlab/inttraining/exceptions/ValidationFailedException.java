package by.epamlab.inttraining.exceptions;

public class ValidationFailedException extends Exception{
    public ValidationFailedException(){}
    public ValidationFailedException(String message){
        super(message);
    }
    public ValidationFailedException(Throwable cause){
        super(cause);
    }
    public ValidationFailedException(String message, Throwable cause){
        super(message, cause);
    }
}
