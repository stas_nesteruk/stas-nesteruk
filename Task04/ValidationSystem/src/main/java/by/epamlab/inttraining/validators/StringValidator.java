package by.epamlab.inttraining.validators;

import by.epamlab.inttraining.exceptions.ValidationFailedException;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidator implements Validator<String> {
    private static final Logger LOGGER = Logger.getLogger(StringValidator.class);
    private static final String REGEXP = "^[A-Z].{0,14}$";

    private static final String SUCCESS_MSG = "Validation successful.";
    private static final String ERROR_MSG   = "Validation error - %s: the string " +
            "must start with a capital letter and must not exceed 15 characters";

    @Override
    public void execute(String value) throws ValidationFailedException{
        Pattern pattern = Pattern.compile(REGEXP);
        Matcher matcher = pattern.matcher(value);
        if(matcher.matches()){
            LOGGER.info(SUCCESS_MSG);
        }else {
            LOGGER.error(String.format(ERROR_MSG, value));
            throw new ValidationFailedException(String.format(ERROR_MSG, value));
        }
    }
}
