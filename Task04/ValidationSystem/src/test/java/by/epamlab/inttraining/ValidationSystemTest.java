package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.Person;
import by.epamlab.inttraining.exceptions.ValidationFailedException;

public class ValidationSystemTest {

    @org.junit.Test
    public void testValidateInt() throws ValidationFailedException {
        ValidationSystem.validate(1);
        ValidationSystem.validate(5);
        ValidationSystem.validate(15);
        ValidationSystem.validate(25);
    }

    @org.junit.Test (expected = ValidationFailedException.class)
    public void testValidateIntFails() throws ValidationFailedException {
        ValidationSystem.validate(12);
    }

    @org.junit.Test (expected = ValidationFailedException.class)
    public void testValidateStringFails() throws ValidationFailedException {
        ValidationSystem.validate(" Example");
    }

    @org.junit.Test
    public void testValidateString() throws ValidationFailedException {
        ValidationSystem.validate("Correct string");
    }

    @org.junit.Test
    public void testValidatePerson() throws ValidationFailedException {
        ValidationSystem.validate(new Person(18, 174));
    }

    @org.junit.Test (expected = ValidationFailedException.class)
    public void testValidatePersonFails() throws ValidationFailedException {
        ValidationSystem.validate(new Person(15, 168));
    }

}