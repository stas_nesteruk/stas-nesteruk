package by.epamlab.inttraining.creators;

import by.epamlab.inttraining.beans.Itinerary;
import by.epamlab.inttraining.creators.pricinginfo.PricingInfoCreator;
import by.epamlab.inttraining.creators.ticketinginfo.TicketingInfoCreator;
import by.epamlab.inttraining.creators.air.AirCreator;

public class ItineraryCreator implements Creator{

    private static final String ONE = "1";
    private Itinerary itinerary;
    private AirCreator air;
    private PricingInfoCreator pricingInfo;
    private TicketingInfoCreator ticketingInfo;

    public ItineraryCreator() {
        this.itinerary = new Itinerary();
        this.air = new AirCreator();
        this.pricingInfo = new PricingInfoCreator();
        this.ticketingInfo = new TicketingInfoCreator();
    }

    @Override
    public void create() {
        air.create();
        pricingInfo.create();
        ticketingInfo.create();
        itinerary.setAir(air.getAir());
        itinerary.setPricingInfo(pricingInfo.getPricingInfo());
        itinerary.setTicketingInfo(ticketingInfo.getTicketingInfoType());
        itinerary.setSequenceNumber(ONE);
    }

    public Itinerary getItinerary() {
        return itinerary;
    }
}
