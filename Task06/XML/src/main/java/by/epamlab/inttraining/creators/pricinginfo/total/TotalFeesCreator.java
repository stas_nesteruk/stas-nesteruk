package by.epamlab.inttraining.creators.pricinginfo.total;

import by.epamlab.inttraining.beans.FeeType;
import by.epamlab.inttraining.beans.FeesType;
import by.epamlab.inttraining.creators.Creator;

public class TotalFeesCreator implements Creator {
    private static final String TAX_CODE = "EE";
    private static final String AMOUNT = "10.00";
    private static final String CURRENCY_CODE = "EUR";
    private FeesType fees;

    public TotalFeesCreator() {
        this.fees = new FeesType();;
    }

    @Override
    public void create() {
        FeeType fee = new FeeType();
        fee.setTaxCode(TAX_CODE);
        fee.setAmount(AMOUNT);
        fee.setCurrencyCode(CURRENCY_CODE);
        fees.setFee(fee);
    }

    public FeesType getFees() {
        return fees;
    }
}
