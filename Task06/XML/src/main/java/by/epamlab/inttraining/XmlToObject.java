package by.epamlab.inttraining;


import by.epamlab.inttraining.beans.Itinerary;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class XmlToObject {
    private static final Logger LOGGER = Logger.getLogger(XmlToObject.class);
    private static final String XML_PATH = "XML\\src\\main\\resources\\xml.xml";
    private static final String MSG_XML_CREATED = "Unmarshaling completed: Itinerary - created";
    private Itinerary itinerary;

    public void execute(){
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Itinerary.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            itinerary = (Itinerary) unmarshaller.unmarshal(new File(XML_PATH));
            LOGGER.info(MSG_XML_CREATED);
        } catch (JAXBException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void toPrint(){
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Itinerary.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            LOGGER.info("Itinerary object:");
            marshaller.marshal(itinerary, System.out);
        } catch (JAXBException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
