package by.epamlab.inttraining.creators;

public interface Creator {
    void create();
}
