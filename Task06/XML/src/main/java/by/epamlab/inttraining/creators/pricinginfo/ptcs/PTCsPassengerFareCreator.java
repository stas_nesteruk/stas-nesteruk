package by.epamlab.inttraining.creators.pricinginfo.ptcs;

import by.epamlab.inttraining.beans.PassengerFareType;
import by.epamlab.inttraining.beans.TotalType;
import by.epamlab.inttraining.creators.Creator;
import by.epamlab.inttraining.creators.pricinginfo.total.TotalBaseCreator;
import by.epamlab.inttraining.creators.pricinginfo.total.TotalExtensionCreator;
import by.epamlab.inttraining.creators.pricinginfo.total.TotalFeesCreator;
import by.epamlab.inttraining.creators.pricinginfo.total.TotalTaxesCreator;

public class PTCsPassengerFareCreator implements Creator {
    private static final String AMOUNT = "163.52";
    private static final String CURRENCY_CODE = "EUR";
    private PassengerFareType passengerFare;
    private TotalTaxesCreator pricingInfoTaxes;
    private TotalBaseCreator pricingInfoBase;
    private TotalFeesCreator pricingInfoFees;
    private TotalExtensionCreator pricingInfoExtension;

    public PTCsPassengerFareCreator() {
        this.passengerFare = new PassengerFareType();
        this.pricingInfoTaxes = new TotalTaxesCreator();
        this.pricingInfoBase = new TotalBaseCreator();
        this.pricingInfoFees = new TotalFeesCreator();
        this.pricingInfoExtension = new TotalExtensionCreator();
    }

    @Override
    public void create() {
        pricingInfoTaxes.create();
        pricingInfoBase.create();
        pricingInfoFees.create();
        pricingInfoExtension.create();
        TotalType total = new TotalType();
        total.setAmount(AMOUNT);
        total.setCurrencyCode(CURRENCY_CODE);
        passengerFare.setBase(pricingInfoBase.getBase());
        passengerFare.setTaxes(pricingInfoTaxes.getTaxes());
        passengerFare.setFees(pricingInfoFees.getFees());
        passengerFare.setTotal(total);
        passengerFare.setExtension(pricingInfoExtension.getExtension());
    }

    public PassengerFareType getPassengerFare() {
        return passengerFare;
    }
}
