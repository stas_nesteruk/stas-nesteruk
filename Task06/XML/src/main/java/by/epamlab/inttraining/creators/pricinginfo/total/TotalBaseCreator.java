package by.epamlab.inttraining.creators.pricinginfo.total;

import by.epamlab.inttraining.beans.BaseType;
import by.epamlab.inttraining.creators.Creator;

public class TotalBaseCreator implements Creator {
    private static final String AMOUNT = "91.00";
    private static final String CURRENCY_CODE = "EUR";
    private BaseType base;

    public TotalBaseCreator() {
        this.base = new BaseType();;
    }

    @Override
    public void create() {
        base.setAmount(AMOUNT);
        base.setCurrencyCode(CURRENCY_CODE);
    }

    public BaseType getBase() {
        return base;
    }
}
