package by.epamlab.inttraining;

import by.epamlab.inttraining.creators.ItineraryCreator;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class ObjectToXml {
    private static final Logger LOGGER = Logger.getLogger(ObjectToXml.class);

    private static final String OUTPUT_XML_PATH = "XML\\ObjectToXML.xml";
    private static final String MSG_XML_CREATED = "Marshaling completed: ObjectToXML.xml - created";
    private static final String BEANS_PATH = "by.epamlab.inttraining.beans";

    private ItineraryCreator itinerary;

    public ObjectToXml() {
        this.itinerary = new ItineraryCreator();
    }

    public void execute(){
        try {
            itinerary.create();
            JAXBContext jaxbContext = JAXBContext.newInstance(BEANS_PATH);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(itinerary.getItinerary(), new File(OUTPUT_XML_PATH));
            LOGGER.info(MSG_XML_CREATED);
        } catch (JAXBException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
