package by.epamlab.inttraining.creators.pricinginfo;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.creators.Creator;
import by.epamlab.inttraining.creators.pricinginfo.ptcs.PTCsPassengerFareCreator;
import by.epamlab.inttraining.creators.pricinginfo.ptcs.PassengerCreator;

public class PricingInfoPTCsCreator implements Creator {
    private static final String SOURCE = "Private";
    private static final String BASIS_CODE = "AAAAAAA";
    private static final String CODE = "ADT";
    private static final String QUANTITY = "1";
    private PTCsType ptcs;
    private PTCsPassengerFareCreator ptCsPassengerFare;
    private PassengerCreator passenger;

    public PricingInfoPTCsCreator() {
        this.ptcs = new PTCsType();
        this.ptCsPassengerFare = new PTCsPassengerFareCreator();
        this.passenger = new PassengerCreator();
    }

    @Override
    public void create() {
        PTCType ptc = new PTCType();
        ptCsPassengerFare.create();
        passenger.create();
        ptc.setSource(SOURCE);
        BasisCodesType basisCodes = new BasisCodesType();
        basisCodes.setBasisCode(BASIS_CODE);
        PassengerQuantityType passengerQuantity = new PassengerQuantityType();
        passengerQuantity.setCode(CODE);
        passengerQuantity.setQuantity(QUANTITY);
        ExtensionType ptcExtension = new ExtensionType();
        ptcExtension.setPassenger(passenger.getPassenger());
        ptc.setPassengerQuantity(passengerQuantity);
        ptc.setBasisCodes(basisCodes);
        ptc.setPassengerFare(ptCsPassengerFare.getPassengerFare());
        ptc.setExtension(ptcExtension);
        ptcs.setPTC(ptc);
    }

    public PTCsType getPtcs() {
        return ptcs;
    }
}
