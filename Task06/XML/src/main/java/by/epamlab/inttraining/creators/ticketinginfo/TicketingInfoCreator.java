package by.epamlab.inttraining.creators.ticketinginfo;

import by.epamlab.inttraining.beans.TicketingInfoType;
import by.epamlab.inttraining.creators.Creator;

public class TicketingInfoCreator implements Creator {
    private static final String TICKET = "eTicket";
    private TicketingInfoType ticketingInfoType;

    public TicketingInfoCreator() {
        this.ticketingInfoType = new TicketingInfoType();
    }

    @Override
    public void create() {
        ticketingInfoType.setTicketType(TICKET);
    }

    public TicketingInfoType getTicketingInfoType() {
        return ticketingInfoType;
    }
}
