package by.epamlab.inttraining.creators.pricinginfo.total;

import by.epamlab.inttraining.beans.TaxType;
import by.epamlab.inttraining.beans.TaxesType;
import by.epamlab.inttraining.creators.Creator;

public class TotalTaxesCreator implements Creator {
    private static final String FIRST_TAX_AMOUNT = "7.38";
    private static final String FIRST_TAX_CURRENCY_CODE = "EUR";
    private static final String FIRST_TAX_TAX_CODE = "AA";
    private static final String SECOND_TAX_AMOUNT = "9.50";
    private static final String SECOND_TAX_CURRENCY_CODE = "EUR";
    private static final String SECOND_TAX_TAX_CODE = "BB";
    private static final String THIRD_TAX_AMOUNT = "26.56";
    private static final String THIRD_TAX_CURRENCY_CODE = "EUR";
    private static final String THIRD_TAX_TAX_CODE = "CC";
    private static final String FOURTH_TAX_AMOUNT = "19.08";
    private static final String FOURTH_TAX_CURRENCY_CODE = "EUR";
    private static final String FOURTH_TAX_TAX_CODE = "DD";
    private TaxesType taxes;

    public TotalTaxesCreator() {
        this.taxes = new TaxesType();
    }

    @Override
    public void create() {
        TaxType firstTax = new TaxType();
        firstTax.setAmount(FIRST_TAX_AMOUNT);
        firstTax.setCurrencyCode(FIRST_TAX_CURRENCY_CODE);
        firstTax.setTaxCode(FIRST_TAX_TAX_CODE);

        TaxType secondTax = new TaxType();
        secondTax.setAmount(SECOND_TAX_AMOUNT);
        secondTax.setCurrencyCode(SECOND_TAX_CURRENCY_CODE);
        secondTax.setTaxCode(SECOND_TAX_TAX_CODE);

        TaxType thirdTax = new TaxType();
        thirdTax.setAmount(THIRD_TAX_AMOUNT);
        thirdTax.setCurrencyCode(THIRD_TAX_CURRENCY_CODE);
        thirdTax.setTaxCode(THIRD_TAX_TAX_CODE);

        TaxType fourthTax = new TaxType();
        fourthTax.setAmount(FOURTH_TAX_AMOUNT);
        fourthTax.setCurrencyCode(FOURTH_TAX_CURRENCY_CODE);
        fourthTax.setTaxCode(FOURTH_TAX_TAX_CODE);

        taxes.getTax().add(firstTax);
        taxes.getTax().add(secondTax);
        taxes.getTax().add(thirdTax);
        taxes.getTax().add(fourthTax);
    }

    public TaxesType getTaxes() {
        return taxes;
    }
}
