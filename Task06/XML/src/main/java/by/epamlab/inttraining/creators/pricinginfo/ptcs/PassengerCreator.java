package by.epamlab.inttraining.creators.pricinginfo.ptcs;

import by.epamlab.inttraining.beans.PassengerType;
import by.epamlab.inttraining.beans.RemoteSystemPTCsType;
import by.epamlab.inttraining.creators.Creator;

public class PassengerCreator implements Creator {
    private static final String PASSENGER_CODE = "ADT";
    private static final String PASSENGER_REFERENCE = "0";
    private static final String PASSENGER_AGE = "35";
    private PassengerType passenger;

    public PassengerCreator() {
        this.passenger = new PassengerType();
    }

    @Override
    public void create() {
        passenger.setPassengerCode(PASSENGER_CODE);
        passenger.setPassengerReference(PASSENGER_REFERENCE);
        passenger.setPassengerAge(PASSENGER_AGE);
        RemoteSystemPTCsType remoteSystemPTCs = new RemoteSystemPTCsType();
        remoteSystemPTCs.setPassengerCode(PASSENGER_CODE);
        passenger.setRemoteSystemPTCs(remoteSystemPTCs);
    }

    public PassengerType getPassenger() {
        return passenger;
    }
}
