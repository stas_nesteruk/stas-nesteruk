package by.epamlab.inttraining.creators.pricinginfo;

import by.epamlab.inttraining.beans.FeeInfoType;
import by.epamlab.inttraining.beans.FeeInfosType;
import by.epamlab.inttraining.creators.Creator;

public class PricingInfoExtensionFeeInfosCreator implements Creator {
    private static final String TAX_CODE = "EE";
    private static final String CURRENCY_CODE = "EUR";
    private FeeInfosType feeInfos;

    public PricingInfoExtensionFeeInfosCreator() {
        this.feeInfos =  new FeeInfosType();
    }

    @Override
    public void create() {
        FeeInfoType feeInfo = new FeeInfoType();
        feeInfo.setTaxCode(TAX_CODE);
        feeInfo.setCurrencyCode(CURRENCY_CODE);
        feeInfos.setFeeInfo(feeInfo);
    }

    public FeeInfosType getFeeInfos() {
        return feeInfos;
    }
}
