package by.epamlab.inttraining.creators.pricinginfo;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.creators.*;

public class PricingInfoCreator implements Creator {
    private static final String SOURCE = "Private";
    private PricingInfoType pricingInfoType;

    private PricingInfoTotalCreator pricingInfoTotal;
    private PricingInfoExtensionCreator pricingInfoExtension;
    private PricingInfoPTCsCreator pricingInfoPTCs;
    private PricingInfoFareInfosCreator pricingInfoFareInfos;

    public PricingInfoCreator() {
        this.pricingInfoType = new PricingInfoType();
        this.pricingInfoTotal = new PricingInfoTotalCreator();
        this.pricingInfoExtension = new PricingInfoExtensionCreator();
        this.pricingInfoPTCs = new PricingInfoPTCsCreator();
        this.pricingInfoFareInfos = new PricingInfoFareInfosCreator();
    }

    @Override
    public void create() {
        pricingInfoTotal.create();
        pricingInfoPTCs.create();
        pricingInfoFareInfos.create();
        pricingInfoExtension.create();
        pricingInfoType.setSource(SOURCE);
        pricingInfoType.setTotal(pricingInfoTotal.getPricingInfoTotal());
        pricingInfoType.setPTCs(pricingInfoPTCs.getPtcs());
        pricingInfoType.setFareInfos(pricingInfoFareInfos.getFareInfos());
        pricingInfoType.setExtension(pricingInfoExtension.getExtensionPricingInfo());
    }

    public PricingInfoType getPricingInfo() {
        return pricingInfoType;
    }
}
