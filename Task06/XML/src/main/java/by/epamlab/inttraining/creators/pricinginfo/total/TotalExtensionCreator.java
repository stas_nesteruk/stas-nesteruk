package by.epamlab.inttraining.creators.pricinginfo.total;

import by.epamlab.inttraining.beans.ExtensionType;
import by.epamlab.inttraining.creators.Creator;
import by.epamlab.inttraining.creators.pricinginfo.PricingInfoExtensionFeeInfosCreator;
import by.epamlab.inttraining.creators.pricinginfo.PricingInfoTaxInfosCreator;

public class TotalExtensionCreator implements Creator {
    private ExtensionType extension;
    private PricingInfoTaxInfosCreator pricingInfoTaxInfos;
    private PricingInfoExtensionFeeInfosCreator pricingInfoExtensionFeeInfos;

    public TotalExtensionCreator() {
        this.extension = new ExtensionType();
        this.pricingInfoTaxInfos = new PricingInfoTaxInfosCreator();
        this.pricingInfoExtensionFeeInfos = new PricingInfoExtensionFeeInfosCreator();
    }

    @Override
    public void create() {
        pricingInfoTaxInfos.create();
        pricingInfoExtensionFeeInfos.create();

        extension.setTaxInfos(pricingInfoTaxInfos.getTaxInfos());
        extension.setFeeInfos(pricingInfoExtensionFeeInfos.getFeeInfos());
    }

    public ExtensionType getExtension() {
        return extension;
    }
}
