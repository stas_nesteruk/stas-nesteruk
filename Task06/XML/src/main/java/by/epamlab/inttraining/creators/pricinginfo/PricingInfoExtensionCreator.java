package by.epamlab.inttraining.creators.pricinginfo;

import by.epamlab.inttraining.beans.ExtensionType;
import by.epamlab.inttraining.beans.ProviderType;
import by.epamlab.inttraining.creators.Creator;

public class PricingInfoExtensionCreator implements Creator {
    private static final String PROVIDER_CODE = "EEEEEEEE";
    private ExtensionType extensionPricingInfo;

    public PricingInfoExtensionCreator() {
        this.extensionPricingInfo = new ExtensionType();
    }

    @Override
    public void create() {
        ProviderType provider = new ProviderType();
        provider.setProviderCode(PROVIDER_CODE);
        extensionPricingInfo.setProvider(provider);
    }

    public ExtensionType getExtensionPricingInfo() {
        return extensionPricingInfo;
    }
}
