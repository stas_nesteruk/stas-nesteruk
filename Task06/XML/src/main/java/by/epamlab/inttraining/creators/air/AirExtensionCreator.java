package by.epamlab.inttraining.creators.air;

import by.epamlab.inttraining.beans.AttributesType;
import by.epamlab.inttraining.beans.ExtensionType;
import by.epamlab.inttraining.creators.Creator;

public class AirExtensionCreator implements Creator {
    private static final String TRIP_TYPE = "OW";
    private ExtensionType airExtension;

    public AirExtensionCreator() {
        this.airExtension = new ExtensionType();
    }

    @Override
    public void create() {
        AttributesType attribute = new AttributesType();
        attribute.setTripType(TRIP_TYPE);
        airExtension.setAttributes(attribute);
    }

    public ExtensionType getAirExtension() {
        return airExtension;
    }
}
