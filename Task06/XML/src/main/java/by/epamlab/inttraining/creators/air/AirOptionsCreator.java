package by.epamlab.inttraining.creators.air;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.creators.Creator;

public class AirOptionsCreator implements Creator {
    private static final String SOURCE = "GDS";
    private static final String NAME = "ABC";
    private OptionsType options;
    private FlightsCreator flights;
    private FlightsExtensionCreator flightsExtension;

    public AirOptionsCreator() {
        this.options = new OptionsType();
        this.flights = new FlightsCreator();
        this.flightsExtension = new FlightsExtensionCreator();
    }

    @Override
    public void create() {
        flights.create();
        flightsExtension.create();
        OptionType option = new OptionType();
        ExtensionType optionExtension = new ExtensionType();
        InventorySystemType inventarySystem = new InventorySystemType();
        inventarySystem.setName(NAME);
        optionExtension.setInventorySystem(inventarySystem);
        optionExtension.setSource(SOURCE);
        option.setExtension(optionExtension);
        for (FlightType flight : flights.getFlights()) {
            option.getFlight().add(flight);
        }
        option.getFlight().get(0).setExtension(flightsExtension.getExtension());
        options.setOption(option);
    }

    public OptionsType getOptions() {
        return options;
    }
}
