package by.epamlab.inttraining.creators.air;

import by.epamlab.inttraining.beans.FlightType;
import by.epamlab.inttraining.creators.Creator;

import java.util.ArrayList;
import java.util.List;

public class FlightsCreator implements Creator {
    private static final String FIRST_FLIGHT_DEPARTURE_DATE_TIME = "2018-12-29T09:00:00.000";
    private static final String FIRST_FLIGHT_ARRIVAL_DATE_TIME = "2018-12-29T10:05:00.000";
    private static final String FIRST_FLIGHT_BOOK_CODE = "K";
    private static final String FIRST_FLIGHT_STOP_QUANTITY = "0";
    private static final String FIRST_FLIGHT_RPH = "0";
    private static final String FIRST_FLIGHT_FLIGHT_NUMBER = "0000";
    private static final String FIRST_FLIGHT_NUMBER_IN_PARTY = "1";
    private static final String ECOND_FLIGHT_DEPARTURE_DATE_TIME = "2018-12-29T11:00:00.000";
    private static final String ECOND_FLIGHT_ARRIVAL_DATE_TIME = "2018-12-29T11:55:00.000";
    private static final String ECOND_FLIGHT_BOOK_CODE = "K";
    private static final String ECOND_FLIGHT_STOP_QUANTITY = "0";
    private static final String ECOND_FLIGHT_RPH = "1";
    private static final String ECOND_FLIGHT_FLIGHT_NUMBER = "1111";
    private static final String ECOND_FLIGHT_NUMBER_IN_PARTY = "1";
    private List<FlightType> flights;
    public FlightsCreator() {
        this.flights = new ArrayList<>();
    }
    @Override
    public void create() {
        FlightType firstFlight = new FlightType();
        firstFlight.setDepartureDateTime(FIRST_FLIGHT_DEPARTURE_DATE_TIME);
        firstFlight.setArrivalDateTime(FIRST_FLIGHT_ARRIVAL_DATE_TIME);
        firstFlight.setBookCode(FIRST_FLIGHT_BOOK_CODE);
        firstFlight.setStopQuantity(FIRST_FLIGHT_STOP_QUANTITY);
        firstFlight.setRPH(FIRST_FLIGHT_RPH);
        firstFlight.setFlightNumber(FIRST_FLIGHT_FLIGHT_NUMBER);
        firstFlight.setNumberInParty(FIRST_FLIGHT_NUMBER_IN_PARTY);

        FlightType secondFlight = new FlightType();
        secondFlight.setDepartureDateTime(ECOND_FLIGHT_DEPARTURE_DATE_TIME);
        secondFlight.setArrivalDateTime(ECOND_FLIGHT_ARRIVAL_DATE_TIME);
        secondFlight.setBookCode(ECOND_FLIGHT_BOOK_CODE);
        secondFlight.setStopQuantity(ECOND_FLIGHT_STOP_QUANTITY);
        secondFlight.setRPH(ECOND_FLIGHT_RPH);
        secondFlight.setFlightNumber(ECOND_FLIGHT_FLIGHT_NUMBER);
        secondFlight.setNumberInParty(ECOND_FLIGHT_NUMBER_IN_PARTY);

        flights.add(firstFlight);
        flights.add(secondFlight);
    }

    public List<FlightType> getFlights() {
        return flights;
    }
}
