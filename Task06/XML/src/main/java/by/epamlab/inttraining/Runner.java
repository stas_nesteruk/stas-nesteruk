package by.epamlab.inttraining;

public class Runner {

    public static void main(String[] args) {
        ObjectToXml objectToXml = new ObjectToXml();
        objectToXml.execute();

        XmlToObject xmlToObject = new XmlToObject();
        xmlToObject.execute();
        xmlToObject.toPrint();
    }
}
