package by.epamlab.inttraining.creators.pricinginfo.fareinfos;

import by.epamlab.inttraining.beans.ExtensionType;
import by.epamlab.inttraining.beans.SegmentType;
import by.epamlab.inttraining.beans.TotalType;
import by.epamlab.inttraining.creators.*;
import by.epamlab.inttraining.creators.pricinginfo.PricingInfoExtensionFeeInfosCreator;
import by.epamlab.inttraining.creators.pricinginfo.PricingInfoTaxInfosCreator;
import by.epamlab.inttraining.creators.pricinginfo.ptcs.PassengerCreator;
import by.epamlab.inttraining.creators.pricinginfo.total.TotalBaseCreator;

public class FareInfoExtensionCreator implements Creator {
    private static final String AMOUNT = "163.52";
    private static final String CURRENCY_CODE = "EUR";
    private static final String CARRIER = "LH";
    private static final String FLIGHT_REFERENCE = "0";
    private ExtensionType extensionFareInfo;
    private TotalBaseCreator pricingInfoBase;
    private PricingInfoTaxInfosCreator pricingInfoTaxInfos;
    private PricingInfoExtensionFeeInfosCreator pricingInfoExtensionFeeInfos;;
    private PassengerCreator passenger;

    public FareInfoExtensionCreator() {
        this.extensionFareInfo = new ExtensionType();
        this.pricingInfoBase = new TotalBaseCreator();
        this.pricingInfoTaxInfos = new PricingInfoTaxInfosCreator();
        this.pricingInfoExtensionFeeInfos = new PricingInfoExtensionFeeInfosCreator();
        this.passenger = new PassengerCreator();
    }

    @Override
    public void create() {
        pricingInfoBase.create();
        pricingInfoTaxInfos.create();
        pricingInfoExtensionFeeInfos.create();
        TotalType total = new TotalType();
        passenger.create();
        total.setAmount(AMOUNT);
        total.setCurrencyCode(CURRENCY_CODE);
        extensionFareInfo.setBase(pricingInfoBase.getBase());
        extensionFareInfo.setTaxInfos(pricingInfoTaxInfos.getTaxInfos());
        extensionFareInfo.setFeeInfos(pricingInfoExtensionFeeInfos.getFeeInfos());
        extensionFareInfo.setTotal(total);

        SegmentType firstSegment = new SegmentType();
        firstSegment.setFlightReference(FLIGHT_REFERENCE);
        SegmentType secondSegment = new SegmentType();
        secondSegment.setFlightReference(FLIGHT_REFERENCE);
        extensionFareInfo.getSegment().add(firstSegment);
        extensionFareInfo.getSegment().add(secondSegment);
        extensionFareInfo.setPassenger(passenger.getPassenger());
        extensionFareInfo.setCarrier(CARRIER);
    }

    public ExtensionType getExtensionFareInfo() {
        return extensionFareInfo;
    }
}
