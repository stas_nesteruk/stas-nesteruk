package by.epamlab.inttraining.creators.pricinginfo;

import by.epamlab.inttraining.beans.FareInfoType;
import by.epamlab.inttraining.beans.FareInfosType;
import by.epamlab.inttraining.creators.Creator;
import by.epamlab.inttraining.creators.pricinginfo.fareinfos.FareInfoExtensionCreator;

public class PricingInfoFareInfosCreator implements Creator {
    private static final String DEPARTURE_DATE = "2018-12-29T09:00:00.000";
    private static final String FARE_REFERENCE = "AAAAAAA";
    private FareInfosType fareInfos;
    private FareInfoExtensionCreator fareInfoExtension;

    public PricingInfoFareInfosCreator() {
        this.fareInfos = new FareInfosType();
        this.fareInfoExtension = new FareInfoExtensionCreator();
    }

    @Override
    public void create() {
        fareInfoExtension.create();
        FareInfoType fareInfo = new FareInfoType();
        fareInfo.setDepartureDate(DEPARTURE_DATE);
        fareInfo.setFareReference(FARE_REFERENCE);
        fareInfo.setExtension(fareInfoExtension.getExtensionFareInfo());
        fareInfos.setFareInfo(fareInfo);
    }

    public FareInfosType getFareInfos() {
        return fareInfos;
    }
}
