package by.epamlab.inttraining.creators.air;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.creators.Creator;

public class AirCreator implements Creator {
    private static final String WAY = "OneWay";
    private static final String TOTAL_TRAVEL_TIME = "PT2H55M";
    private AirType air;
    private AirOptionsCreator airOptions;
    private AirExtensionCreator airExtension;

    public AirCreator() {
        this.air = new AirType();
        this.airOptions = new AirOptionsCreator();
        this.airExtension = new AirExtensionCreator();
    }

    @Override
    public void create() {
        airOptions.create();
        airExtension.create();
        air.setDirection(WAY);
        air.setTotalTravelTime(TOTAL_TRAVEL_TIME);
        air.setOptions(airOptions.getOptions());
        air.setExtension(airExtension.getAirExtension());
    }

    public AirType getAir() {
        return air;
    }
}
