package by.epamlab.inttraining.creators.pricinginfo;

import by.epamlab.inttraining.beans.TotalType;
import by.epamlab.inttraining.creators.*;
import by.epamlab.inttraining.creators.pricinginfo.total.TotalBaseCreator;
import by.epamlab.inttraining.creators.pricinginfo.total.TotalExtensionCreator;
import by.epamlab.inttraining.creators.pricinginfo.total.TotalFeesCreator;
import by.epamlab.inttraining.creators.pricinginfo.total.TotalTaxesCreator;

public class PricingInfoTotalCreator implements Creator {
    private static final String AMOUNT = "163.52";
    private static final String CURRENCY_CODE = "EUR";
    private TotalType pricingInfoTotal;
    private TotalTaxesCreator pricingInfoTaxes;
    private TotalBaseCreator pricingInfoBase;
    private TotalFeesCreator pricingInfoFees;
    private TotalExtensionCreator pricingInfoExtension;

    public PricingInfoTotalCreator() {
        this.pricingInfoTotal = new TotalType();
        this.pricingInfoTaxes = new TotalTaxesCreator();
        this.pricingInfoBase = new TotalBaseCreator();
        this.pricingInfoFees = new TotalFeesCreator();
        this.pricingInfoExtension = new TotalExtensionCreator();
    }

    @Override
    public void create() {
        pricingInfoTaxes.create();
        pricingInfoBase.create();
        pricingInfoFees.create();
        pricingInfoExtension.create();

        TotalType total = new TotalType();
        total.setAmount(AMOUNT);
        total.setCurrencyCode(CURRENCY_CODE);

        pricingInfoTotal.setTaxes(pricingInfoTaxes.getTaxes());
        pricingInfoTotal.setBase(pricingInfoBase.getBase());
        pricingInfoTotal.setFees(pricingInfoFees.getFees());
        pricingInfoTotal.setTotal(total);
        pricingInfoTotal.setExtension(pricingInfoExtension.getExtension());
    }

    public TotalType getPricingInfoTotal() {
        return pricingInfoTotal;
    }
}
