package by.epamlab.inttraining.creators.air;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.creators.Creator;

public class FlightsExtensionCreator implements Creator {
    private static final String NAME = "ABC";
    private static final String FLYING_TIME = "PT1H5M";
    private static final String BOOK_CODE = "K";
    private static final String BOOK_DESIG = "9";
    private static final String CABIN = "Economy";
    private static final String ETICKET = "E";
    private static final String TERMINAL = "1";
    private ExtensionType extension;

    public FlightsExtensionCreator() {
        this.extension = new ExtensionType();
    }

    @Override
    public void create() {
        InventorySystemType inventarySystem = new InventorySystemType();
        inventarySystem.setName(NAME);

        FlightSupplementalInfoType flightSupplementalInfo = new FlightSupplementalInfoType();
        flightSupplementalInfo.setFlyingTime(FLYING_TIME);

        extension.setFlightSupplementalInfo(flightSupplementalInfo);
        BookingClassAvailabilityType bookingClassAvailability = new BookingClassAvailabilityType();
        bookingClassAvailability.setBookCode(BOOK_CODE);
        bookingClassAvailability.setBookDesig(BOOK_DESIG);
        bookingClassAvailability.setCabin(CABIN);

        extension.setBookingClassAvailability(bookingClassAvailability);
        extension.setEticket(ETICKET);

        TerminalInformationType terminalInformation = new TerminalInformationType();
        terminalInformation.setTerminal(TERMINAL);

        extension.setTerminalInformation(terminalInformation);
        extension.setInventorySystem(inventarySystem);
    }

    public ExtensionType getExtension() {
        return extension;
    }
}
