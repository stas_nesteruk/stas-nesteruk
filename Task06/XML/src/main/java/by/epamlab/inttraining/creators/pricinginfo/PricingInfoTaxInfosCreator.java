package by.epamlab.inttraining.creators.pricinginfo;

import by.epamlab.inttraining.beans.TaxInfoType;
import by.epamlab.inttraining.beans.TaxInfosType;
import by.epamlab.inttraining.creators.Creator;

public class PricingInfoTaxInfosCreator implements Creator {
    private static final String TAX_CODE_AA = "AA";
    private static final String TAX_CODE_BB = "BB";
    private static final String TAX_CODE_CC = "CC";
    private static final String TAX_CODE_DD = "DD";
    private static final String EUR = "EUR";
    private TaxInfosType taxInfos;

    public PricingInfoTaxInfosCreator() {
        this.taxInfos = new TaxInfosType();
    }

    @Override
    public void create() {
        TaxInfoType firstTaxInfo = new TaxInfoType();
        firstTaxInfo.setTaxCode(TAX_CODE_AA);
        firstTaxInfo.setBaseCurrency(EUR);

        TaxInfoType secondTaxInfo = new TaxInfoType();
        secondTaxInfo.setTaxCode(TAX_CODE_BB);
        secondTaxInfo.setBaseCurrency(EUR);

        TaxInfoType thirdTaxInfo = new TaxInfoType();
        thirdTaxInfo.setTaxCode(TAX_CODE_CC);
        thirdTaxInfo.setBaseCurrency(EUR);

        TaxInfoType fourthTaxInfo = new TaxInfoType();
        fourthTaxInfo.setTaxCode(TAX_CODE_DD);
        fourthTaxInfo.setBaseCurrency(EUR);

        taxInfos.getTaxInfo().add(firstTaxInfo);
        taxInfos.getTaxInfo().add(secondTaxInfo);
        taxInfos.getTaxInfo().add(thirdTaxInfo);
        taxInfos.getTaxInfo().add(fourthTaxInfo);
    }

    public TaxInfosType getTaxInfos() {
        return taxInfos;
    }
}
