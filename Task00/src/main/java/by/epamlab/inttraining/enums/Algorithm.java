package by.epamlab.inttraining.enums;

import by.epamlab.inttraining.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum  Algorithm {
    FIBONACHI{
        public void execute(int number, int loopType){
            List<Integer> fibonachiList = new ArrayList<>();
            int value;
            int count;
            if (number == 0){
                fibonachiList.add(Constants.ZERO_VALUE_FIBONACHI);
            }else {
                if (number == 1) {
                    fibonachiList.add(Constants.FIRST_VALUE_FIBONACHI);
                } else {
                    fibonachiList.add(Constants.ZERO_VALUE_FIBONACHI);
                    fibonachiList.add(Constants.FIRST_VALUE_FIBONACHI);
                    switch (Loop.byOrdinal(loopType)) {
                        case WHILE:
                            count = fibonachiList.size();
                            while (count < number) {
                                value = fibonachiList.get(count - 1) + fibonachiList.get(count - 2);
                                fibonachiList.add(value);
                                count++;
                            }
                            break;
                        case DOWHILE:
                            count = fibonachiList.size();
                            do {
                                value = fibonachiList.get(count - 1) + fibonachiList.get(count - 2);
                                fibonachiList.add(value);
                                count++;
                            } while (count < number);
                            break;
                        case FOR:
                            for (int i = 2; i < number; i++) {
                                value = fibonachiList.get(i - 1) + fibonachiList.get(i - 2);
                                fibonachiList.add(value);
                            }
                            break;
                        default:
                            LOGGER.log(Level.SEVERE, "Fibonachi: Invalid loop type...");
                            break;
                    }
                }
            }
            System.out.println("Fibonacci sequence:");
            for (int i : fibonachiList){
                System.out.print(i + " ");
            }
        }
    },
    FACTORIAL{
        @Override
        public void execute(int number, int loopType){
            long result = 1;
            switch (Loop.byOrdinal(loopType)){
                case WHILE:
                    while (number > 1){
                        result *= number;
                        number--;
                    }
                    break;
                case DOWHILE:
                    if(number == 0) System.out.println("Factorial equals: " + result);
                    do{
                        result *= number;
                        number--;
                    }while (number > 1);
                    break;
                case FOR:
                    for(int i = 1; i <= number; i++){
                        result *= i;
                    }
                    break;
                default:
                    LOGGER.log(Level.SEVERE,"Factorial: Invalid loop type...");
                    break;
            }
            System.out.println("Factorial equals: " + result);
        }
    };
    public abstract void execute(int number, int loopType);

    private static final Logger LOGGER = Logger.getLogger(Algorithm.class.getName());

}
