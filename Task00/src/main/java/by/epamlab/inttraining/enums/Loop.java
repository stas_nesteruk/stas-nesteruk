package by.epamlab.inttraining.enums;

public enum Loop {
    WHILE(1), DOWHILE(2), FOR(3);

    private int id;

    Loop(int id){
        this.id = id;
    }

    public static Loop byOrdinal(int id){
        return Loop.values()[id - 1];
    }
}
