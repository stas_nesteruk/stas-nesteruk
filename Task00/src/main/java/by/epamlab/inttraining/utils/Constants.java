package by.epamlab.inttraining.utils;

import by.epamlab.inttraining.enums.Algorithm;
import by.epamlab.inttraining.enums.Loop;

public class Constants {
    public static final int ZERO_VALUE_FIBONACHI = 0;
    public static final int FIRST_VALUE_FIBONACHI = 1;
    public static final String DESCRIPTION_MESSAGE = "Please enter value from %d to %d:\n";
    public static final String ALGORITHM_DESCRIPTION_MESSAGE = "Enter algorithm number:";
    public static final String ALGORITHM_DESCRIPTION_ERROR_MESSAGE = "Invalid algorithm number,";
    public static final int ALGORITHM_MINIMUM_VALUE = 1;
    public static final int ALGORITHM_MAXIMUM_VALUE = Algorithm.values().length;
    public static final String LOOPTYPE_DESCRIPTION_MESSAGE = "Enter loop type:";
    public static final String LOOPTYPE_DESCRIPTION_ERROR_MESSAGE = "Invalid loop type number,";
    public static final int LOOPTYPE_MINIMUM_VALUE = 1;
    public static final int LOOPTYPE_MAXIMUM_VALUE = Loop.values().length;
    public static final String NUMBER_DESCRIPTION_MESSAGE = "Enter number:";
    public static final String NUMBER_DESCRIPTION_ERROR_MESSAGE = "Number, should not be less than zero,";
    public static final int NUMBER_MINIMUM_VALUE = 0;
    public static final int NUMBER_MAXIMUM_VALUE = 20;
}
