import by.epamlab.inttraining.enums.Algorithm;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import static by.epamlab.inttraining.utils.Constants.*;

public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());
    private static Scanner scanner = new Scanner(System.in);

    private static int insertData(String message, int minValue, int maxValue, String errorMessage){
        System.out.println(message);
        int value;
        while(true){
            try{
                value = scanner.nextInt();
                if(value < minValue || value > maxValue){
                    System.out.printf(DESCRIPTION_MESSAGE, minValue, maxValue);
                    continue;
                }else{
                    break;
                }
            }catch (InputMismatchException ex){
                System.out.println(errorMessage + " try again:");
                scanner.nextLine();
            }
        }
        return value;
    }


    public static void main(String[] args) {
        int algorithmId = insertData(ALGORITHM_DESCRIPTION_MESSAGE,
                ALGORITHM_MINIMUM_VALUE,
                ALGORITHM_MAXIMUM_VALUE,
                ALGORITHM_DESCRIPTION_ERROR_MESSAGE);
        int loopType = insertData(LOOPTYPE_DESCRIPTION_MESSAGE,
                LOOPTYPE_MINIMUM_VALUE,
                LOOPTYPE_MAXIMUM_VALUE,
                LOOPTYPE_DESCRIPTION_ERROR_MESSAGE);
        int number = insertData(NUMBER_DESCRIPTION_MESSAGE,
                NUMBER_MINIMUM_VALUE,
                NUMBER_MAXIMUM_VALUE,
                NUMBER_DESCRIPTION_ERROR_MESSAGE);
        switch (algorithmId){
            case 1:
                Algorithm.FIBONACHI.execute(number, loopType);
                break;
            case 2:
                Algorithm.FACTORIAL.execute(number, loopType);
                break;
            default:
                LOGGER.log(Level.SEVERE,"Invalid algorithm type..");
                break;
        }
    }

}
