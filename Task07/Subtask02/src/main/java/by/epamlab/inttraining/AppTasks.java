package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.Author;
import by.epamlab.inttraining.beans.Book;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class AppTasks {
    private static final int TWO_HUNDRED = 200;
    private static Set<Book> books;

    static {
        DataLoader.load();
        books = DataLoader.getBooks();
    }

    public static boolean checkIsHaveBookWithMoreThen200Pages() {
        return books.stream().anyMatch(x -> x.getNumberOfPages() > TWO_HUNDRED);
    }

    public static boolean checkIsAllBookWithMoreThen200Pages() {
        return books.stream().noneMatch(x -> x.getNumberOfPages() < TWO_HUNDRED);
    }

    public static Map<String, List<Book>> getBooksWithMaxAndMinPages() {
        Map<String, List<Book>> result = new HashMap<>();
        List<Book> booksWithMaxCountPages = books.stream().collect(groupingBy(Book::getNumberOfPages, TreeMap::new, toList()))
                .lastEntry()
                .getValue();

        List<Book> booksWithMinCountPages = books.stream().collect(groupingBy(Book::getNumberOfPages, TreeMap::new, toList()))
                .firstEntry()
                .getValue();
        result.put("Books with minimum pages:", booksWithMinCountPages);
        result.put("Books with maximum pages:", booksWithMaxCountPages);
        return result;
    }

    public static List<Book> getBooksWithOneAuthor() {
        return books.stream().collect(groupingBy(book -> book.getAuthors().size() == 1, TreeMap::new, toList()))
                .lastEntry().getValue();
    }

    public static List<Book> getSortedBooksByPagesAndThenByName() {
        return books.stream().sorted(Comparator.comparing(Book::getNumberOfPages).thenComparing(Book::getName))
                .collect(Collectors.toList());
    }

    public static Set<Book> getListOfBookNames() {
        return books;
    }

    public static List<String> getListOfAuthorWithBookLess200Pages() {
        return books.stream().filter(book -> book.getNumberOfPages() < TWO_HUNDRED)
                .map(Book::getAuthors)
                .flatMap(Collection::stream)
                .map(Author::getName)
                .distinct()
                .collect(Collectors.toList());
    }

}
