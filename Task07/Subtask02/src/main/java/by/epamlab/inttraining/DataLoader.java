package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.Author;
import by.epamlab.inttraining.beans.Book;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class DataLoader {
    private static final Logger LOGGER = Logger.getLogger(DataLoader.class);
    private static final String FILE_EXTENSION = ".txt";
    private static final String LIST_OF_AUTHORS = "ListOfAuthors";
    private static final String PATH = "Subtask02\\src\\main\\resources\\";
    private static final String LIST_OF_BOOKS = "ListOfBooks";
    private static final int MINIMUM_AGE_LIMIT = 30;
    private static final int BOUND_AGE_LIMIT = 51;
    private static final int FILTER_DOWN_AGE = 21;
    private static final int FILTER_UP_AGE = 31;
    private static final int BOUND_FILTER_UP_AGE = 50;
    private static final int MAXIMUM_COUNT_AUTHORS = 30;
    private static final int MINIMUM_BOOK_PAGES_LIMIT = 50;
    private static final int BOUND_BOOK_PAGES_LIMIT = 301;
    private static final int MINIMUM_COUNT_BOOKS = 2;
    private static final int BOUND_COUNT_BOOKS = 11;
    private static Set<Author> allAuthors = new HashSet<>();
    private static Set<Book> books = new HashSet<>();

    private DataLoader() {
    }

    public static void load() {
        try {
            List<String> listOfAuthors = Files.readAllLines(Paths.get(PATH + LIST_OF_AUTHORS + FILE_EXTENSION));
            List<String> listOfBooks = Files.readAllLines(Paths.get(PATH + LIST_OF_BOOKS + FILE_EXTENSION));
            Random random = new Random();
            listOfAuthors.forEach(x -> {
                int authorAge = random.nextInt(BOUND_AGE_LIMIT) + MINIMUM_AGE_LIMIT;
                Author author = new Author(x, authorAge);
                allAuthors.add(author);
            });
            int filterAgeUP = random.nextInt(FILTER_UP_AGE) + BOUND_FILTER_UP_AGE;
            int filterAgeDown = random.nextInt(FILTER_DOWN_AGE) + MINIMUM_AGE_LIMIT;
            Set<Author> randomAuthors = allAuthors.stream()
                    .filter(author -> author.getAge() > filterAgeDown && author.getAge() < filterAgeUP)
                    .limit(MAXIMUM_COUNT_AUTHORS)
                    .collect(Collectors.toSet());

            randomAuthors.forEach(author -> {
                int countBook = random.nextInt(BOUND_COUNT_BOOKS) + MINIMUM_COUNT_BOOKS;
                Set<Book> authorBooks = new HashSet<>();
                for (int j = 0; j < countBook; j++) {
                    int idBook = random.nextInt(listOfBooks.size());
                    String bookName = listOfBooks.get(idBook);
                    int numberOfPages = random.nextInt(BOUND_BOOK_PAGES_LIMIT) + MINIMUM_BOOK_PAGES_LIMIT;
                    Book book = new Book(bookName, numberOfPages);
                    book.getAuthors().add(author);
                    authorBooks.add(book);
                }
                author.setBooks(authorBooks);
                authorBooks.forEach(book -> {
                    final Optional<Book> findBook = books.stream()
                            .filter(x -> x.getName().equals(book.getName()))
                            .findFirst();
                    if (findBook.isPresent()) {
                        book.getAuthors().forEach(y -> findBook.get().getAuthors().add(y));
                    } else {
                        books.add(book);
                    }
                });
            });
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public static Set<Book> getBooks() {
        return books;
    }
}
