package by.epamlab.inttraining.beans;

import java.util.Set;

public class Author {
    private String name;
    private int Age;
    private Set<Book> books;

    public Author() {
    }

    public Author(String name, int age) {
        this.name = name;
        Age = age;
    }

    public Author(String name, int age, Set<Book> books) {
        this.name = name;
        Age = age;
        this.books = books;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return name + ";" + Age + ";";
    }

}
