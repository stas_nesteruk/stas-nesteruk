package by.epamlab.inttraining.beans;

import java.util.HashSet;
import java.util.Set;

public class Book {
    private String name;
    private int numberOfPages;
    private Set<Author> authors;

    public Book() {
    }

    public Book(String name, int numberOfPages) {
        this.name = name;
        this.numberOfPages = numberOfPages;
        this.authors = new HashSet<>();
    }

    public Book(String name, int numberOfPages, Set<Author> authors) {
        this.name = name;
        this.numberOfPages = numberOfPages;
        this.authors = authors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return name + ";" + numberOfPages + ";";
    }
}
