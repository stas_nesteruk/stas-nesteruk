package by.epamlab.inttraining;

import org.apache.log4j.Logger;

public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        LOGGER.info("Is have books with more than 200 pages:");
        LOGGER.info(AppTasks.checkIsHaveBookWithMoreThen200Pages());

        LOGGER.info("Is have all books more than 200 pages:");
        LOGGER.info(AppTasks.checkIsAllBookWithMoreThen200Pages());

        AppTasks.getBooksWithMaxAndMinPages().forEach((msg, books) -> {
            LOGGER.info(msg);
            books.forEach(LOGGER::info);
        });

        LOGGER.info("Books with one author:");
        AppTasks.getBooksWithOneAuthor().forEach(LOGGER::info);

        LOGGER.info("Sorted books by pages and then by names:");
        AppTasks.getSortedBooksByPagesAndThenByName().forEach(x -> LOGGER.info(String.format("%-35s%d", x.getName(), x.getNumberOfPages())));

        LOGGER.info("Book names:");
        AppTasks.getListOfBookNames().forEach(book -> LOGGER.info(book.getName()));

        LOGGER.info("Author names with books less 200 pages");
        AppTasks.getListOfAuthorWithBookLess200Pages().forEach(LOGGER::info);
    }
}
