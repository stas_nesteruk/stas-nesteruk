package by.epamlab.inttraining.beans.phones.apple;

import by.epamlab.inttraining.beans.Battery;

public class AppleBattery implements Battery {
    private int capacity;
    private String name;

    public AppleBattery(String capacity) {
        String[] lines = capacity.split(" ");
        this.capacity = Integer.parseInt(lines[0]);
        this.name = lines[1];
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public String toString() {
        return String.valueOf(capacity) + " " + name;
    }
}
