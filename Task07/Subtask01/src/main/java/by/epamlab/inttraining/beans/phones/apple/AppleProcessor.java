package by.epamlab.inttraining.beans.phones.apple;

import by.epamlab.inttraining.beans.Processor;

public class AppleProcessor implements Processor {
    private String model;

    public AppleProcessor(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return model;
    }

    @Override
    public String getModel() {
        return model;
    }
}
