package by.epamlab.inttraining.beans;

public class Phone {
    private String name;
    private int price;
    private Battery battery;
    private Display display;
    private Processor processor;

    public Phone(String name, int price, Battery battery, Display display, Processor processor) {
        this.name = name;
        this.price = price;
        this.battery = battery;
        this.display = display;
        this.processor = processor;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public Battery getBattery() {
        return battery;
    }

    public Display getDisplay() {
        return display;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setBattery(Battery battery) {
        this.battery = battery;
    }

    public void setDisplay(Display display) {
        this.display = display;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(name).append(';');
        sb.append(price).append(';');
        sb.append(battery).append(';');
        sb.append(display).append(';');
        sb.append(processor).append(';');
        return sb.toString();
    }
}
