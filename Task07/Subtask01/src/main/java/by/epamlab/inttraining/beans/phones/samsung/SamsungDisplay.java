package by.epamlab.inttraining.beans.phones.samsung;

import by.epamlab.inttraining.beans.Display;

public class SamsungDisplay implements Display {
    private String size;
    private String resolution;

    public SamsungDisplay(String size, String resolution) {
        this.size = size;
        this.resolution = resolution;
    }

    @Override
    public String toString() {
        return size + ';' + resolution;
    }

    @Override
    public String getSize() {
        return size;
    }

    @Override
    public String getResolution() {
        return resolution;
    }
}
