package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.Phone;
import by.epamlab.inttraining.beans.Processor;
import by.epamlab.inttraining.utils.Constants;
import by.epamlab.inttraining.utils.Custom;

import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.TreeMap;
import java.util.function.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class AppTasks {
    private static final Comparator<Phone> SIMPLE_COMPARATOR = Comparator.comparing(Phone::getName);
    private static final Comparator<Phone> DIFFICULT_COMPARATOR = Comparator.comparing(Phone::getName).
            thenComparing(Phone::getPrice);

    public static List<Phone> getSortedPhonesSimpleComparator() {
        return Constants.phones.stream().sorted(SIMPLE_COMPARATOR).collect(toList());
    }

    public static List<Phone> getSortedPhonesDifficultComparator() {
        return Constants.phones.stream().sorted(DIFFICULT_COMPARATOR).collect(toList());
    }

    public static final Function<List<Phone>, List<Phone>> PHONES_WITH_MAX_BATTERY_CAPACITY = list -> list.stream()
            .collect(Collectors.groupingBy(x -> x.getBattery().getCapacity(), TreeMap::new, Collectors.toList()))
            .lastEntry()
            .getValue();

    public static final UnaryOperator<List<Phone>> NAME_TO_UPPER_CASE = list -> list.stream()
            .peek(x -> x.setName(x.getName().toUpperCase()))
            .collect(Collectors.toList());

    public static final BiConsumer<Phone, Processor> CHANGE_PROCESSOR = Phone::setProcessor;

    public static final String FULL_HD = "1080x1920";
    public static final Predicate<Phone> IS_HAVE_FULL_HD_DISPLAY = value -> value.toString()
            .contains(FULL_HD);

    public static final Supplier<List<String>> ALL_DISPLAY_RESOLUTION = () -> Constants.phones
            .stream()
            .map(x -> x.getDisplay().getResolution())
            .distinct()
            .collect(Collectors.toList());

    public static final Custom<String, OptionalDouble> AVERAGE_COST_BY_NAME = (list, name) -> list.stream()
            .filter(x -> x.getName().contains(name))
            .mapToInt(Phone::getPrice)
            .average();

    public static final Custom<Integer, List<Phone>> CHANGE_PRICE = (list, price) -> list.stream()
            .map(x -> new Phone(x.getName(),
                    x.getPrice() + price,
                    x.getBattery(),
                    x.getDisplay(),
                    x.getProcessor()))
            .collect(Collectors.toList());

    public static final Custom<Integer, List<String>> PHONES_MORE_EXPENSIVE_BY_PRICE = (list, price) -> list.stream()
            .filter(x -> x.getPrice() >= price)
            .map(Phone::getName)
            .collect(Collectors.toList());
}
