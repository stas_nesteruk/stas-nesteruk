package by.epamlab.inttraining.utils;


import by.epamlab.inttraining.beans.Phone;
import org.apache.log4j.Logger;

import java.util.List;

@FunctionalInterface
public interface Custom<U, R> {
    Logger LOGGER = Logger.getLogger(Custom.class);
    String FULL_HD = "1080x1920";

    default void print() {
        LOGGER.info(this);
    }

    R apply(List<Phone> phones, U u);

    static boolean isHaveFullHD(Phone phone) {
        return phone.toString().contains(FULL_HD);
    }

    static void compareCharacteristics(Phone first, Phone second) {
        LOGGER.info("~~~~~~~~~~~~~~~~~~~~~Characteristics~~~~~~~~~~~~~~~~~~~~~~");
        String formatHeader = "\t\t%s\t\t\t%s";
        String formatBody = "Value: %-22sValue: %s";
        String[] firstParam = first.toString().split(";");
        String[] secondParam = second.toString().split(";");
        LOGGER.info(String.format(formatHeader, first.getName(), second.getName()));
        LOGGER.info(String.format(formatBody, firstParam[3], secondParam[3]));
        LOGGER.info(String.format(formatBody, firstParam[4], secondParam[4]));
        LOGGER.info(String.format(formatBody, firstParam[5], secondParam[5]));
        LOGGER.info(String.format(formatBody, firstParam[2], secondParam[2]));
        LOGGER.info(String.format(formatBody, firstParam[1], secondParam[1]));
    }
}
