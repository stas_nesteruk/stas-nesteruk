package by.epamlab.inttraining.beans;

public interface Processor {
    String getModel();
}
