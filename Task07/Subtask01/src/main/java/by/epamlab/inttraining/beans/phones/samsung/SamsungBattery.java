package by.epamlab.inttraining.beans.phones.samsung;

import by.epamlab.inttraining.beans.Battery;

public class SamsungBattery implements Battery {
    private int capacity;
    private String name;

    public SamsungBattery(String capacity) {
        String[] lines = capacity.split(" ");
        this.capacity = Integer.parseInt(lines[0]);
        this.name = lines[1];
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public String toString() {
        return String.valueOf(capacity) + " " + name;
    }
}
