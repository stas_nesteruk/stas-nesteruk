package by.epamlab.inttraining.beans;

public interface Battery {
    int getCapacity();
}
