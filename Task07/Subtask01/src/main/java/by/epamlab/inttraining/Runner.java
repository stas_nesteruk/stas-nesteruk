package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.Phone;
import by.epamlab.inttraining.beans.phones.apple.AppleProcessor;
import by.epamlab.inttraining.utils.Constants;
import by.epamlab.inttraining.utils.Custom;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class);
    private static Consumer<String> printer = LOGGER::info;
    private static Consumer<List<Phone>> listPrinter = phones ->
            phones.forEach(phone -> printer.accept(phone.toString()));

    public static void main(String[] args) {
        printer.accept("Sorted with simple comparator(by name):");
        listPrinter.accept(AppTasks.getSortedPhonesSimpleComparator());

        printer.accept("\n");
        printer.accept("Sorted with difficult comparator(by name and price):");
        listPrinter.accept(AppTasks.getSortedPhonesDifficultComparator());

        printer.accept("\n");
        printer.accept("Phones with max battery value:");
        listPrinter.accept(AppTasks.PHONES_WITH_MAX_BATTERY_CAPACITY.apply(Constants.phones));

        printer.accept("\n");
        printer.accept("Name to upper case");
        listPrinter.accept(AppTasks.NAME_TO_UPPER_CASE.apply(Constants.phones));

        printer.accept("\n");
        printer.accept("Phone with full hd display");
        listPrinter.accept(Constants.phones.stream()
                .filter(AppTasks.IS_HAVE_FULL_HD_DISPLAY)
                .collect(Collectors.toList()));

        printer.accept("\n");
        printer.accept("Average price of Samsung phones:");
        AppTasks.AVERAGE_COST_BY_NAME.apply(Constants.phones, "Samsung")
                .ifPresent(x -> printer.accept(String.format("%.2f", x)));

        printer.accept("\n");
        printer.accept("Change price on 100");
        listPrinter.accept(AppTasks.CHANGE_PRICE.apply(Constants.phones, 100));

        printer.accept("\n");
        printer.accept("Phones with price more 400:");
        AppTasks.PHONES_MORE_EXPENSIVE_BY_PRICE.apply(Constants.phones, 400).forEach(LOGGER::info);

        Phone phone = Constants.phones.get(4);
        printer.accept("\n");
        printer.accept("Check is have full hd scree in: " + phone.toString());
        printer.accept(String.valueOf(Custom.isHaveFullHD(phone)));
        Custom.compareCharacteristics(phone, phone);

        printer.accept("\n");
        printer.accept("All display resolution:");
        AppTasks.ALL_DISPLAY_RESOLUTION.get().forEach(LOGGER::info);

        printer.accept("\n");
        printer.accept("Change processor in phone: " + phone);
        AppTasks.CHANGE_PROCESSOR.accept(phone, new AppleProcessor("CUSTOM X200"));
        printer.accept(phone.toString());
    }
}
