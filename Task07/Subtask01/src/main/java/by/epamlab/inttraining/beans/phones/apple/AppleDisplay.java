package by.epamlab.inttraining.beans.phones.apple;

import by.epamlab.inttraining.beans.Display;

public class AppleDisplay implements Display {
    private String size;
    private String resolution;

    public AppleDisplay(String size, String resolution) {
        this.size = size;
        this.resolution = resolution;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(size).append(';');
        sb.append(resolution);
        return sb.toString();
    }

    @Override
    public String getSize() {
        return size;
    }

    @Override
    public String getResolution() {
        return resolution;
    }
}
