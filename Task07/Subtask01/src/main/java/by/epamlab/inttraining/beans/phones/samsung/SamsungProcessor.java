package by.epamlab.inttraining.beans.phones.samsung;

import by.epamlab.inttraining.beans.Processor;

public class SamsungProcessor implements Processor {
    private String model;

    public SamsungProcessor(String model) {
        this.model = model;
    }


    @Override
    public String toString() {
        return model;
    }

    @Override
    public String getModel() {
        return model;
    }
}
