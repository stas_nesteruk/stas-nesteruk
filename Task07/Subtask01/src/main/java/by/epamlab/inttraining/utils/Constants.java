package by.epamlab.inttraining.utils;


import by.epamlab.inttraining.beans.Battery;
import by.epamlab.inttraining.beans.Display;
import by.epamlab.inttraining.beans.Phone;
import by.epamlab.inttraining.beans.Processor;
import by.epamlab.inttraining.beans.phones.apple.AppleBattery;
import by.epamlab.inttraining.beans.phones.apple.AppleDisplay;
import by.epamlab.inttraining.beans.phones.apple.AppleProcessor;
import by.epamlab.inttraining.beans.phones.samsung.SamsungBattery;
import by.epamlab.inttraining.beans.phones.samsung.SamsungDisplay;
import by.epamlab.inttraining.beans.phones.samsung.SamsungProcessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Constants {
    public static List<Phone> phones = new ArrayList<>();
    public static List<String> phoneNames = new ArrayList<>();
    public static List<Display> displays = new ArrayList<>(Arrays.asList(
            new SamsungDisplay("4.7\"", "750x1334"),
            new SamsungDisplay("6.4\"", "1080x2340"),
            new SamsungDisplay("6.7\"", "1080x2400"),
            new SamsungDisplay("5.9\"", "1080x2340"),
            new SamsungDisplay("6.2\"", "720x1520"),
            new AppleDisplay("5.8\"", "1125x2436"),
            new AppleDisplay("6.1\"", "828x1792"),
            new AppleDisplay("6.5\"", "1242x2688"),
            new AppleDisplay("4.7\"", "750x1334"),
            new AppleDisplay("5.5\"", "1080x1920")));

    public static List<Battery> batteries = new ArrayList<>(Arrays.asList(
            new SamsungBattery("4000 мАч"),
            new SamsungBattery("4500 мАч"),
            new SamsungBattery("3100 мАч"),
            new SamsungBattery("3400 мАч"),
            new AppleBattery("3190 мАч"),
            new AppleBattery("3046 мАч"),
            new AppleBattery("2942 мАч"),
            new AppleBattery("4500 мАч")));

    public static List<Processor> processors = new ArrayList<>(Arrays.asList(
            new SamsungProcessor("Exynos 9610"),
            new SamsungProcessor("Qualcomm Snapdragon 675"),
            new SamsungProcessor("Exynos 7904"),
            new SamsungProcessor("Exynos 7884"),
            new SamsungProcessor("Exynos 9820"),
            new AppleProcessor("Apple A13 Bionic"),
            new AppleProcessor("Apple A12 Bionic"),
            new AppleProcessor("Apple A11 Bionic"),
            new AppleProcessor("Apple A10 Fusion"),
            new AppleProcessor("Apple A9")));

    static {
        phoneNames.add("Samsung Galaxy A50");
        phoneNames.add("Apple iPhone 11 Pro");
        phoneNames.add("Apple iPhone 11");
        phoneNames.add("Samsung Galaxy A10");
        phoneNames.add("Samsung Galaxy S10e");
        phoneNames.add("Apple iPhone XR");
        phoneNames.add("Samsung Galaxy A70");
        phoneNames.add("Samsung Galaxy S10");
        phoneNames.add("Apple iPhone 7");
        phoneNames.add("Apple iPhone 6s");

        Random random = new Random();
        int price;
        String name;
        Display display;
        Battery battery;
        Processor processor;

        for (int i = 0; i < 20; i++) {
            name = phoneNames.get(random.nextInt(phoneNames.size()));
            price = random.nextInt(300) + 200;
            if (name.contains("Samsung")) {
                display = displays.get(random.nextInt(5));
                battery = batteries.get(random.nextInt(4));
                processor = processors.get(random.nextInt(5));
            } else {
                display = displays.get(random.nextInt(5) + 5);
                battery = batteries.get(random.nextInt(4) + 4);
                processor = processors.get(random.nextInt(5) + 5);
            }
            phones.add(new Phone(name, price, battery, display, processor));
        }
    }
}
