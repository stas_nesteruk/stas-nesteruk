package by.epamlab.inttraining.beans;

public interface Display {
    String getSize();

    String getResolution();
}
