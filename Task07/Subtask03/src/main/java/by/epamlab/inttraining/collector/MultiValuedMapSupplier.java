package by.epamlab.inttraining.collector;

import by.epamlab.inttraining.beans.CurrencyRate;

import java.time.Month;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class MultiValuedMapSupplier implements Supplier<Map<Month, CurrencyRate>> {
    @Override
    public Map<Month, CurrencyRate> get() {
        return new HashMap<>();
    }
}
