package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.CurrencyRate;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DataLoader {
    private static final Logger LOGGER = Logger.getLogger(DataLoader.class);
    private static final String FILE_EXTENSION = ".txt";
    private static final String LIST_OF_CURRENCY = "CurrencyRate";
    private static final String PATH = "Subtask03\\src\\main\\resources\\";
    private static final int YEAR_INDEX = 2;
    private static final int MONTH_INDEX = 1;
    private static final int DAY_INDEX = 0;
    private static final int CURRENCY_RATE_INDEX = 1;
    private static final int DATE_INDEX = 0;
    private static List<CurrencyRate> currencies = new ArrayList<>();

    private DataLoader() {
    }

    public static void load() {
        try {
            Path path = Paths.get(PATH + LIST_OF_CURRENCY + FILE_EXTENSION);
            List<String> lines = Files.readAllLines(path);
            lines.forEach(x -> {
                String[] line = x.split("\t");
                String[] date = line[DATE_INDEX].split("\\.");
                CurrencyRate currency = new CurrencyRate(
                        LocalDate.of(Integer.parseInt(date[YEAR_INDEX]),
                                Integer.parseInt(date[MONTH_INDEX]),
                                Integer.parseInt(date[DAY_INDEX])),
                        Double.parseDouble(line[CURRENCY_RATE_INDEX]));
                currencies.add(currency);
            });
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public static List<CurrencyRate> getCurrencies() {
        return currencies;
    }
}
