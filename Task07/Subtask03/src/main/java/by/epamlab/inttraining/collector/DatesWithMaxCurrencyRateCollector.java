package by.epamlab.inttraining.collector;

import by.epamlab.inttraining.beans.CurrencyRate;

import java.time.Month;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class DatesWithMaxCurrencyRateCollector implements Collector<CurrencyRate, Map<Month, CurrencyRate>, List<CurrencyRate>> {

    @Override
    public Supplier<Map<Month, CurrencyRate>> supplier() {
        return new MultiValuedMapSupplier();
    }

    @Override
    public BiConsumer<Map<Month, CurrencyRate>, CurrencyRate> accumulator() {
        return new MultiValuedMapAccumulator();
    }

    @Override
    public BinaryOperator<Map<Month, CurrencyRate>> combiner() {
        return new MultiValuedMapCombiner();
    }

    @Override
    public Function<Map<Month, CurrencyRate>, List<CurrencyRate>> finisher() {
        return map -> map.values().stream()
                .sorted(Comparator.comparing(CurrencyRate::getDate))
                .collect(Collectors.toList());
    }

    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of(Characteristics.CONCURRENT);
    }

}
