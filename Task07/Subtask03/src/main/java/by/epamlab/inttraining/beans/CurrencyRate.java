package by.epamlab.inttraining.beans;

import java.time.LocalDate;

public class CurrencyRate {
    private static final int TEN = 10;
    private static final int HUNDRED = 100;
    private static final int THOUSAND = 1000;

    private LocalDate date;
    private int copecs;


    public CurrencyRate(LocalDate date, double copecs) {
        this.date = date;
        this.copecs = convertToCopecs(copecs);
    }

    public LocalDate getDate() {
        return date;
    }

    public int getCopecs() {
        return copecs;
    }

    @Override
    public String toString() {
        return String.valueOf(date) + ";" + toConvert(copecs);
    }

    public static String toConvert(int copecks) {
        return copecks / THOUSAND + "." + copecks / HUNDRED % TEN + copecks % HUNDRED;
    }

    private static int convertToCopecs(double copecs) {
        double d = Math.round(copecs * THOUSAND) / 1000d;
        return (int) (d * THOUSAND);
    }
}
