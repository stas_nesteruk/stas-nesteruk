package by.epamlab.inttraining.collector;

import by.epamlab.inttraining.beans.CurrencyRate;

import java.time.Month;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BinaryOperator;

public class MultiValuedMapCombiner implements BinaryOperator<Map<Month, CurrencyRate>> {
    @Override
    public Map<Month, CurrencyRate> apply(Map<Month, CurrencyRate> map, Map<Month, CurrencyRate> map2) {
        HashMap<Month, CurrencyRate> newMap = new HashMap<>(map);
        map2.forEach((k, v) -> newMap.merge(k, v, (x, y) -> {
            if (x.getCopecs() > y.getCopecs()) {
                return x;
            } else {
                return y;
            }
        }));
        return newMap;
    }
}
