package by.epamlab.inttraining.collector;

import by.epamlab.inttraining.beans.CurrencyRate;

import java.time.Month;
import java.util.Map;
import java.util.function.BiConsumer;

public class MultiValuedMapAccumulator implements BiConsumer<Map<Month, CurrencyRate>, CurrencyRate> {
    @Override
    public void accept(Map<Month, CurrencyRate> map, CurrencyRate rate) {
        if (map.get(rate.getDate().getMonth()) == null) {
            map.put(rate.getDate().getMonth(), rate);
        } else {
            int copecs = map.get(rate.getDate().getMonth()).getCopecs();
            if (copecs < rate.getCopecs()) {
                map.put(rate.getDate().getMonth(), rate);
            }
        }
    }
}
