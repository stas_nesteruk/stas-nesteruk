package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.CurrencyRate;
import org.apache.log4j.Logger;

public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class);
    private static final String OUTPUT_FORMAT = "%-10s%s";

    public static void main(String[] args) {

        AppTasks.minAndMaxCurrencyRatesPerYear().forEach((x, y) -> LOGGER.info(x + " " + y));
        LOGGER.info("Average rate per year: ");
        LOGGER.info(AppTasks.averageCurrencyRatePerYear());

        AppTasks.minAndMaxCurrencyRateInEachMonth().forEach((x, y) -> {
            LOGGER.info(x);
            y.forEach((month, rate) -> LOGGER.info(String.format(OUTPUT_FORMAT,
                    month.toString().toLowerCase(),
                    CurrencyRate.toConvert(rate.getCopecs()))));
        });

        LOGGER.info("Average rate in each month:");
        AppTasks.averageCurrencyRateInEachMonth().forEach((x, y) -> LOGGER.info(String.format(OUTPUT_FORMAT,
                x.toString().toLowerCase(),
                CurrencyRate.toConvert(y.intValue()))));

        LOGGER.info("Dates with max rate in each month(sorted by month):");
        AppTasks.datesWithMaxCurrencyRateInEachMonth().forEach(LOGGER::info);

        LOGGER.info("With parallel:");
        AppTasks.collectorWithParrallel().forEach(LOGGER::info);

    }
}
