package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.CurrencyRate;
import by.epamlab.inttraining.collector.DatesWithMaxCurrencyRateCollector;

import java.time.Month;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.averagingInt;
import static java.util.stream.Collectors.groupingBy;

public class AppTasks {
    private static final String MINIMUM_RATE = "Minimum rate: ";
    private static final String MAXIMUM_RATE = "Maximum rate: ";
    private static final String MIN_RATE_PER_MONTHS = "Min rate per months: ";
    private static final String MAX_RATE_PER_MONTHS = "Max rate per months: ";
    private static List<CurrencyRate> currencies;

    static {
        DataLoader.load();
        currencies = DataLoader.getCurrencies();
    }

    public AppTasks() {
    }

    public static Map<String, CurrencyRate> minAndMaxCurrencyRatesPerYear() {
        Map<String, CurrencyRate> result = new HashMap<>();
        currencies.stream()
                .min(Comparator.comparing(CurrencyRate::getCopecs))
                .ifPresent(x -> result.put(MINIMUM_RATE, x));
        currencies.stream()
                .max(Comparator.comparing(CurrencyRate::getCopecs))
                .ifPresent(x -> result.put(MAXIMUM_RATE, x));
        return result;
    }

    public static String averageCurrencyRatePerYear() {
        double avarage = currencies.stream()
                .mapToInt(CurrencyRate::getCopecs)
                .average().orElse(Double.NaN);
        return CurrencyRate.toConvert((int) avarage);
    }

    public static Map<String, Map<Month, CurrencyRate>> minAndMaxCurrencyRateInEachMonth() {
        Map<String, Map<Month, CurrencyRate>> result = new HashMap<>();
        Map<Month, CurrencyRate> minRates = currencies.stream().collect(
                Collectors.toMap(x -> x.getDate().getMonth(),
                        Function.identity(),
                        BinaryOperator.minBy(Comparator.comparing(CurrencyRate::getCopecs))));

        Map<Month, CurrencyRate> maxRates = currencies.stream().collect(
                Collectors.toMap(x -> x.getDate().getMonth(),
                        Function.identity(),
                        BinaryOperator.maxBy(Comparator.comparing(CurrencyRate::getCopecs))));
        result.put(MIN_RATE_PER_MONTHS, minRates);
        result.put(MAX_RATE_PER_MONTHS, maxRates);
        return result;
    }

    public static Map<Month, Double> averageCurrencyRateInEachMonth() {
        return currencies.stream().collect(
                groupingBy(x -> x.getDate().getMonth(), averagingInt(CurrencyRate::getCopecs)));
    }

    public static List<CurrencyRate> datesWithMaxCurrencyRateInEachMonth() {
        return currencies.stream().collect(new DatesWithMaxCurrencyRateCollector());
    }

    public static List<CurrencyRate> collectorWithParrallel() {
        return currencies.stream().parallel().collect(new DatesWithMaxCurrencyRateCollector());
    }
}
