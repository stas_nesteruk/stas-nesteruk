package by.epamlab.inttraining.repository.storage;

import by.epamlab.inttraining.configuration.JPAConfig;
import by.epamlab.inttraining.entity.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfig.class})
@WebAppConfiguration
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void findProductByNameContainsOrDescriptionContains() {
        List<Product> productList = productRepository.findProductByNameContainsOrDescriptionContains("Samsung HC0587187", "Samsung HC0587187");
        assertEquals(1, productList.size());
    }
}