package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.entity.Category;
import by.epamlab.inttraining.entity.Producer;
import by.epamlab.inttraining.entity.Product;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class ShoppingCartTest {

    private static final Product TEST_PRODUCT = new Product();

    static {
        TEST_PRODUCT.setId(5);
        TEST_PRODUCT.setName("Test");
        TEST_PRODUCT.setCountStock(4);
        TEST_PRODUCT.setPrice(BigDecimal.valueOf(150));
        TEST_PRODUCT.setImageLink("link");
        TEST_PRODUCT.setProducer(new Producer());
        TEST_PRODUCT.setCategory(new Category());
    }

    @org.junit.Test
    public void addProduct() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(TEST_PRODUCT, 4);
        int actual = shoppingCart.getTotalCount();
        int expected = 4;
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void removeProduct() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(TEST_PRODUCT, 5);
        shoppingCart.removeProduct(TEST_PRODUCT.getId(), 2);
        int actual = shoppingCart.getTotalCount();
        int expected = 3;
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void getTotalCost() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(TEST_PRODUCT, 5);
        BigDecimal actual = shoppingCart.getTotalCost();
        BigDecimal expected = BigDecimal.valueOf(750);
        assertEquals(expected, actual);
    }
}