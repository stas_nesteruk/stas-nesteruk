package by.epamlab.inttraining.controller;

import by.epamlab.inttraining.entity.Product;
import by.epamlab.inttraining.form.BaseProductForm;
import by.epamlab.inttraining.service.CategoryService;
import by.epamlab.inttraining.service.ProducerService;
import by.epamlab.inttraining.service.ProductService;
import by.epamlab.inttraining.utils.Constants;
import by.epamlab.inttraining.utils.SessionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;

@Controller
public class EditController {
    private static final Logger LOGGER = Logger.getLogger(EditController.class);
    private static final String STATUS_ON = "ON";
    private static final String STATUS_OFF = "OFF";

    private static final String ADD_NEW_PRODUCT_FORM = "addNewProductForm";
    private static final String SORT_BY_NAME = "name";
    private static final String ADDED_ACTION = "Added";
    private static final String DELETE_ACTION = "Delete";
    private static final String UPDATE_ACTION = "Update";
    private static final String EDIT_PRODUCT_FORM = "editProductForm";
    private static final String ID_PRODUCT = "ID_PRODUCT";
    private static final String IMAGE_LINK = "imageLink";


    @Autowired
    private ProducerService producerService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;
    @Autowired
    ServletContext context;

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String switchEditMode(HttpServletRequest req) {
        if (!SessionUtils.isEditMode(req)) {
            SessionUtils.setEditMode(req, STATUS_ON);
        } else {
            switch (SessionUtils.getEditMode(req)) {
                case "ON":
                    SessionUtils.setEditMode(req, STATUS_OFF);
                    break;
                case "OFF":
                    SessionUtils.setEditMode(req, STATUS_ON);
                    break;
            }
        }
        return Constants.REDIRECT_PRODUCTS;
    }

    @RequestMapping(value = "/add-product", method = RequestMethod.GET)
    public String getAddProductPage(Model model) {
        model.addAttribute(Constants.CATEGORIES, categoryService.findAll(new Sort(SORT_BY_NAME)));
        model.addAttribute(Constants.PRODUCERS, producerService.findAll(new Sort(SORT_BY_NAME)));
        model.addAttribute(Constants.CURRENT_PAGE, "page/addNewProduct.jsp");
        model.addAttribute(ADD_NEW_PRODUCT_FORM, new BaseProductForm());
        return Constants.PAGE_TEMPLATE;
    }

    @RequestMapping(value = "/add-product", method = RequestMethod.POST)
    public String saveNewProduct(@Valid @ModelAttribute(ADD_NEW_PRODUCT_FORM) BaseProductForm form,
                                 BindingResult bindingResult,
                                 HttpServletRequest req) throws IOException {
        if (form.getFile().isEmpty()) {
            bindingResult.rejectValue("file", "NotNull");
        }
        if (bindingResult.hasErrors()) {
            req.setAttribute(Constants.CATEGORIES, categoryService.findAll(new Sort(SORT_BY_NAME)));
            req.setAttribute(Constants.PRODUCERS, producerService.findAll(new Sort(SORT_BY_NAME)));
            req.setAttribute(Constants.CURRENT_PAGE, "page/addNewProduct.jsp");
            return Constants.PAGE_TEMPLATE;
        }
        Product product = new Product();
        MultipartFile file = form.getFile();
        String root = context.getRealPath("/");
        File path = new File(root + "media/" + File.separator + file.getOriginalFilename());
        FileCopyUtils.copy(form.getFile().getBytes(), path);

        product.setImageLink("/media/" + file.getOriginalFilename());
        product.setName(form.getNameProduct());
        product.setCategory(categoryService.findByName(form.getNameCategory()));
        product.setProducer(producerService.findByName(form.getNameProducer()));
        product.setCountStock(form.getNameCount());
        product.setDescription(form.getNameDescription());
        product.setPrice(form.getNamePrice());

        productService.save(product);
        loggingAction(ADDED_ACTION, product);
        return Constants.REDIRECT_PRODUCTS;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteProduct(HttpServletRequest req) {
        int id = Integer.parseInt(req.getParameter("id"));
        Product product = productService.findProductById(id);
        productService.delete(product);
        loggingAction(DELETE_ACTION, product);
        return Constants.REDIRECT_PRODUCTS;
    }

    @RequestMapping(value = "/edit-product", method = RequestMethod.GET)
    public String getEditProductPage(HttpServletRequest req, Model model) {
        int id = Integer.parseInt(req.getParameter("id"));
        Product product = productService.findProductById(id);
        BaseProductForm form = new BaseProductForm();
        form.setNameDescription(product.getDescription());
        form.setNameCount(product.getCountStock());
        form.setNameProducer(product.getProducer().getName());
        form.setNameCategory(product.getCategory().getName());
        form.setNameProduct(product.getName());
        form.setNamePrice(product.getPrice());


        model.addAttribute(Constants.CATEGORIES, categoryService.findAll(new Sort("name")));
        model.addAttribute(Constants.PRODUCERS, producerService.findAll(new Sort("name")));
        req.getSession().setAttribute(ID_PRODUCT, id);
        req.getSession().setAttribute(IMAGE_LINK, product.getImageLink());
        model.addAttribute(Constants.CURRENT_PAGE, "page/editProduct.jsp");
        model.addAttribute(EDIT_PRODUCT_FORM, form);
        return Constants.PAGE_TEMPLATE;
    }

    @RequestMapping(value = "/edit-product", method = RequestMethod.POST)
    public String saveEditProduct(@Valid @ModelAttribute(EDIT_PRODUCT_FORM) BaseProductForm form,
                                  BindingResult bindingResult,
                                  HttpServletRequest req) throws IOException {
        if (bindingResult.hasErrors()) {
            req.setAttribute(Constants.CATEGORIES, categoryService.findAll(new Sort("name")));
            req.setAttribute(Constants.PRODUCERS, producerService.findAll(new Sort("name")));
            req.setAttribute(Constants.CURRENT_PAGE, "page/editProduct.jsp");
            return Constants.PAGE_TEMPLATE;
        }
        int id = (int) req.getSession().getAttribute(ID_PRODUCT);
        Product product = new Product();
        boolean isImage = false;
        if (!form.getFile().isEmpty()) {
            MultipartFile file = form.getFile();
            String root = context.getRealPath("/");
            File path = new File(root + "media/" + File.separator + file.getOriginalFilename());
            FileCopyUtils.copy(form.getFile().getBytes(), path);
            product.setImageLink("/media/" + file.getOriginalFilename());
            isImage = true;
        }
        if (!isImage) {
            product.setImageLink(productService.findProductById(id).getImageLink());
        }
        product.setId(id);
        product.setName(form.getNameProduct());
        product.setCategory(categoryService.findByName(form.getNameCategory()));
        product.setProducer(producerService.findByName(form.getNameProducer()));
        product.setCountStock(form.getNameCount());
        product.setDescription(form.getNameDescription());
        product.setPrice(form.getNamePrice());
        productService.save(product);
        loggingAction(UPDATE_ACTION, product);
        req.getSession().removeAttribute(IMAGE_LINK);
        req.getSession().removeAttribute(ID_PRODUCT);
        return Constants.REDIRECT_PRODUCTS;
    }

    private void loggingAction(String action, Product product) {
        LOGGER.info(action + " product: " +
                product.getName() + ";" +
                product.getPrice() + ";" +
                product.getCategory() + ";" +
                product.getProducer() + ";" +
                product.getImageLink() + ";" +
                product.getCountStock() + ";" +
                product.getDescription());
    }
}
