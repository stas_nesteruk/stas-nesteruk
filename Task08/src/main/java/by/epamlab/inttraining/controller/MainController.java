package by.epamlab.inttraining.controller;


import by.epamlab.inttraining.beans.CurrentUser;
import by.epamlab.inttraining.entity.Account;
import by.epamlab.inttraining.form.SignUpForm;
import by.epamlab.inttraining.service.AccountService;
import by.epamlab.inttraining.utils.Constants;
import by.epamlab.inttraining.utils.SecurityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class MainController {
    private static final Logger LOGGER = Logger.getLogger(MainController.class);
    private static final String SIGN_UP_FORM = "signUpForm";

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String getMainPage(Model model) {
        CurrentUser currentUser = SecurityUtils.getCurrentUser();
        if (currentUser != null) {
            return Constants.REDIRECT_PRODUCTS;
        }
        model.addAttribute(SIGN_UP_FORM, new SignUpForm());
        return "main";
    }

    @RequestMapping(value = "/main", method = RequestMethod.POST)
    public String signUp(@Valid @ModelAttribute(SIGN_UP_FORM) SignUpForm signUpForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "main";
        }
        Account account = new Account();
        String login = signUpForm.getLogin();
        String password = signUpForm.getPassword();
        account.setLogin(login);
        account.setEmail(signUpForm.getEmail());
        account.setPassword(password);
        Account isHaveAccount = accountService.findByLoginAndPassword(login, password);
        if (isHaveAccount == null) {
            accountService.save(account);
            SecurityUtils.authentificate(account);
            LOGGER.info("User sign up: " + account.getId() + " " + account.getLogin());
            return Constants.REDIRECT_PRODUCTS;
        } else {
            return Constants.REDIRECT_ERROR;
        }
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String getErrorPage() {
        return "error";
    }
}
