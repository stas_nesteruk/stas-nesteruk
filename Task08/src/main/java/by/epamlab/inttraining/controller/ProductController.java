package by.epamlab.inttraining.controller;

import by.epamlab.inttraining.beans.ShoppingCart;
import by.epamlab.inttraining.entity.Product;
import by.epamlab.inttraining.exception.ServerErrorException;
import by.epamlab.inttraining.exception.ValidationException;
import by.epamlab.inttraining.form.ShoppingCartDetailsForm;
import by.epamlab.inttraining.service.CategoryService;
import by.epamlab.inttraining.service.ProducerService;
import by.epamlab.inttraining.service.ProductService;
import by.epamlab.inttraining.utils.Constants;
import by.epamlab.inttraining.utils.SessionUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ProductController {
    private static final Logger LOGGER = Logger.getLogger(ProductController.class);
    private static final String PRODUCTS = "products";
    private static final String SORT_BY_NAME = "name";
    private static final String GET_PARAMETER_QUERY = "query";
    private static final String SEARCH_QUERY = "searchQuery";
    private static final String ERROR_MSG_NOT_FOUND_PRODUCT = "Product not found in database by id= ";
    private static final String TOTAL_COUNT = "totalCount";
    private static final String TOTAL_COST = "totalCost";
    private static final int ERROR_CODE_400 = 400;
    private static final String ID_PRODUCT = "idProduct";
    private static final String COUNT = "count";
    @Autowired
    private ProducerService producerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String getAllProducts(Model model) {
        List<Product> products = productService.findAll(new Sort(SORT_BY_NAME));
        model.addAttribute(Constants.CATEGORIES, categoryService.findAll(new Sort(SORT_BY_NAME)));
        model.addAttribute(Constants.PRODUCERS, producerService.findAll(new Sort(SORT_BY_NAME)));
        model.addAttribute(PRODUCTS, productsWhereCountGreaterZero(products));
        model.addAttribute(Constants.CURRENT_PAGE, "page/proddemo.jsp");
        return Constants.PAGE_TEMPLATE;
    }

    @RequestMapping(value = "/products/*", method = RequestMethod.GET)
    public String getAllProductsByCategory(HttpServletRequest req, Model model) {
        final int substringIndex = "/products".length();
        String url = req.getRequestURI().substring(substringIndex);
        List<Product> products = productService.findProductByCategoryUrl(url);
        model.addAttribute(Constants.CATEGORIES, categoryService.findAll(new Sort(SORT_BY_NAME)));
        model.addAttribute(PRODUCTS, productsWhereCountGreaterZero(products));
        model.addAttribute(Constants.CURRENT_PAGE, "page/proddemo.jsp");
        return Constants.PAGE_TEMPLATE;
    }

    private List<Product> productsWhereCountGreaterZero(List<Product> products) {
        return products.stream().filter(x -> x.getCountStock() > 0).collect(Collectors.toList());
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(HttpServletRequest req, Model model) {
        String query = req.getParameter(GET_PARAMETER_QUERY);
        List<Product> products = productService.findProductByNameContainsOrDescriptionContains(query, query);
        model.addAttribute(Constants.CATEGORIES, categoryService.findAll(new Sort(SORT_BY_NAME)));
        model.addAttribute(SEARCH_QUERY, query);
        model.addAttribute(PRODUCTS, products);
        model.addAttribute(Constants.CURRENT_PAGE, "page/proddemo.jsp");
        return Constants.PAGE_TEMPLATE;
    }

    @RequestMapping(value = "/product/add/cart", method = RequestMethod.POST)
    public void addProductToCart(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ShoppingCartDetailsForm form = createProductForm(req);
        ShoppingCart shoppingCart = (ShoppingCart)
                req.getSession().getAttribute(Constants.CURRENT_SHOPPING_CART);
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
            req.getSession().setAttribute(Constants.CURRENT_SHOPPING_CART, shoppingCart);
        }
        Product product = productService.findProductById(form.getIdProduct());
        if (product == null) {
            throw new ServerErrorException(ERROR_MSG_NOT_FOUND_PRODUCT + form.getIdProduct());
        }
        try {
            shoppingCart.addProduct(product, form.getCount());
        } catch (ValidationException ex) {
            resp.setStatus(ERROR_CODE_400);
            resp.getWriter().write(ex.getMessage());
            resp.getWriter().close();
            return;
        }
        LOGGER.info("User add product to shopping card: " + product.getId() + " " + form.getCount());
        JSONObject json = new JSONObject();
        json.put(TOTAL_COUNT, shoppingCart.getTotalCount());
        json.put(TOTAL_COST, shoppingCart.getTotalCost());

        resp.setContentType("application/json");
        resp.getWriter().println(json.toString());
        resp.getWriter().close();
    }

    @RequestMapping(value = "/product/remove/cart", method = RequestMethod.POST)
    private void removeProductFromShoppingCart(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ShoppingCartDetailsForm form = createProductForm(req);
        ShoppingCart shoppingCart = (ShoppingCart)
                req.getSession().getAttribute(Constants.CURRENT_SHOPPING_CART);
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
            req.getSession().setAttribute(Constants.CURRENT_SHOPPING_CART, shoppingCart);
        }
        shoppingCart.removeProduct(form.getIdProduct(), form.getCount());
        if (shoppingCart.getItems().isEmpty()) {
            SessionUtils.clearCurrentShoppingCart(req);
        }
        Product product = productService.findProductById(form.getIdProduct());
        LOGGER.info("User remove product from shopping card: " + product.getId() + " " + form.getCount());
        JSONObject json = new JSONObject();
        json.put(TOTAL_COUNT, shoppingCart.getTotalCount());
        json.put(TOTAL_COST, shoppingCart.getTotalCost());

        resp.setContentType("application/json");
        resp.getWriter().println(json.toString());
        resp.getWriter().close();
    }

    @RequestMapping(value = "/shopping-cart", method = RequestMethod.GET)
    public String shoppingCart(HttpServletRequest request, Model model) {
        if (request.getSession().getAttribute(Constants.CURRENT_SHOPPING_CART) != null) {
            model.addAttribute(Constants.CATEGORIES, categoryService.findAll(new Sort(SORT_BY_NAME)));
            model.addAttribute(Constants.CURRENT_PAGE, "page/shopping-cart.jsp");
            return Constants.PAGE_TEMPLATE;
        } else {
            return Constants.REDIRECT_PRODUCTS;
        }
    }

    private ShoppingCartDetailsForm createProductForm(HttpServletRequest req) {
        return new ShoppingCartDetailsForm(
                Integer.parseInt(req.getParameter(ID_PRODUCT)),
                Integer.parseInt(req.getParameter(COUNT)));
    }

}
