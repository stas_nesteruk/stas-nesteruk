package by.epamlab.inttraining.controller;

import by.epamlab.inttraining.beans.CurrentUser;
import by.epamlab.inttraining.beans.Role;
import by.epamlab.inttraining.entity.Account;
import by.epamlab.inttraining.form.LoginForm;
import by.epamlab.inttraining.form.SignUpForm;
import by.epamlab.inttraining.service.AccountService;
import by.epamlab.inttraining.utils.Constants;
import by.epamlab.inttraining.utils.SecurityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class UserController {
    private static final Logger LOGGER = Logger.getLogger(UserController.class);
    private static final String LOGIN = "login";
    private static final String LOGIN_FORM = "loginForm";
    private static final String LOGIN_ERROR = "LOGIN_ERROR";
    private static final String REDIRECT_LOGIN = "redirect: /login";
    private static final String ERROR_MSG_USER_DONT_EXIST = "User don't exist in database";
    private static final String SPRING_SECURITY_LAST_EXCEPTION = "SPRING_SECURITY_LAST_EXCEPTION";
    private static final String SIGN_UP_FORM = "signUpForm";
    private static final String SIGNUP = "signup";

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage(Model model) {
        model.addAttribute(LOGIN_FORM, new LoginForm());
        return LOGIN;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String authorization(@Valid @ModelAttribute(LOGIN_FORM) LoginForm form, BindingResult bindingResult, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return LOGIN;
        }
        Account user = accountService.findByLoginAndPassword(form.getLogin(), form.getPassword());
        CurrentUser currentUserInSession = SecurityUtils.getCurrentUser();
        if (currentUserInSession != null) {
            return Constants.REDIRECT_PRODUCTS;
        } else {
            if (user == null) {
                LOGGER.error(ERROR_MSG_USER_DONT_EXIST);
                request.setAttribute(LOGIN_ERROR, ERROR_MSG_USER_DONT_EXIST);
                return REDIRECT_LOGIN;
            } else {
                LOGGER.info("User sign in: " + user.getId() + " " + user.getLogin() + user.getRole());
                return Constants.REDIRECT_PRODUCTS;
            }
        }
    }

    @RequestMapping(value = "/login-failed")
    public String loginFailed(HttpSession session, Model model) {
        if (session.getAttribute(SPRING_SECURITY_LAST_EXCEPTION) == null) {
            return REDIRECT_LOGIN;
        }
        model.addAttribute(LOGIN_FORM, new LoginForm());
        return LOGIN;
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.GET)
    public String getSignUpPage(Model model) {
        if (SecurityUtils.isCurrentUserAuthentificated()) {
            return Constants.REDIRECT_PRODUCTS;
        } else {
            model.addAttribute(SIGN_UP_FORM, new SignUpForm());
            return SIGNUP;
        }
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public String signUpUser(@Valid @ModelAttribute(SIGN_UP_FORM) SignUpForm signUpForm, BindingResult bindingResult, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return SIGNUP;
        }
        Account user = new Account();
        String login = signUpForm.getLogin();
        String password = signUpForm.getPassword();
        user.setLogin(login);
        user.setEmail(signUpForm.getEmail());
        user.setPassword(password);
        user.setRole(Role.USER.name());
        Account isHaveAccount = accountService.findByLoginAndPassword(login, password);
        if (isHaveAccount == null) {
            accountService.save(user);
            Account byLoginAndPassword = accountService.findByLoginAndPassword(user.getLogin(), user.getPassword());
            SecurityUtils.authentificate(byLoginAndPassword);
            LOGGER.info("User sign up: " + byLoginAndPassword.getId() + " " + byLoginAndPassword.getLogin() + byLoginAndPassword.getRole());
            return Constants.REDIRECT_PRODUCTS;
        } else {
            return "redirect: /error";
        }
    }
}
