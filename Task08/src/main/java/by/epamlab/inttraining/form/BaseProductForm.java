package by.epamlab.inttraining.form;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.math.BigDecimal;

public class BaseProductForm {
    @Size(min = 4, message = "Product name should be more 4 characters")
    private String nameProduct;
    @Size(min = 3, message = "Category name should be more 3 characters")
    private String nameCategory;
    @Size(min = 4, message = "Producer name should be more 3 characters")
    private String nameProducer;
    @Min(value = 1, message = "Price should be positive")
    @NotNull(message = "Price should be not null")
    @DecimalMax(value = "9999.99", message = "Price should be less 9999.99")
    @DecimalMin(value = "1.00", message = "Price should be more 1")
    private BigDecimal namePrice;
    @Min(value = 1, message = "Count should be positive")
    @NotNull(message = "Count should be not null")
    @Digits(integer = 2, fraction = 0)
    private int nameCount;
    @Size(min = 4, message = "Descriptions should be more 4 characters")
    private String nameDescription;

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public BaseProductForm() {
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public String getNameProducer() {
        return nameProducer;
    }

    public void setNameProducer(String nameProducer) {
        this.nameProducer = nameProducer;
    }

    public BigDecimal getNamePrice() {
        return namePrice;
    }

    public void setNamePrice(BigDecimal namePrice) {
        this.namePrice = namePrice;
    }

    public int getNameCount() {
        return nameCount;
    }

    public void setNameCount(int nameCount) {
        this.nameCount = nameCount;
    }

    public String getNameDescription() {
        return nameDescription;
    }

    public void setNameDescription(String nameDescription) {
        this.nameDescription = nameDescription;
    }
}
