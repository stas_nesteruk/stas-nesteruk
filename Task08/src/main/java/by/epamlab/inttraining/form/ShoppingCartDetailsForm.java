package by.epamlab.inttraining.form;

public class ShoppingCartDetailsForm {
    private int idProduct;
    private int count;

    public ShoppingCartDetailsForm(int idProduct, int count) {
        this.idProduct = idProduct;
        this.count = count;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
