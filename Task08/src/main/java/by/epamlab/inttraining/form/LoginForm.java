package by.epamlab.inttraining.form;

import javax.validation.constraints.Size;

public class LoginForm {
    @Size(min = 5)
    private String login;
    @Size(min = 5)
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
