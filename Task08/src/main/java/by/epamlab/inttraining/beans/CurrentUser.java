package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.entity.Account;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Collections;
import java.util.Objects;

public class CurrentUser extends User implements Serializable {
    private static final long serialVersionUID = -1969116115947300361L;
    private int id;
    private String login;
    private String role;

    public CurrentUser(Account account) {
        super(account.getLogin(), account.getPassword(), Collections.singleton(new SimpleGrantedAuthority(account.getRole())));
        this.id = account.getId();
        this.login = account.getLogin();
        this.role = account.getRole();
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return login;
    }

    public String getRole() {
        return role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CurrentUser)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        CurrentUser that = (CurrentUser) o;
        return id == that.id &&
                Objects.equals(login, that.login) &&
                Objects.equals(role, that.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, login, role);
    }
}
