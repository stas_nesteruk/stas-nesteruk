package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.entity.Product;
import by.epamlab.inttraining.exception.ValidationException;
import by.epamlab.inttraining.utils.Constants;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ShoppingCart implements Serializable {
    private static final long serialVersionUID = 3395917651650114892L;
    private Map<Integer, ShoppingCartItem> products = new HashMap<>();
    private int totalCount;
    private BigDecimal totalCost;

    public void addProduct(Product product, int count) {
        ShoppingCartItem item = products.get(product.getId());
        if (item == null) {
            ShoppingCartItem newItem = new ShoppingCartItem(product, count);
            products.put(product.getId(), newItem);
        } else {
            if (product.getCountStock() >= (item.getCount() + count)) {
                item.setCount(item.getCount() + count);
            } else {
                throw new ValidationException(Constants.ERROR_MSG_WRONG_COUNT);
            }
        }
        refreshTotalCount();
    }

    public void removeProduct(int idProduct, int count) {
        ShoppingCartItem item = products.get(idProduct);
        if (item != null) {
            if (item.getCount() > count) {
                item.setCount(item.getCount() - count);
            } else {
                products.remove(idProduct);
            }
            refreshTotalCount();
        }
    }

    private void refreshTotalCount() {
        totalCount = 0;
        totalCost = BigDecimal.ZERO;
        getItems().forEach(x -> {
            totalCount += x.getCount();
            totalCost = totalCost.add(x.getProduct().getPrice().multiply(BigDecimal.valueOf(x.getCount())));
        });
    }


    public Collection<ShoppingCartItem> getItems() {
        return products.values();
    }

    public int getTotalCount() {
        return totalCount;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    @Override
    public String toString() {
        return String.format("ShoppingCart [products=%s, totalCount=%d, totalCost=%s]", products, totalCount, totalCost);
    }
}
