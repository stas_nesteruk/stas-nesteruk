package by.epamlab.inttraining.exception;

import javax.servlet.http.HttpServletResponse;

public class AccessDeniedException extends AbstractException {
    private static final long serialVersionUID = -6502066305442608148L;

    public AccessDeniedException(String msg) {
        super(msg, HttpServletResponse.SC_FORBIDDEN);
    }
}
