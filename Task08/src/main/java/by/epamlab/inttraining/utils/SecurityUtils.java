package by.epamlab.inttraining.utils;

import by.epamlab.inttraining.beans.CurrentUser;
import by.epamlab.inttraining.entity.Account;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {
    
    private SecurityUtils() {
    }

    public static CurrentUser getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        Object principal = authentication.getPrincipal();
        if (principal instanceof CurrentUser) {
            return ((CurrentUser) principal);
        } else {
            return null;
        }
    }


    public static void authentificate(Account account) {
        CurrentUser currentUser = new CurrentUser(account);
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                currentUser, currentUser.getPassword(), currentUser.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public static boolean isCurrentUserAuthentificated() {
        return getCurrentUser() != null;
    }
}
