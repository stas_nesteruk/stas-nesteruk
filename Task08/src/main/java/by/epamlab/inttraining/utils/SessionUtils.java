package by.epamlab.inttraining.utils;

import javax.servlet.http.HttpServletRequest;

public class SessionUtils {
    private SessionUtils() {
    }

    public static void setEditMode(HttpServletRequest req, String status) {
        req.getSession().setAttribute(Constants.EDIT_MODE, status);
    }

    public static String getEditMode(HttpServletRequest req) {
        return (String) req.getSession().getAttribute(Constants.EDIT_MODE);
    }

    public static void clearCurrentShoppingCart(HttpServletRequest req) {
        req.getSession().removeAttribute(Constants.CURRENT_SHOPPING_CART);
    }

    public static boolean isEditMode(HttpServletRequest req) {
        return getEditMode(req) != null;
    }
}
