package by.epamlab.inttraining.utils;

public class Constants {
    public static final int MAX_PRODUCT_COUNT_PER_PAGE = 12;
    public static final String CURRENT_SHOPPING_CART = "CURRENT_SHOPPING_CART";
    public static final String CURRENT_USER = "CURRENT_USER";
    public static final String EDIT_MODE = "EDIT_MODE";

    public static final String ERROR_MSG_WRONG_COUNT = "The count of products in the shopping cart should not exceed the count in the stock.";
    public static final String CATEGORIES = "categories";
    public static final String PRODUCERS = "producers";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String PAGE_TEMPLATE = "page-template";
    public static final String REDIRECT_PRODUCTS = "redirect: /products";
    public static final String REDIRECT_ERROR = "redirect: /error";
}
