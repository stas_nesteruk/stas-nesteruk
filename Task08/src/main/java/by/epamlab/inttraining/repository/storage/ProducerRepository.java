package by.epamlab.inttraining.repository.storage;

import by.epamlab.inttraining.entity.Producer;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProducerRepository extends PagingAndSortingRepository<Producer, Integer> {
    Producer findByName(String name);

    @Override
    List<Producer> findAll(Sort sort);
}
