package by.epamlab.inttraining.repository.storage;

import by.epamlab.inttraining.entity.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Integer> {
    Account findByLogin(String login);

    Account findByLoginAndPassword(String login, String Password);

    Account findById(int id);
}
