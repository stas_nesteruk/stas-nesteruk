package by.epamlab.inttraining.repository.storage;

import by.epamlab.inttraining.entity.OrderItem;
import org.springframework.data.repository.CrudRepository;

public interface OrderItemRepository extends CrudRepository<OrderItem, Integer> {
}
