package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Producer;
import by.epamlab.inttraining.repository.storage.ProducerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProducerService {

    @Autowired
    private ProducerRepository producerRepository;

    public Producer findByName(String name) {
        return producerRepository.findByName(name);
    }

    public List<Producer> findAll(Sort sort) {
        return producerRepository.findAll(sort);
    }
}
