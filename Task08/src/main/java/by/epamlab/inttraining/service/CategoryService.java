package by.epamlab.inttraining.service;

import by.epamlab.inttraining.entity.Category;
import by.epamlab.inttraining.repository.storage.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> findAll(Sort sort) {
        return categoryRepository.findAll(sort);
    }

    public Category findByName(String name) {
        return categoryRepository.findByName(name);
    }
}
