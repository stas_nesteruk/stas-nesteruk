package by.epamlab.inttraining.service;

import by.epamlab.inttraining.beans.CurrentUser;
import by.epamlab.inttraining.entity.Account;
import by.epamlab.inttraining.repository.storage.AccountRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AccountService implements UserDetailsService {
    private static final Logger LOGGER = Logger.getLogger(AccountService.class);
    private static final String ERROR_MSG_ACCOUNT_NOT_FOUND = "Account not found by ";

    @Autowired
    private AccountRepository accountRepository;

    public Account findByLogin(String login) {
        return accountRepository.findByLogin(login);
    }

    public Account findByLoginAndPassword(String login, String password) {
        return accountRepository.findByLoginAndPassword(login, password);
    }

    public Account findById(int id) {
        return accountRepository.findById(id);
    }

    public void save(Account account) {
        accountRepository.save(account);
    }

    @Override
    public UserDetails loadUserByUsername(String login) {
        Account account = findByLogin(login);
        if (account != null) {
            return new CurrentUser(account);
        } else {
            LOGGER.error(ERROR_MSG_ACCOUNT_NOT_FOUND + login);
            throw new UsernameNotFoundException(ERROR_MSG_ACCOUNT_NOT_FOUND + login);
        }
    }
}
