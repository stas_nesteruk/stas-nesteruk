insert into producer (name) values ('Apple');
insert into producer (name) values ('Lenovo');
insert into producer (name) values ('Asus');
insert into producer (name) values ('Dell');
insert into producer (name) values ('Amazon');
insert into producer (name) values ('Samsung');
insert into producer (name) values ('Nokia');
insert into producer (name) values ('Sony');
insert into producer (name) values ('Transcend');

insert into category (name, url) values ('Notebook', '/notebook');
insert into category (name, url) values ('Phone', '/phone');
insert into category (name, url) values ('Tablet', '/tablet');
insert into category (name, url) values ('Mp3-player', '/mp3-player');


insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Lenovo SD391114', 'Monitor 13" / Intel Core I5 (2,4 GHz) / RAM 12 GB / HDD 770 GB / Weight 6,0 kg / Bluetooth / Webcam / LAN / USB / WiFi', '/media/notebook-1.jpeg', 1150, 1, 2, 6);
insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Asus HE0657705', 'Monitor 23" / Intel Core I5 (2,0 GHz) / RAM 4 GB / HDD 630 GB / Weight 3,4 kg / DVD+/-RW / WiFi / Bluetooth / Webcam / USB / Intel HD Graphics / LAN', '/media/notebook-2.jpeg', 1200, 1, 3, 15);
insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Dell NVA0514700', 'Monitor 13" / Intel Core I7 (3,2 GHz) / RAM 8 GB / HDD 260 GB / Weight 4,8 kg / Webcam / Intel HD Graphics / LAN','/media/notebook-1.jpeg',1350,1,4,10);

insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Samsung HC0587187', 'Diagonal 5,2" / Camera: 2.2Mp / RAM 500 Mb / 1400 mA/h / Orange / Weight 240 g / 2 Sim cards / Dictophone / SD','/media/phone-1.jpeg',350,2,6,4);
insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Nokia CEE9493815', 'Diagonal 4,4" / Camera: 2.2Mp / RAM 500 Mb / 1800 mA/h / White / Weight 240 g / Dictophone / Bluetooth / 2 Sim cards / USB','/media/phone-2.jpeg',450,2,7,8);
insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Apple VN6467', 'Diagonal 5,6" / Camera: 2.6Mp / RAM 500 Mb / 1200 mA/h / White / Weight 340 g / 2 Sim cards / Bluetooth / Dictophone', '/media/phone-1.jpeg',1000,2,1,7);

insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Apple FI00663972','Monitor 7" / RAM 2 GB / HDD 210 GB / Blue / Weight 140 g / GPS / WiFi / Webcam','/media/tablet-2.jpeg',600,3,1,4);
insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Samsung PAU06361','Monitor 8" / RAM 2 GB / HDD 190 GB / Blue / Weight 100 g / Bluetooth / GPS / WiFi','/media/tablet-1.jpeg',750,3,6,6);
insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Lenovo UQC1841726','Monitor 9" / RAM 2 GB / HDD 80 GB / White / Weight 260 g / GPS / USB / 3G / HDMI','/media/tablet-1.jpeg',800,3,2,2);

insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Apple HK0390207','Memory 16 Gb / Weight 29 g / White / MP3 / Dictophone / AMV / WAV / FM receiver / WMA / MPEG-4','/media/mp3-1.jpeg',80,4,1,12);
insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Sony QDL497766','Memory 20 Gb / Weight 65 g / White / MP3 / WAV / WMA / OGG / AMV / AVI / MPEG-4 / SD slot','/media/mp3-2.jpeg',50,4,8,15);
insert into product (name, description, image_link, price, id_category, id_producer, count_stock) values ('Transcend ISO35239148','Memory 20 Gb / Weight 32 g / Silver / MP3 / USB / AMV / Bluetooth / FM receiver / OGG','/media/mp3-1.jpeg',20,4,9,10);

insert into ACCOUNT (LOGIN, PASSWORD, EMAIL, ROLE) values ('admin', 'admin', 'admin@admin.com', 'ADMIN');
insert into ACCOUNT (LOGIN, PASSWORD, EMAIL, ROLE) values ('tempuser', 'tempuser', 'user@user.com', 'USER');

