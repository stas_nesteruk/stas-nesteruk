<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="order">
    <c:if test="${not empty ORDER_SUCCESS}">
        <div class="alert alert-success" role="alert">${ORDER_SUCCESS}</div>
    </c:if>
    <c:if test="${not empty ORDER_ERROR}">
        <div class="alert alert-danger" role="alert">${ORDER_ERROR}</div>
    </c:if>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Count</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="item" items="${order}" varStatus="loop">
            <tr id="product${item.product.id}" class="order-item">
                <td class="text-center">
                    <div class="thumbnail" style="width: 120px; height: 120px; margin:0px auto;">
                        <img src="${item.product.imageLink}" alt="${item.product.name}">
                    </div>
                        ${item.product.name}
                </td>
                <td class="price">$ ${item.product.price}</td>
                <td class="count">${item.count}</td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="1" class="text-right"><strong>Total:</strong></td>
            <td colspan="2" class="total">$ ${totalCost}</td>
        </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-lg-2 col-lg-offset-5">
            <a href="/my-orders" class="btn btn-default">My orders</a>
        </div>
    </div>
</div>