<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div>
    <h4 class="title">Add product</h4>
    <div class="row">
        <form:form method="post" action="/edit-product" id="form-edit-product" class="form-edit-product"
                   commandName="editProductForm"
                   enctype="multipart/form-data">
            <div class="col-xs-12 col-sm-6">
                <form:errors path="nameProduct" cssClass="errorMsg pull-right"/>
                <form:input path="nameProduct" class="form-control name-product"
                            placeholder="Product name"/>
                <div class="list-group">
                    <form:errors path="nameCategory" cssClass="errorMsg pull-right"/>
                    <form:select path="nameCategory" class="form-control name-category"
                                 id="addNewProductSelectCategory" style="margin-bottom: 20px;">
                        <c:forEach var="category" items="${categories}">
                            <option>${category.name}</option>
                        </c:forEach>
                    </form:select>
                    <form:errors path="nameProducer" cssClass="errorMsg pull-right"/>
                    <form:select path="nameProducer" class="form-control name-producer"
                                 id="addNewProductSelectProducer" style="margin-bottom: 20px;">
                        <c:forEach var="producer" items="${producers}">
                            <option>${producer.name}</option>
                        </c:forEach>
                    </form:select>
                    <form:errors path="namePrice" cssClass="errorMsg pull-right"/>
                    <form:input type="number" path="namePrice" class="list-group-item form-control name-price"
                                placeholder="Price"/>
                    <form:errors path="nameCount" cssClass="errorMsg pull-right"/>
                    <form:input type="number" path="nameCount" class="list-group-item form-control name-count"
                                placeholder="Count"/>
                </div>
                <div class="list-group">
                    <form:errors path="nameDescription" cssClass="errorMsg pull-right"/>
                    <form:textarea path="nameDescription" class="list-group-item form-control name-description"
                                   placeholder="Description" rows="2"></form:textarea>
                </div>
                <button type="submit" class="btn btn-primary" value="Upload">Add Product</button>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="thumbnail editImage">
                    <img class="product-edit-image" src="${imageLink}" alt="">
                </div>

                <div class="input-group">
                    <form:errors path="file" cssClass="errorMsg pull-right"/>
                    <form:input path="file" type='file' id="edit-product-image-file" value="Upload"/>
                </div>
            </div>
            <br>
        </form:form>
    </div>
</div>