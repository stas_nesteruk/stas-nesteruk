<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Shop</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/static/css/bootstrap.css">
    <link rel="stylesheet" href="/static/css/bootstrap-theme.css">
    <link rel="stylesheet" href="/static/css/font-awesome.css">
    <link rel="stylesheet" href="/static/css/app.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="wrapper">
            <form:form action="/login" class="form-signin" method="post" commandName="loginForm">
                <c:if test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION != null}">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                            ${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message }
                        <c:remove var="SPRING_SECURITY_LAST_EXCEPTION" scope="session"/>
                    </div>
                </c:if>
                <label>Username</label><form:errors path="login" cssClass="errorMsg pull-right"/>
                <form:input path="login" type="text" name="login" class="form-control"/>
                <label>Password</label><form:errors path="password" cssClass="errorMsg pull-right"/>
                <input type="Password" name="password" class="form-control">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form:form>
        </div>
    </div>
</div>
<footer class="footer">
    <jsp:include page="fragment/footer.jsp"/>
</footer>
<script src="/static/js/jquery.js"></script>
<script src="/static/js/bootstrap.js"></script>
<script src="/static/js/app.js"></script>
</body>
</html>