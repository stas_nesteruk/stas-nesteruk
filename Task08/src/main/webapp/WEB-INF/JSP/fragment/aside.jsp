<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="visible-xs-block xs-option-container">
    <a class="pull-left" data-toggle="collapse" href="#productCatalog">Product catalog<span class="caret"></span></a>
</div>
<div id="productCatalog" class="panel panel-default collapse">
    <div class="panel-heading"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Product catalog</div>
    <div class="list-group">
        <c:forEach var="category" items="${categories}">
            <a href="/products${category.url}"
               class="list-group-item ${selectedCategoryUrl == category.url ? 'active' : ''}">${category.name}</a>
        </c:forEach>
    </div>
</div>
