<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#shop-navbar"
                    aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/products">Shop</a>
        </div>
        <div class="collapse navbar-collapse" id="shop-navbar">
            <ul class="nav navbar-nav navbar-right">
                <sec:authorize access="isAuthenticated()">
                    <li>
                        <form action="/search" class="navbar-form search">
                            <div id="findProducts" class="form-group">
                                <input type="text" name="query" class="form-control" placeholder="Search for..."
                                       value="${searchQuery}">
                            </div>
                        </form>
                    </li>
                    <li><a>Welcome, <sec:authentication property="principal.description"/></a></li>
                    <li><a href="/my-orders">My orders</a></li>
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <li><a href="/edit" ${EDIT_MODE == 'ON' ? 'style="font-weight: bold"' : ''}>Edit mode</a>
                        </li>
                    </sec:authorize>
                    <li><a href="sign-out">Sign out</a></li>
                </sec:authorize>
                <sec:authorize access="!isAuthenticated()">
                    <li><a href="/login">Sign in</a></li>
                    <li><a href="/sign-up">Sign up</a></li>
                </sec:authorize>
                <li id="currentShoppingCart" class="dropdown ${not empty CURRENT_SHOPPING_CART ? '': 'hidden'}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="fa fa-shopping-cart" area-hidden="true"></i> Shopping cart(<span
                            class="total-count">${CURRENT_SHOPPING_CART.totalCount}</span>)
                        <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu shopping-cart-desc">
                        Total count: <span class="total-count">${CURRENT_SHOPPING_CART.totalCount}</span><br>
                        <hr class="hr">
                        Total cost: <span class="total-cost">${CURRENT_SHOPPING_CART.totalCost}</span><br>
                        <hr class="hr">
                        <a href="/shopping-cart" class="btn btn-default btn-block">View cart</a>
                    </div>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>