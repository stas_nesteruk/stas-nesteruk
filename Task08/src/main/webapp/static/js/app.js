;$(function () {
    var init = function () {
        initBuyBtn();
        initAddCategoryBtn();
        initAddNewProductBtn();
        initDeleteBtn();
        $('#addToCart').click(addProductToCart);
        $('#addProductModal .count').change(calculateCost);
        $('.remove-product').click(removeProductFromCart);
        $('#add-product-image-file').change(filePreviewAdd);
        $('#edit-product-image-file').change(filePreviewEdit);
        $('#addProduct').click(postNewProduct);
        $('#saveEditProduct').click(postEditProduct);
    };

    var filePreviewAdd = function () {
        var file = this.files[0];
        var reader = new FileReader();
        reader.onload = viewer.load;
        reader.readAsDataURL(file);
        $('#form-add-product .image-preview-default-text').hide();
        $('#form-add-product .product-image').show();
    };
    var filePreviewEdit = function () {
        var file = this.files[0];
        var reader = new FileReader();
        reader.onload = viewer.loadEdit;
        reader.readAsDataURL(file);
    };

    var viewer = {
        load: function (e) {
            $('#form-add-product .product-image').attr('src', e.target.result);
        },
        loadEdit: function (e) {
            $('#form-edit-product .product-edit-image').attr('src', e.target.result);
        }
    }


    var showAddProductModal = function () {
        var idProduct = $(this).attr('data-id-product');
        var product = $('#product' + idProduct);
        $('#addProductModal').attr('data-id-product', idProduct);
        $('#addProductModal .product-image').attr('src', product.find('.thumbnail img').attr('src'));
        $('#addProductModal .name').text(product.find('.name').text());
        var price = product.find('.price').text();
        $('#addProductModal .price').text(price);
        $('#addProductModal .category').text(product.find('.category').text());
        $('#addProductModal .producer').text(product.find('.producer').text());
        $('#addProductModal .count').val(1);
        var count = parseInt(product.find('.count').text());
        console.log("count ", count);
        if (count > 10) {
            $('#addProductModal .count').attr('max', 10);
        } else {
            $('#addProductModal .count').attr('max', count);
        }


        $('#addProductModal .cost').text(price);
        $('#addToCart').removeClass('hidden');
        $('#addToCartLoader').addClass('hidden');
        $('#addProductModal').modal({
            show: true
        });
    };


    var initBuyBtn = function () {
        $(".buy-btn").click(showAddProductModal);
    };

    var initDeleteBtn = function () {
        $(".delete-btn").click(deleteProduct);
    };

    var deleteProduct = function () {
        var idProduct = $(this).attr('data-id-product');
        $.ajax({
            url: '/edit',
            method: 'POST',
            data: {
                idProduct: idProduct,
                command: "DELETE"
            },
            success: function (data) {
                window.location.reload(true);
            },
            error: function () {
                alert("error");
            }
        });
    };

    var initAddCategoryBtn = function () {
        $(".add-category-btn").click(showAddCategoryModal);
    };

    var initAddNewProductBtn = function () {
        $(".add-new-product-btn").click(showAddNewProductModal);
    };

    var showAddNewProductModal = function () {
        $('#addNewProductModal').modal({
            show: true
        });
    };

    var showAddCategoryModal = function () {
        $('#addCategoryModal .name-category').attr('placeholder', 'Enter name');
        $('#addCategoryModal').modal({
            show: true
        });
    };

    var calculateCost = function () {
        var priceStr = $('#addProductModal .price').text();
        var price = parseFloat(priceStr.replace('$', ' '));
        var count = parseInt($('#addProductModal .count').val());
        var min = $('#addProductModal .count').attr('min');
        var max = $('#addProductModal .count').attr('max');
        if (count >= min && count <= max) {
            var cost = price * count;
            $('#addProductModal .cost').text('$ ' + cost);
        } else {
            $('#addProductModal .count').val(1);
            $('#addProductModal .cost').text(priceStr);
        }
    };

    var confirm = function (msg, okFunction) {
        if (window.confirm(msg)) {
            okFunction();
        }
    };

    var removeProductFromCart = function () {
        var btn = $(this);
        confirm('Are you sure?', function () {
            executeRemoveProduct(btn);
        });
    };
    var refreshTotalCost = function () {
        var total = 0;
        $('#shoppingCartProducts .item').each(function (index, value) {
            var count = parseInt($(value).find('.count').text());
            var price = parseFloat($(value).find('.price').text().replace('$', ' '));
            total += price * count;
        });
        $('#shoppingCartProducts .total').text('$' + total);
    }

    var addProductToCart = function () {
        var idProduct = $('#addProductModal').attr('data-id-product');
        var count = $('#addProductModal .count').val();
        $('#addToCart').addClass('hidden');
        $('#addToCartLoader').removeClass('hidden');
        $.ajax({
            url: '/product/add/cart',
            method: 'POST',
            data: {
                idProduct: idProduct,
                count: count
            },
            success: function (data) {
                $('#currentShoppingCart .total-count').text(data.totalCount);
                $('#currentShoppingCart .total-cost').text(data.totalCost);
                $('#currentShoppingCart').removeClass('hidden');
                $('#addProductModal').modal('hide');
            },
            error: function (jqXHR, error, errorThrown) {
                if (jqXHR.status == 400) {
                    alert(jqXHR.responseText);
                } else {
                    alert("Something went wrong");
                }
            }
        });
    };

    var postNewProduct = function () {
        var command = $(this).attr('data-command');
        var name = $('#addNewProductModal .name-product').val();
        var description = $('#addNewProductModal .name-description').val();
        var producer = $('#addNewProductModal .name-producer').val();
        var category = $('#addNewProductModal .name-category').val();
        var imageLink = $('#addNewProductModal .product-image').val();
        var price = $('#addNewProductModal .name-price').val();
        var count = $('#addNewProductModal .name-count').val();

        var sampleFile = document.getElementById("add-product-image-file").files[0];

        var formData = new FormData();
        formData.append('image', sampleFile);
        formData.append('description', description);
        formData.append('producer', producer);
        formData.append('category', category);
        formData.append('imageLink', imageLink);
        formData.append('price', price);
        formData.append('name', name);
        formData.append('count', count);
        formData.append('command', command);

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/edit", true);
        xhr.send(formData);

        xhr.onload = function () {
            //alert(this.responseText);
            if (this.status === 200) {
                window.location.reload(true);
            } else if (this.status === 403) {
                //jumpToLogin();
            }
        }

    };

    var postEditProduct = function () {
        var command = $('#editProductModal').attr('data-command');
        var idProduct = $('#editProductModal').attr('data-id-product');
        var name = $('#editProductModal .name-product').val();
        var description = $('#editProductModal .name-description').val();
        var producer = $('#editProductModal .name-producer').val();
        var category = $('#editProductModal .name-category').val();
        var imageLink = $('#editProductModal .product-edit-image').attr('src');
        var price = $('#editProductModal .name-price').val();
        var count = $('#editProductModal .name-count').val();

        var sampleFile = document.getElementById("edit-product-image-file").files[0];

        var formData = new FormData();
        formData.append('image', sampleFile);
        formData.append('id', idProduct);
        formData.append('description', description);
        formData.append('producer', producer);
        formData.append('category', category);
        formData.append('imageLink', imageLink);
        formData.append('price', price);
        formData.append('name', name);
        formData.append('count', count);
        formData.append('command', command);

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/edit", true);
        xhr.send(formData);

        xhr.onload = function () {
            //alert(this.responseText);
            if (this.status === 200) {
                window.location.reload(true);
            } else if (this.status === 403) {
                //jumpToLogin();
            }
        }
    }

    var executeRemoveProduct = function (btn) {
        var idProduct = btn.attr('data-id-product');
        var count = btn.attr('data-count');
        $.ajax({
            url: '/product/remove/cart',
            method: 'POST',
            data: {
                idProduct: idProduct,
                count: count
            },
            success: function (data) {
                if (data.totalCount === 0) {
                    window.location.href = '/products';
                } else {
                    var prevCount = parseInt($('#product' + idProduct + ' .count').text());
                    var remCount = parseInt(count);
                    if (remCount >= prevCount) {
                        $('#product' + idProduct).remove();
                    } else {
                        btn.click(removeProductFromCart);
                        $('#product' + idProduct + ' .count').text(prevCount - remCount);
                        if (prevCount - remCount == 1) {
                            $('#product' + idProduct + ' .remove-product.all').remove();
                        }
                    }
                    refreshTotalCost();
                }
            },
            error: function () {
                alert("error");
            }
        });
    };
    init();
});