package by.epamlab.inttraining.beans.currencies;

public class USD extends Currency {
    private static final String TYPE_CURRENCY = "USD";
    public USD(int value) {
        super(value);
    }

    public USD(int rubs, int coins){
        super(rubs, coins);
    }

    @Override
    protected String typeCurrencyToPrint() {
        return TYPE_CURRENCY;
    }

    @Override
    public String getTypeCurrency() {
        return TYPE_CURRENCY;
    }
}
