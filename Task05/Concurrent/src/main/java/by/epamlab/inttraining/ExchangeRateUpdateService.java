package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.ExchangeRate;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ExchangeRateUpdateService implements Runnable{
    private static final Logger LOGGER = Logger.getLogger(ExchangeRateUpdateService.class);
    private static final int COUNT_EXCHANGER_STEPS = 2;
    private static final int FIVE = 5;
    private static Map<String, ExchangeRate> currencies = new ConcurrentHashMap<>();
    private static final String HEAD_OUTPUT_SEPARATOR = "//////////////////////////////////";
    private static final String FOOTER_OUTPUT_SEPARATOR = "//////////////////////////////////";
    private static final String USD = "USD";
    private static final String EUR = "EUR";
    private static final int INITIAL_RATE_USD_BUY = 204;
    private static final int INITIAL_RATE_USD_SALE = 206;
    private static final int INITIAL_RATE_EUR_BUY = 226;
    private static final int INITIAL_RATE_EUR_SALE = 228;

    static {
        ExchangeRate usd = new ExchangeRate(USD,
                                          INITIAL_RATE_USD_BUY,
                                          INITIAL_RATE_USD_SALE);
        ExchangeRate eur = new ExchangeRate(EUR,
                                          INITIAL_RATE_EUR_BUY,
                                          INITIAL_RATE_EUR_SALE);
        currencies.put(usd.getCurrencyType(), usd);
        currencies.put(eur.getCurrencyType(), eur);
    }


    public Map<String, ExchangeRate> getCurrencies() {
        return currencies;
    }

    @Override
    public void run() {
        Random random = new Random();
        ExchangeRate oldExchangeRate;
        ExchangeRate newExchangeRate;
        int buy;
        int sale;
        int marginBuy;
        int marginSale;
        while(true) {
            int seconds = random.nextInt(FIVE + 1) + FIVE;
            int step = random.nextInt(COUNT_EXCHANGER_STEPS);
            try {
                Set<String> keys = currencies.keySet();
                for (String currencyKey : keys) {
                    oldExchangeRate = currencies.get(currencyKey);
                    newExchangeRate = new ExchangeRate(oldExchangeRate);
                    buy = oldExchangeRate.getCurrencyBuy();
                    sale = oldExchangeRate.getCurrencySale();
                    marginBuy = buy / Constants.HUNDRED;
                    marginSale = sale / Constants.HUNDRED;
                    switch (step) {
                        case 0:
                            buy += marginBuy;
                            newExchangeRate.setCurrencyBuy(buy);
                            sale += marginSale;
                            newExchangeRate.setCurrencySale(sale);
                            currencies.replace(oldExchangeRate.getCurrencyType(),
                                    oldExchangeRate,
                                    newExchangeRate);
                            break;
                        case 1:
                            buy -= marginBuy;
                            newExchangeRate.setCurrencyBuy(buy);
                            sale -= marginSale;
                            newExchangeRate.setCurrencySale(sale);
                            currencies.replace(oldExchangeRate.getCurrencyType(),
                                    oldExchangeRate,
                                    newExchangeRate);
                            break;
                        default:
                            LOGGER.error(Constants.ERROR_MSG_WRONG_ACTION);
                            break;
                    }
                }
                exchangeRateToPrint();
                Thread.sleep(seconds * Constants.THOUSAND);
            } catch (InterruptedException ex) {
                LOGGER.error(ex.getMessage());
                Thread.currentThread().interrupt();
            }
        }
    }

    private void exchangeRateToPrint(){
        final StringBuilder sb = new StringBuilder();
        sb.append("\n").append(HEAD_OUTPUT_SEPARATOR).append("\n");
        sb.append("\t\tExchange rate").append("\n");
        for (ExchangeRate rate:currencies.values()) {
            sb.append(rate).append("\n");
        }
        sb.append(FOOTER_OUTPUT_SEPARATOR).append("\n");
        LOGGER.info(sb);
    }

}
