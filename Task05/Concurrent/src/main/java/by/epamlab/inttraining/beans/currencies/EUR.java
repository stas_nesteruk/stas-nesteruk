package by.epamlab.inttraining.beans.currencies;

public class EUR extends Currency {
    private static final String TYPE_CURRENCY = "EUR";

    public EUR(int value) {
        super(value);
    }
    public EUR(int rubs, int coins){
        super(rubs, coins);
    }
    @Override
    protected String typeCurrencyToPrint() {
        return TYPE_CURRENCY;
    }

    @Override
    public String getTypeCurrency() {
        return TYPE_CURRENCY;
    }
}
