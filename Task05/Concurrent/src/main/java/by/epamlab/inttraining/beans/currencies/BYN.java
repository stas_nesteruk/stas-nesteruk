package by.epamlab.inttraining.beans.currencies;

public class BYN extends Currency{
    private static final String TYPE_CURRENCY = "BYN";

    public BYN(int value) {
        super(value);
    }

    @Override
    protected String typeCurrencyToPrint() {
        return TYPE_CURRENCY;
    }

    @Override
    public String getTypeCurrency() {
        return TYPE_CURRENCY;
    }
}
