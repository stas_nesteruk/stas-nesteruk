package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.utils.Constants;

public class ExchangeRate {
    private String currencyType;
    private int currencyBuy;
    private int currencySale;

    public ExchangeRate(String currencyType, int currencyBuy, int currencySale){
        this.currencyType = currencyType;
        this.currencyBuy = currencyBuy;
        this.currencySale = currencySale;
    }

    public ExchangeRate(ExchangeRate rate){
        this.currencyType = rate.getCurrencyType();
        this.currencyBuy = rate.getCurrencyBuy();
        this.currencySale = rate.getCurrencySale();
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public int getCurrencyBuy() {
        return currencyBuy;
    }

    public void setCurrencyBuy(int currencyBuy) {
        this.currencyBuy = currencyBuy;
    }

    public int getCurrencySale() {
        return currencySale;
    }

    public void setCurrencySale(int currencySale) {
        this.currencySale = currencySale;
    }

    private static String convert(int copecks){
        return copecks / Constants.HUNDRED + "." +
                copecks / Constants.TEN % Constants.TEN +
                copecks % Constants.TEN;
    }

    @Override
    public String toString() {
        return getCurrencyType() + "      " +
                convert(currencyBuy) + " " +
                convert(currencySale);
    }
}
