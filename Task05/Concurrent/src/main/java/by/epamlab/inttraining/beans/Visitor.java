package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.beans.currencies.Currency;
import javafx.util.Pair;

import java.util.*;

public class Visitor implements Runnable{
    private static final int ONE_HUNDRED_IN_CENTS = 10000;
    private static final int ONE_THOUSAND_IN_CENTS = 100000;
    private String visitorName;
    private Exchanger[] exchangers;
    private List<Currency> currencies;
    private boolean queue;            // true - Exchanger / false Speculator

    public Visitor(){}


    public Visitor(String visitorName, Exchanger[] exchangers, Currency... currencies) {
        this.visitorName = visitorName;
        this.exchangers = exchangers;
        this.currencies = new ArrayList<>();
        this.currencies.addAll(Arrays.asList(currencies));
    }

    public String getVisitorName() {
        return visitorName;
    }

    @Override
    public String toString() {
        return visitorName + " " + currencies;
    }

    public void setQueue(boolean queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        if(queue){
            exchangers[0].putVisitor(this);
        }else {
            exchangers[1].putVisitor(this);
        }
    }

    public Map<Currency, Integer> exchange(){
        Random random = new Random();
        Currency mostMoney = currencies.get(0);
        for (Currency cyrrency : currencies) {
            if(cyrrency.getCopecks() > mostMoney.getCopecks())
                mostMoney = cyrrency;
        }
        int amountMoney;
        boolean flag = false;
        do{
            amountMoney = random.nextInt(ONE_THOUSAND_IN_CENTS - ONE_HUNDRED_IN_CENTS + 1) +
                    ONE_HUNDRED_IN_CENTS;
            if(amountMoney < mostMoney.getCopecks()){
                flag = true;
            }
        }while (!flag);
        Map<Currency, Integer> mostMoneyAndAmount = Collections.singletonMap(mostMoney,amountMoney);
        return mostMoneyAndAmount;
    }

    public void setCurrency(Currency exchangedMoney) {
        boolean foundCurrency = false;
        String typeCurrency = exchangedMoney.getTypeCurrency();
        for (Currency currency : currencies) {
            if (currency.getTypeCurrency().equals(typeCurrency)) {
                currency.add(exchangedMoney.getCopecks());
                foundCurrency = true;
            }
        }
        if (!foundCurrency) {
            currencies.add(exchangedMoney);
        }
    }
}
