package by.epamlab.inttraining.utils;

public class Constants {
    public static final int TEN = 10;
    public static final int HUNDRED = 100;
    public static final int THOUSAND = 1000;
    public static final String ERROR_MSG_CURRENCY_DONT_FOUND = "Currencies don't found!";
    public static final String ERROR_MSG_AMOUNT_OF_MONEY = "Amount must be between 100 and 1000!!!";
    public static final String ERROR_MSG_WRONG_ACTION = "Wrong action!";
    public static final String[] VISITOR_NAMES = {
        "John McMakin",
        "Curtis Irvine",
        "Aaron Carlson",
        "Diana Maxwell",
        "Maria Riddell",
        "Betty May",
        "Ashley Massey",
        "Erik Nichols",
        "Florence Duron",
        "John King",
        "James Garcia",
        "Aaron Armwood",
        "Patrick Bailey",
        "Erica Chambers",
        "Dave Griffin",
        "Lillian Rodriquez",
        "Mario Rhodes",
        "Cathy Robinson",
        "Brent Rose",
        "Daniel Carroll"};


}
