package by.epamlab.inttraining;

import org.apache.log4j.Logger;

import java.util.concurrent.ForkJoinPool;

public class Runner {
    private static final int NUMBER = 1_000_000_000;
    private static final int COUNT_PARALLELISM = 8;
    private static final Logger LOGGER = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        SumOfNumbers sumOfNumbers = new SumOfNumbers(0,NUMBER);
        ForkJoinPool pool = new ForkJoinPool(COUNT_PARALLELISM);
        long result = pool.invoke(sumOfNumbers);
        long endTime = System.currentTimeMillis();
        LOGGER.info(result);
        LOGGER.info(endTime - startTime);
    }
}