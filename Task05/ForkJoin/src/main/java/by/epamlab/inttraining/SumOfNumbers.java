package by.epamlab.inttraining;

import java.util.concurrent.RecursiveTask;

public class SumOfNumbers extends RecursiveTask<Long> {
    private static final int THRESHOLD = 10_000;
    private static final int TWO = 2;
    private final int start;
    private final int end;

    public SumOfNumbers(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {
        int length = end - start;
        if(length <= THRESHOLD){
            long result = 0;
            for(int i = start; i < end; i++){
                result += i + 1;
            }
            return result;
        }
        SumOfNumbers firstTask = new SumOfNumbers(start, start + length/TWO);
        firstTask.fork();
        SumOfNumbers secondTask = new SumOfNumbers(start + length/TWO, end);
        Long secondResult = secondTask.compute();
        Long firstResult = firstTask.join();
        return firstResult + secondResult;
    }
}
