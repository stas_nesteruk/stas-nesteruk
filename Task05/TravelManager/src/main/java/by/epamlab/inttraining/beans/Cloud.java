package by.epamlab.inttraining.beans;

import java.util.concurrent.atomic.AtomicInteger;

public class Cloud {
    private AtomicInteger countPhotos = new AtomicInteger(0);

    public int postAndGet(int value){
        countPhotos.addAndGet(value);
        return 0;
    }

    public int get(){
        return countPhotos.get();
    }

}
