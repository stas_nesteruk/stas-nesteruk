package by.epamlab.inttraining.events.spain;

import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.utils.Constants;

public class SpainTakeAPhoto implements Event {
    @Override
    public void execute(Tourist tourist) {
        tourist.toTakeAPhoto();
    }

    @Override
    public String getEventName() {
        return Constants.SPAIN_TAKE_PHOTO;
    }
}
