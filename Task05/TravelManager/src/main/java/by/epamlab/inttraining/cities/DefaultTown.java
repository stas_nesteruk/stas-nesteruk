package by.epamlab.inttraining.cities;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.events.defaulttown.ArriveDepartureDefaultPoint;
import by.epamlab.inttraining.utils.Constants;


public class DefaultTown implements TownAction {

    @Override
    public Event[] execute(Tourist tourist) {
        int tasks = DefaultTownEvents.values().length;
        Event[] events = new Event[tasks];
        for (int i = 0; i < tasks; i++) {
            events[i] = DefaultTownEvents.values()[i].getEvent();
        }
        return events;
    }

    @Override
    public String getTownName(){
        return Constants.DEFAULT_TOWN;
    }

}

enum DefaultTownEvents{
    ARRIVE_DEPARTURE_POINT{
        @Override
        public Event getEvent() {
            return new ArriveDepartureDefaultPoint();
        }
    };
    public abstract Event getEvent();
}
