package by.epamlab.inttraining.services;

import by.epamlab.inttraining.events.rome.RomePastaRestaurant;

public class RomePastaCustomerService implements Runnable {
    @Override
    public void run() {
        while(true) {
            while (RomePastaRestaurant.BARRIER.getParties() != 0) {
                RomePastaRestaurant.takeTourist();
            }
        }
    }
}
