package by.epamlab.inttraining.cities;

import by.epamlab.inttraining.beans.Bus;
import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.events.spain.ArriveDepartureSpainPoint;
import by.epamlab.inttraining.events.spain.SpainTakeAPhoto;
import by.epamlab.inttraining.events.spain.SpainVisitToBeach;
import by.epamlab.inttraining.utils.Constants;

public class Spain implements TownAction {

    @Override
    public Event[] execute(Tourist tourist) {
        int tasks = SpainEvents.values().length;
        Event[] events = new Event[tasks];
        for (int i = 0; i < tasks; i++) {
            events[i] = SpainEvents.values()[i].getEvent();
        }
        return events;
    }

    @Override
    public String getTownName() {
        return Constants.SPAIN;
    }
}

enum SpainEvents{
    TAKE_A_PHOTO{
        @Override
        public Event getEvent() {
            return new SpainTakeAPhoto();
        }
    },
    VISIT_TO_BEACH{
        @Override
        public Event getEvent() {
            return new SpainVisitToBeach();
        }
    },
    ARRIVE_DEPARTURE_POINT{
        @Override
        public Event getEvent() {
            return new ArriveDepartureSpainPoint();
        }
    };
    public abstract Event getEvent();
}
