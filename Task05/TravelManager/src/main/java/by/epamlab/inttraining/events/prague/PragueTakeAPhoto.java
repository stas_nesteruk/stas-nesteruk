package by.epamlab.inttraining.events.prague;

import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.utils.Constants;

public class PragueTakeAPhoto implements Event {
    public void execute(Tourist tourist){
        tourist.toTakeAPhoto();
    }

    @Override
    public String getEventName() {
        return Constants.PRAGUE_TAKE_PHOTO;
    }
}
