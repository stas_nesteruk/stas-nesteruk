package by.epamlab.inttraining.exception;

public class TownException extends RuntimeException {
    public TownException() {}
    public TownException(String message) {
        super(message);
    }
    public TownException(Throwable cause) {
        super(cause);
    }
    public TownException(String message, Throwable cause) {
        super(message, cause);
    }
}
