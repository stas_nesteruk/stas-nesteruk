package by.epamlab.inttraining.events.paris;

import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

import java.util.concurrent.Semaphore;

public class ParisVisitToLouvre implements Event {
    private static final Logger LOGGER = Logger.getLogger(ParisVisitToLouvre.class);
    private static final int CUSTOMER_SERVICE_TIME = 1000;
    private static final int MAX_SIZE_QUEUE = 5;

    private static Semaphore semaphore;

    static{
        semaphore = new Semaphore(MAX_SIZE_QUEUE, true);
    }

    @Override
    public void execute(Tourist tourist) {
        try {
            semaphore.acquire();
            LOGGER.info(tourist.getName() + Constants.PARIS_LOOK_AT_MONA_LISA);
            Thread.sleep(CUSTOMER_SERVICE_TIME);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
        LOGGER.info(tourist.getName() + Constants.PARIS_LEAVE_FROM_MONA_LISA);
        semaphore.release();
    }

    @Override
    public String getEventName() {
        return Constants.PARIS_VISIT_TO_LOUVRE;
    }
}
