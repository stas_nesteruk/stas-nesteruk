package by.epamlab.inttraining.cities;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.events.paris.ArriveDepartureParisPoint;
import by.epamlab.inttraining.events.paris.ParisTakeAPhoto;
import by.epamlab.inttraining.events.paris.ParisVisitToLouvre;
import by.epamlab.inttraining.utils.Constants;

public class Paris implements TownAction {
    @Override
    public Event[] execute(Tourist tourist) {
        int tasks = ParisEvents.values().length;
        Event[] events = new Event[tasks];
        for (int i = 0; i < tasks; i++) {
            events[i] = ParisEvents.values()[i].getEvent();
        }
        return events;
    }

    @Override
    public String getTownName() {
        return Constants.PARIS;
    }
}

enum ParisEvents{
    TAKE_A_PHOTO{
        @Override
        public Event getEvent() {
            return new ParisTakeAPhoto();
        }
    },
    VISIT_TO_LOUVRE{
        @Override
        public Event getEvent() {
            return new ParisVisitToLouvre();
        }
    },
    ARRIVE_DEPARTURE_POINT{
        @Override
        public Event getEvent() {
            return new ArriveDepartureParisPoint();
        }
    };
    public abstract Event getEvent();
}