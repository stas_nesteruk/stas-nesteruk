package by.epamlab.inttraining.services;

import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

public class CustomerService implements Runnable{
    private static final Logger LOGGER = Logger.getLogger(CustomerService.class);
    @Override
    public void run() {
        LOGGER.info(Constants.CUSTOMER_SERVICE);
    }
}
