package by.epamlab.inttraining.services;

import by.epamlab.inttraining.events.rome.RomePizzaRestaurant;

public class RomePizzaCustomerService implements Runnable {
    @Override
    public void run() {
        while(true) {
            while (RomePizzaRestaurant.BARRIER.getParties() != 0) {
                RomePizzaRestaurant.takeTourist();
            }
        }
    }
}
