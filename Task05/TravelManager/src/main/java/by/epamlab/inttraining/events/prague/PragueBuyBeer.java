package by.epamlab.inttraining.events.prague;

import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.utils.Constants;

public class PragueBuyBeer implements Event {

    @Override
    public void execute(Tourist tourist) {
        BeerStore.putTourist(tourist);
    }

    @Override
    public String getEventName() {
        return Constants.PRAGUE_BUY_BEER;
    }


}
