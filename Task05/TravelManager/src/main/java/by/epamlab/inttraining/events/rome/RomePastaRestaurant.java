package by.epamlab.inttraining.events.rome;

import by.epamlab.inttraining.services.CustomerService;
import by.epamlab.inttraining.beans.Tourist;
import org.apache.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CyclicBarrier;

public class RomePastaRestaurant{
    private static final Logger LOGGER = Logger.getLogger(RomePastaRestaurant.class);
    private static BlockingQueue<Tourist> secondRestaurantPaste;
    private static final int MAX_SIZE_VISITORS = 20;
    private static final int MAX_QUEUE_CAPACITY = 5;
    public static final CyclicBarrier BARRIER = new CyclicBarrier(MAX_QUEUE_CAPACITY, new CustomerService());

    static {
        secondRestaurantPaste = new ArrayBlockingQueue<>(MAX_SIZE_VISITORS, true);
    }

    public static void putTourist(Tourist tourist){
        try {
            secondRestaurantPaste.put(tourist);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    public static void takeTourist(){
        Tourist tourist = null;
        try {
            tourist = secondRestaurantPaste.take();
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

}
