package by.epamlab.inttraining.events;

import by.epamlab.inttraining.beans.Tourist;

public interface Event {
    void execute(Tourist tourist);
    String getEventName();
}
