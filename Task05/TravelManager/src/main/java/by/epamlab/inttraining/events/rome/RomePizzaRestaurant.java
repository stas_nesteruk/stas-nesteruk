package by.epamlab.inttraining.events.rome;

import by.epamlab.inttraining.services.CustomerService;
import by.epamlab.inttraining.beans.Tourist;
import org.apache.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CyclicBarrier;

public class RomePizzaRestaurant{
    private static final Logger LOGGER = Logger.getLogger(RomePizzaRestaurant.class);
    private static BlockingQueue<Tourist> firstRestaurantPizza;
    private static final int MAX_SIZE_VISITORS = 20;
    private static final int MAX_QUEUE_CAPACITY = 5;
    public static final CyclicBarrier BARRIER = new CyclicBarrier(MAX_QUEUE_CAPACITY, new CustomerService());

    static {
        firstRestaurantPizza = new ArrayBlockingQueue<>(MAX_SIZE_VISITORS, true);
    }

    public static void putTourist(Tourist tourist){
        try {
            firstRestaurantPizza.put(tourist);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    public static void takeTourist(){
        try {
            firstRestaurantPizza.take();
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }
}
