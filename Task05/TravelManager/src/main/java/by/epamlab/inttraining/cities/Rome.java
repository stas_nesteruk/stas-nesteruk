package by.epamlab.inttraining.cities;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.events.rome.ArriveDepartureRomePoint;
import by.epamlab.inttraining.events.rome.RomeTakeAPhoto;
import by.epamlab.inttraining.events.rome.RomeVisitRestaurant;
import by.epamlab.inttraining.utils.Constants;

public class Rome implements TownAction {
    @Override
    public Event[] execute(Tourist tourist) {
        int tasks = RomeEvents.values().length;
        Event[] events = new Event[tasks];
        for (int i = 0; i < tasks; i++) {
            events[i] = RomeEvents.values()[i].getEvent();
        }
        return events;
    }

    @Override
    public String getTownName() {
        return Constants.ROME;
    }
}

enum RomeEvents{
    TAKE_A_PHOTO{
        @Override
        public Event getEvent() {
            return new RomeTakeAPhoto();
        }
    },
    VISIT_TO_RESTAURANT{
        @Override
        public Event getEvent() {
            return new RomeVisitRestaurant();
        }
    },
    ARRIVE_DEPARTURE_POINT{
        @Override
        public Event getEvent() {
            return new ArriveDepartureRomePoint();
        }
    };
    public abstract Event getEvent();
}