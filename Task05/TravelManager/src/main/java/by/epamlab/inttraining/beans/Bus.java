package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Bus{
    private static final Logger LOGGER = Logger.getLogger(Bus.class);
    private List<Tourist> passengers;

    public Bus(){
        passengers = new CopyOnWriteArrayList<>();
    }

    public void loading(Tourist tourist){
        passengers.add(tourist);
    }

    public void unloading(){
        passengers.clear();
    }

    public void visitTo(String nameTown) {
        LOGGER.info(Constants.BUS_MSG_ARRIVED_AT + nameTown);
        unloading();
    }
}
