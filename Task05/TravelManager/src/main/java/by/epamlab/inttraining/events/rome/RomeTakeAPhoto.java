package by.epamlab.inttraining.events.rome;

import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.utils.Constants;

public class RomeTakeAPhoto implements Event {
    @Override
    public void execute(Tourist tourist){
        tourist.toTakeAPhoto();
    }

    @Override
    public String getEventName() {
        return Constants.ROME_TAKE_PHOTO;
    }
}
