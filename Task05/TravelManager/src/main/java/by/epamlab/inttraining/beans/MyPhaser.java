package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

import java.util.concurrent.Phaser;

public class MyPhaser extends Phaser {
    private static final Logger LOGGER = Logger.getLogger(MyPhaser.class);
    private int numPhases;

    public MyPhaser(int parties, int phaseCount) {
        super(parties);
        numPhases = phaseCount - 1;
    }

    @Override
    protected boolean onAdvance(int p, int regParties) {
        switch (p){
            case 0:
                LOGGER.info(Constants.MSG_PHASE_ONE);
                break;
            case 1:
                LOGGER.info(Constants.MSG_PHASE_TWO);
                break;
            case 2:
                LOGGER.info(Constants.MSG_PHASE_THREE);
                break;
            case 3:
                LOGGER.info(Constants.MSG_PHASE_FOUR);
                break;
            case 4:
                LOGGER.info(Constants.MSG_PHASE_FIVE);
                break;
        }
        return (p == numPhases || regParties == 0);
    }
}
