package by.epamlab.inttraining.events.prague;

import by.epamlab.inttraining.beans.Tourist;
import org.apache.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BeerStore{
    private static final Logger LOGGER = Logger.getLogger(BeerStore.class);
    private static final int MAXIMUM_QUEUE_CAPACITY = 1;
    private static final int CUSTOMER_SERVICE_TIME = 1500;

    private static BlockingQueue<Tourist> tourists;

    static{
        tourists = new ArrayBlockingQueue<>(MAXIMUM_QUEUE_CAPACITY, true);
    }
    public BeerStore() {
    }

    public static void putTourist(Tourist tourist){
        try {
            tourists.put(tourist);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    public static void buyBeer(){
        Tourist tourist = null;
        try {
            tourist = tourists.take();
            Thread.sleep(CUSTOMER_SERVICE_TIME);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

}
