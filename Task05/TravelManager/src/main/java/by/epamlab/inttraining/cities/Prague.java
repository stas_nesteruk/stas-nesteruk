package by.epamlab.inttraining.cities;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.events.prague.ArriveDeparturePraguePoint;
import by.epamlab.inttraining.events.prague.PragueBuyBeer;
import by.epamlab.inttraining.events.prague.PragueTakeAPhoto;
import by.epamlab.inttraining.utils.Constants;

public class Prague implements TownAction {

    @Override
    public Event[] execute(Tourist tourist) {
        int tasks = PragueEvents.values().length;
        Event[] events = new Event[tasks];
        for (int i = 0; i < tasks; i++) {
            events[i] = PragueEvents.values()[i].getEvent();
        }
        return events;
    }

    @Override
    public String getTownName() {
        return Constants.PRAGUE;
    }
}

enum PragueEvents{
    TAKE_A_PHOTO{
        @Override
        public Event getEvent() {
            return new PragueTakeAPhoto();
        }
    },
    BUY_BEER{
        @Override
        public Event getEvent() {
            return new PragueBuyBeer();
        }
    },
    ARRIVE_DEPARTURE_POINT{
        @Override
        public Event getEvent() {
            return new ArriveDeparturePraguePoint();
        }
    };
    public abstract Event getEvent();

}
