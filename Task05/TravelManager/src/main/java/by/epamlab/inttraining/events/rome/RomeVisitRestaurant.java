package by.epamlab.inttraining.events.rome;

import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.utils.Constants;

public class RomeVisitRestaurant implements Event {
    @Override
    public void execute(Tourist tourist) {
        if(tourist.isQueue()){
            RomePizzaRestaurant.putTourist(tourist);
        }else {
            RomePastaRestaurant.putTourist(tourist);
        }
    }

    @Override
    public String getEventName() {
        return Constants.ROME_VISIT_TO_RESTAURANT;
    }
}
