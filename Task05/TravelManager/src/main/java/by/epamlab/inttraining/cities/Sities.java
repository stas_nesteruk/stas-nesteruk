package by.epamlab.inttraining.cities;


public enum Sities {
    PARIS{
        {
            this.town = new Paris();
        }
    },
    ROME{
        {
            this.town = new Rome();
        }
    },
    PRAGUE{
        {
            this.town = new Prague();
        }
    },
    DEFAULT{
        {
            this.town = new DefaultTown();
        }
    },
    SPAIN{
        {
            this.town = new Spain();
        }
    };
    TownAction town;
    public TownAction getTown(){
        return town;
    }

}
