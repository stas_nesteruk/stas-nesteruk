package by.epamlab.inttraining.utils;

public class Constants {
    public static final String ERROR_MSG_WRONG_TOWN = "Wrong town";
    public static final String MANAGER_MSG = "Все пассажиры заняли свои места!\n";
    public static final String DEFAULT_TOWN = "Начальный город";
    public static final String PRAGUE = "Прагу";
    public static final String PARIS = "Париж";
    public static final String ROME = "Рим";
    public static final String SPAIN = "Испанию";
    public static final String SECONDS = " секунд";
    public static final String MSG_PHASE_ONE = "Автобус отправляется в Прагу";
    public static final String MSG_PHASE_TWO = "Автобус отправляется в Париж";
    public static final String MSG_PHASE_THREE = "Автобус отправляется в Риж";
    public static final String MSG_PHASE_FOUR = "Автобус отправляется в Испанию";
    public static final String MSG_PHASE_FIVE = "Автобус возвращается в пункт отправки";
    public static final String MANAGER_MSG_COUNT_PHOTOS_IN_CLOUD = "Количество фотографий ";
    public static final String MANAGER_MSG_TOUR_COMPLETED = "Тур завершен!";
    public static final String TOURIST_MSG_CREATED_PHOTO = " сделал фото";
    public static final String TOURIST_MSG_BUY_BEER = " купил пиво";
    public static final String TOURIST_MSG_WAITING_HIS_TURN = " ждет своей очереди";
    public static final String TOURIST_MSG_WENT_TO_EAT = " пошел есть";
    public static final String TOURIST_MSG_EAT_IN = " поел за ";
    public static final String TOURIST_MSG_COME_TO_THE_BEACH = " пришел на пляж";
    public static final String TOURIST_MSG_TUNNED = " позагарал ";
    public static final String TOURIST_MSG_DRUNK_BEER = " выпил пиво за ";
    public static final String BUS_MSG_TAKE_HIS_SEAT = " занял свое место в автобусе";
    public static final String BUS_MSG_ARRIVED_AT = "Прибыли в ";
    public static final String ARRIVE_DEPARTURE_DEFAULT_POINT = "ArriveDepartureDefaultPoint";
    public static final String ARRIVE_DEPARTURE_PARIS_POINT = "ArriveDepartureParisPoint";
    public static final String ARRIVE_DEPARTURE_PRAGUE_POINT = "ArriveDeparturePraguePoint";
    public static final String ARRIVE_DEPARTURE_ROME_POINT = "ArriveDepartureRomePoint";
    public static final String ARRIVE_DEPARTURE_SPAIN_POINT = "ArriveDepartureSpainPoint";
    public static final String PARIS_TAKE_PHOTO = "Paris photo";
    public static final String ROME_TAKE_PHOTO = "Rome photo";
    public static final String PRAGUE_TAKE_PHOTO = "Prague photo";
    public static final String SPAIN_TAKE_PHOTO = "Spain photo";
    public static final String PARIS_LOOK_AT_MONA_LISA = " смотрит на Мону Лизу";
    public static final String PARIS_LEAVE_FROM_MONA_LISA = " уходит";
    public static final String PARIS_VISIT_TO_LOUVRE = "Поситить Лувр";
    public static final String PRAGUE_BUY_BEER = "Купить пиво";
    public static final String ROME_VISIT_TO_RESTAURANT = "Посетить ресторан";
    public static final String SPAIN_VISIT_TO_BEACH = "Посетить пляж";

    public static final String CUSTOMER_SERVICE = "Обслуживание клиентов";
    public static final String[] TOURIST_NAMES = {
            "John McMakin",
            "Curtis Irvine",
            "Aaron Carlson",
            "Diana Maxwell",
            "Maria Riddell",
            "Betty May",
            "Ashley Massey",
            "Erik Nichols",
            "Florence Duron",
            "John King",
            "James Garcia",
            "Aaron Armwood",
            "Patrick Bailey",
            "Erica Chambers",
            "Dave Griffin",
            "Lillian Rodriquez",
            "Mario Rhodes",
            "Cathy Robinson",
            "Brent Rose",
            "Daniel Carroll"};

}
