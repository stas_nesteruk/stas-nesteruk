package by.epamlab.inttraining;

import by.epamlab.inttraining.services.PragueBeerStoreCustomerService;
import by.epamlab.inttraining.services.RomePastaCustomerService;
import by.epamlab.inttraining.services.RomePizzaCustomerService;

public class Runner {
    public static void main(String[] args) {
        TravelManager manager = new TravelManager();
        PragueBeerStoreCustomerService pragueBreweryStoreService = new PragueBeerStoreCustomerService();
        Thread breweryStore = new Thread(pragueBreweryStoreService);
        breweryStore.setDaemon(true);
        breweryStore.start();

        RomePizzaCustomerService romePizzaCustomerService = new RomePizzaCustomerService();
        Thread romePizzaStore = new Thread(romePizzaCustomerService);
        romePizzaStore.setDaemon(true);
        romePizzaStore.start();

        RomePastaCustomerService romePastaCustomerService = new RomePastaCustomerService();
        Thread romePastaStore = new Thread(romePastaCustomerService);
        romePastaStore.setDaemon(true);
        romePastaStore.start();

        manager.execute();
    }
}
