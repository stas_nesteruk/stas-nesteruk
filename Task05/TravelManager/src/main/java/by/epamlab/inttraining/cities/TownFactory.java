package by.epamlab.inttraining.cities;

import by.epamlab.inttraining.exception.TownException;
import by.epamlab.inttraining.utils.Constants;

public class TownFactory {
    public TownAction defineTown(String currentTown) {
        try {
            Sities currentEnum = Sities.valueOf(currentTown.toUpperCase());
            return currentEnum.getTown();
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new TownException(currentTown + Constants.ERROR_MSG_WRONG_TOWN, e);
        }
    }

}
