package by.epamlab.inttraining.cities;

import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.events.Event;

public interface TownAction {
    Event[] execute(Tourist tourist);
    String getTownName();
}
