package by.epamlab.inttraining.services;

import by.epamlab.inttraining.events.prague.BeerStore;

public class PragueBeerStoreCustomerService implements Runnable {

    @Override
    public void run() {
        while (true) {
            BeerStore.buyBeer();
        }
    }
}
