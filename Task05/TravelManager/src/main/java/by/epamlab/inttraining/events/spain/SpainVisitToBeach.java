package by.epamlab.inttraining.events.spain;

import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

public class SpainVisitToBeach implements Event {
    private static final Logger LOGGER = Logger.getLogger(SpainVisitToBeach.class);
    @Override
    public void execute(Tourist tourist) {
        LOGGER.info(tourist.getName() + Constants.TOURIST_MSG_COME_TO_THE_BEACH);
    }

    @Override
    public String getEventName() {
        return Constants.SPAIN_VISIT_TO_BEACH;
    }
}
