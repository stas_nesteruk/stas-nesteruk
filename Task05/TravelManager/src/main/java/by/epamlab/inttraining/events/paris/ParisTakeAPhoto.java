package by.epamlab.inttraining.events.paris;

import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.utils.Constants;

public class ParisTakeAPhoto implements Event {
    @Override
    public void execute(Tourist tourist){
        tourist.toTakeAPhoto();
    }

    @Override
    public String getEventName() {
        return Constants.PARIS_TAKE_PHOTO;
    }
}
