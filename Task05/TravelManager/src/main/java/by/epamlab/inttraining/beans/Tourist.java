package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.TravelManager;
import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.events.rome.RomePastaRestaurant;
import by.epamlab.inttraining.events.rome.RomePizzaRestaurant;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Phaser;

public class Tourist implements Runnable{
    private static final Logger LOGGER = Logger.getLogger(Tourist.class);

    private static final int THOUSAND = 1000;
    private static final int FIVE = 5;

    private String name;
    private final int eatingTime;
    private int numberOfPhotos;
    private TravelManager manager;
    private Phaser phaser;
    private Bus bus;
    private boolean queue = false;

    public boolean isQueue() {
        return queue;
    }

    public Tourist(String name, TravelManager manager) {
        this.name = name;
        this.numberOfPhotos = 0;
        this.phaser = manager.getPhaser();
        phaser.register();
        Random random = new Random();
        eatingTime = random.nextInt(FIVE) + 1;
        this.manager = manager;
        this.bus = manager.getBus();
    }

    public Bus getBus() {
        return bus;
    }

    public String getName() {
        return name;
    }

    public void setQueue(boolean queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
        while (!phaser.isTerminated()){
            Event[] events = manager.getCurrentTown().execute(this);
            for (Event event : events) {
                event.execute(this);
                switch (manager.getCurrentTown().getTownName()){
                    case Constants.PRAGUE :
                        if(event.getEventName().equals(Constants.PRAGUE_BUY_BEER)){
                            LOGGER.info(name + Constants.TOURIST_MSG_BUY_BEER);
                            drinkBeer();
                        }
                        break;
                    case Constants.ROME :
                        if(event.getEventName().equals(Constants.ROME_VISIT_TO_RESTAURANT)){
                            LOGGER.info(name + Constants.TOURIST_MSG_WAITING_HIS_TURN);
                            eatToRestaurant();
                        }
                        break;
                    case Constants.SPAIN :
                        if(event.getEventName().equals(Constants.SPAIN_VISIT_TO_BEACH)) {
                            tunnedOnBeach();
                        }
                        break;
                }
            }
            setNumberOfPhotos(manager.getCloud().postAndGet(numberOfPhotos));
            phaser.arriveAndAwaitAdvance();
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage());
                Thread.currentThread().interrupt();
            }
        }
    }

    private void setNumberOfPhotos(int numberOfPhotos) {
        this.numberOfPhotos = numberOfPhotos;
    }

    public void toTakeAPhoto() {
        this.numberOfPhotos++;
        LOGGER.info(name + Constants.TOURIST_MSG_CREATED_PHOTO);
    }

    @Override
    public String toString() {
        return name;
    }

    private void drinkBeer(){
        try {
            Thread.sleep((long) eatingTime * THOUSAND);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
        LOGGER.info(name + Constants.TOURIST_MSG_DRUNK_BEER + eatingTime + Constants.SECONDS);
    }

    private void eatToRestaurant(){
        try {
            if (queue) {
                RomePizzaRestaurant.BARRIER.await();
                LOGGER.info(name + Constants.TOURIST_MSG_WENT_TO_EAT);
            } else {
                RomePastaRestaurant.BARRIER.await();
                LOGGER.info(name + Constants.TOURIST_MSG_WENT_TO_EAT);
            }
            Thread.sleep((long)eatingTime * THOUSAND);
        } catch (InterruptedException | BrokenBarrierException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
        LOGGER.info(name + Constants.TOURIST_MSG_EAT_IN + eatingTime + Constants.SECONDS);
    }

    private void tunnedOnBeach(){
        Random random = new Random();
        int restTime = random.nextInt(FIVE) + 1;
        try {
            Thread.sleep((long) restTime * THOUSAND);
            LOGGER.info(name + Constants.TOURIST_MSG_TUNNED + restTime + Constants.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }


}
