package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.cities.TownAction;
import by.epamlab.inttraining.cities.TownFactory;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Phaser;

public class TravelManager {
    private static final Logger LOGGER = Logger.getLogger(TravelManager.class);
    private static final int NUMBERS_OF_TOURISTS = 40;
    private static final int COUNT_TOURS = 2;
    private static List<String> tour;
    private Bus bus;
    private Cloud cloud;
    private Phaser phaser;
    private TownAction town;
    private TownFactory client;

    static {
        tour = new ArrayList<>();
        tour.add("Default");
        tour.add("Prague");
        tour.add("Paris");
        tour.add("Rome");
        tour.add("Spain");
    }

    public TravelManager() {
        this.bus = new Bus();
        this.cloud = new Cloud();
        this.client = new TownFactory();
    }

    public Bus getBus() {
        return bus;
    }

    public Phaser getPhaser() {
        return phaser;
    }

    public TownAction getCurrentTown(){
        return town;
    }
    private void updateCurrentPosition(int currentTown){
        this.town = client.defineTown(tour.get(currentTown));
    }

    public void execute(){
        for (int i = 0; i < COUNT_TOURS; i++) {
            int currentTown = 0;
            phaser = new MyPhaser(1, tour.size());
            generatorTourists();
            while (!phaser.isTerminated()){
                updateCurrentPosition(currentTown);
                bus.visitTo(town.getTownName());
                phaser.arriveAndAwaitAdvance();
                LOGGER.info(Constants.MANAGER_MSG);
                currentTown++;
            }
            LOGGER.info(Constants.MANAGER_MSG_TOUR_COMPLETED);
            LOGGER.info(Constants.MANAGER_MSG_COUNT_PHOTOS_IN_CLOUD + cloud.get());
        }
    }

    public Cloud getCloud(){
        return cloud;
    }

    private void generatorTourists(){
        Random random = new Random();
        int numberVisitorName;
        for (int i = 0; i < NUMBERS_OF_TOURISTS; i++) {
            numberVisitorName = random.nextInt(Constants.TOURIST_NAMES.length);
            Tourist tourist = new Tourist(Constants.TOURIST_NAMES[numberVisitorName],
                    this);
            if(i%2==0){
                tourist.setQueue(true);
            }else{
                tourist.setQueue(false);
            }
            new Thread(tourist).start();
        }
    }
}
