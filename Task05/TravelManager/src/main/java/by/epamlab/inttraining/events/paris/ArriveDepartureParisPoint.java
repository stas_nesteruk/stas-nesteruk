package by.epamlab.inttraining.events.paris;

import by.epamlab.inttraining.beans.Tourist;
import by.epamlab.inttraining.events.Event;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

public class ArriveDepartureParisPoint implements Event {
    private static final Logger LOGGER = Logger.getLogger(ArriveDepartureParisPoint.class);

    @Override
    public void execute(Tourist tourist) {
        tourist.getBus().loading(tourist);
        LOGGER.info(tourist.getName() + Constants.BUS_MSG_TAKE_HIS_SEAT);
    }

    @Override
    public String getEventName() {
        return Constants.ARRIVE_DEPARTURE_PARIS_POINT;
    }
}
