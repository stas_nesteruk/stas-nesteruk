package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.ExchangeRateUpdateService;
import by.epamlab.inttraining.beans.currencies.BYN;
import by.epamlab.inttraining.beans.currencies.Currency;
import by.epamlab.inttraining.utils.Constants;
import javafx.util.Pair;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public abstract class Exchanger{
    private static final Logger LOGGER = Logger.getLogger(Exchanger.class);
    private static final String OUTPUT_SEPARATOR = "----------------------------------";
    private static final int MIN_EXCHANGE_AMOUNT_CENTS = 10000;
    private static final int MAX_EXCHANGE_AMOUNT_CENTS = 100000;
    private static final int RATE = 100;
    private final int maxSizeVisitors;

    private Queue<Visitor> visitors;
    private ExchangeRateUpdateService system;

    Exchanger(ExchangeRateUpdateService system, int capacity) {
        this.system = system;
        this.visitors = new LinkedList<>();
        this.maxSizeVisitors = capacity;
    }

    public Map<String, ExchangeRate> getCurrencies() {
        return new HashMap<>(system.getCurrencies());
    }

    public ExchangeRateUpdateService getSystem() {
        return system;
    }

    public synchronized void putVisitor(Visitor visitor) {
        while(visitors.size() >= maxSizeVisitors){
            try{
                wait();
            }catch (InterruptedException ex){
                LOGGER.error(ex.getMessage());
                Thread.currentThread().interrupt();
            }
        }
        visitors.add(visitor);
        notify();
    }

    public synchronized void exchangeMoney(){
        Map<String, ExchangeRate> currencies = getCurrencies();
        while(visitors.isEmpty()){
            try{
                wait();
            }catch (InterruptedException ex){
                LOGGER.error(ex.getMessage());
                Thread.currentThread().interrupt();
            }
        }
        queueStatus(visitors);
        Visitor visitor = visitors.poll();
        Map<Currency,Integer> moneyAndAmountToExchange = visitor.exchange();
        Currency money = (Currency) moneyAndAmountToExchange.keySet().toArray()[0];
        int amount = moneyAndAmountToExchange.get(moneyAndAmountToExchange.keySet().toArray()[0]);
        String typeCurrency = money.getTypeCurrency();
        ExchangeRate exchangeRate = currencies.get(typeCurrency);
        if(amount < MIN_EXCHANGE_AMOUNT_CENTS || amount > MAX_EXCHANGE_AMOUNT_CENTS){
            LOGGER.error(Constants.ERROR_MSG_AMOUNT_OF_MONEY);
        }else{
            BYN exchangedMoney = new BYN((amount * exchangeRate.getCurrencyBuy())/RATE);
            money.sub(amount);
            visitor.setCurrency(exchangedMoney);
            printReceipt(visitor.getVisitorName(),
                        typeCurrency,
                        amount,
                        exchangedMoney);
        }
        notify();
    }

    private synchronized void printReceipt(String visitorName, String typeCurrency, int cash, BYN exchangedCash){
        final StringBuilder sb = new StringBuilder();
        sb.append("\n").append(OUTPUT_SEPARATOR).append("\n");
        sb.append(nameExchangerToPrint());
        sb.append("\n").append(OUTPUT_SEPARATOR).append("\n");
        sb.append("Client: ").append(visitorName).append("\n");
        sb.append("Change currency: ").append(typeCurrency).append("\n");
        sb.append("Exchanged ").append(Currency.convert(cash)).append(" on ").append(exchangedCash).append("\n");
        LOGGER.info(sb);
    }

    protected abstract String nameExchangerToPrint();

    private synchronized void queueStatus(Queue<Visitor> visitors){
        final StringBuilder sb = new StringBuilder();
        sb.append("\n").append(OUTPUT_SEPARATOR).append("\n");
        sb.append(nameExchangerToPrint());
        sb.append("\n").append(OUTPUT_SEPARATOR).append("\n");
        sb.append("Visitors in line: ").append(visitors.size());
        int count = 1;
        for (Visitor visitor:visitors) {
            sb.append("\n").append(count).append(". ").append(visitor).append(";");
            count++;
        }
        sb.append("\n");
        LOGGER.info(sb);
    }

}
