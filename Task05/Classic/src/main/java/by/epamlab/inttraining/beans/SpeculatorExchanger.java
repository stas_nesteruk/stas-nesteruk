package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.ExchangeRateUpdateService;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SpeculatorExchanger extends Exchanger{
    private static final Logger LOGGER = Logger.getLogger(SpeculatorExchanger.class);
    private static final String EXCHANGER_NAME = "Speculator";
    private static final int MAX_SIZE_VISITORS = 100;
    private static final int MARGIN_PERCENT = 5;

    public SpeculatorExchanger(ExchangeRateUpdateService system){
        super(system,MAX_SIZE_VISITORS);
    }

    @Override
    protected String nameExchangerToPrint() {
        return EXCHANGER_NAME;
    }

    @Override
    public Map<String, ExchangeRate> getCurrencies(){
        int buy;
        int sale;
        int marginBuy;
        int marginSale;
        ExchangeRate exchangeRate;
        Map<String, ExchangeRate> currenciesSpeculator = new HashMap<>(getSystem().getCurrencies());
        Set<String> keys = currenciesSpeculator.keySet();
        for (String currencyKey : keys) {
            exchangeRate = currenciesSpeculator.get(currencyKey);
            buy = exchangeRate.getCurrencyBuy();
            sale = exchangeRate.getCurrencySale();
            marginBuy = buy * MARGIN_PERCENT / Constants.HUNDRED;
            marginSale = sale * MARGIN_PERCENT / Constants.HUNDRED;
            buy += marginBuy;
            sale -= marginSale;
            switch (currencyKey){
                case "USD" :
                    ExchangeRate usd = new ExchangeRate("USD", buy,sale);
                    currenciesSpeculator.replace(currencyKey, exchangeRate, usd);
                    break;
                case "EUR" :
                    ExchangeRate eur = new ExchangeRate("EUR", buy, sale);
                    currenciesSpeculator.replace(currencyKey, exchangeRate, eur);
                    break;
                default:
                    LOGGER.error(Constants.ERROR_MSG_CURRENCY_DONT_FOUND);
            }
        }
        return currenciesSpeculator;
    }
}
