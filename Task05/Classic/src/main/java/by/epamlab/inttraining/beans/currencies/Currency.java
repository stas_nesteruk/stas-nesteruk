package by.epamlab.inttraining.beans.currencies;

public abstract class Currency {
    private static final int TEN = 10;
    private static final int CURRENCY_RATE = 100;
    private int copecks;

    Currency(int value){
        this.copecks = value;
    }

    Currency(int rubs, int coins){
        this(rubs * CURRENCY_RATE + coins);
    }

    public int getCopecks() {
        return copecks;
    }

    public void add(int copecks){
        this.copecks += copecks;
    }

    public void sub(int copecks){
        this.copecks -= copecks;
    }

    @Override
    public String toString() {
        return typeCurrencyToPrint() + " " + copecks / CURRENCY_RATE + "." +
                copecks / TEN % TEN + copecks % TEN;
    }

    public static String convert(int copecks){
        return copecks / CURRENCY_RATE + "." + copecks / TEN % TEN + copecks % TEN;
    }

    protected abstract String typeCurrencyToPrint();
    public abstract String getTypeCurrency();
}
