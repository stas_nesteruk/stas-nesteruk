package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.*;
import by.epamlab.inttraining.beans.currencies.EUR;
import by.epamlab.inttraining.beans.currencies.USD;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

import java.util.Random;

public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class);
    private static final int CURRENCY_EXCHANGER_CUSTOMER_SERVICE_TIME = 3000;
    private static final int SPECULATOR_EXCHANGER_CUSTOMER_SERVICE_TIME = 2000;

    public static void main(String[] args) {
        ExchangeRateUpdateService service = new ExchangeRateUpdateService();

        Exchanger pointExchanger = new CurrencyExchanger(service);
        CustomerService currencyExchangerCustomerService = new CustomerService(pointExchanger,
                CURRENCY_EXCHANGER_CUSTOMER_SERVICE_TIME);
        Exchanger speculatorExchanger = new SpeculatorExchanger(service);
        CustomerService speculatorExchangerCustomerService = new CustomerService(speculatorExchanger,
                SPECULATOR_EXCHANGER_CUSTOMER_SERVICE_TIME);

        Exchanger[] exchangers = {pointExchanger, speculatorExchanger};

        Thread currencySystem = new Thread(service);
        Thread official = new Thread(currencyExchangerCustomerService);
        Thread speculator = new Thread(speculatorExchangerCustomerService);

        currencySystem.setDaemon(true);
        currencySystem.start();
        official.setDaemon(true);
        official.start();
        speculator.setDaemon(true);
        speculator.start();

        GeneratorVisitors generatorVisitors = new GeneratorVisitors(exchangers);
        Thread visitors = new Thread(generatorVisitors);
        visitors.start();
    }

    static class GeneratorVisitors implements Runnable{
        private Exchanger[] exchangers;
        private static final int MAX_AMOUNT_OF_MONEY = 1900;

        GeneratorVisitors(Exchanger[] exchangers){
           this.exchangers = exchangers;
        }

        @Override
        public void run() {
            Random random = new Random();
            int numberVisitorName;
            int usd;
            int eur;
            while (true){
                int period = random.nextInt(10) + 1;
                int count  = random.nextInt(5) + 5;
                for (int i = 1; i <= count; i++) {
                    numberVisitorName = random.nextInt(Constants.VISITOR_NAMES.length);
                    usd = random.nextInt(MAX_AMOUNT_OF_MONEY) + Constants.HUNDRED;
                    eur = random.nextInt(MAX_AMOUNT_OF_MONEY) + Constants.HUNDRED;
                    Visitor visitor = new Visitor(Constants.VISITOR_NAMES[numberVisitorName],
                                            exchangers,
                                            new USD(usd, 0),
                                            new EUR(eur, 0));
                    if(i % 2 == 0){
                        visitor.setQueue(true);
                    }else {
                        visitor.setQueue(false);
                    }
                    new Thread(visitor).start();
                }
                try {
                    Thread.sleep(period * Constants.THOUSAND);
                } catch (InterruptedException ex) {
                    LOGGER.error(ex.getMessage());
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
}
