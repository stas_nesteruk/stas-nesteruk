package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.Exchanger;
import org.apache.log4j.Logger;

public class CustomerService implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(CustomerService.class);
    private int customerServiceTime;
    private Exchanger exchanger;

    CustomerService(Exchanger exchanger, int customerSeriviceTime) {
        this.exchanger = exchanger;
        this.customerServiceTime = customerSeriviceTime;
    }

    @Override
    public void run() {
        while (true){
            exchanger.exchangeMoney();
            try {
                Thread.sleep(customerServiceTime);
            } catch (InterruptedException ex) {
                LOGGER.error(ex.getMessage());
                Thread.currentThread().interrupt();
            }
        }
    }
}
