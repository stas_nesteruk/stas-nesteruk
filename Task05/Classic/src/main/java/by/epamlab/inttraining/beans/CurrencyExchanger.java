package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.ExchangeRateUpdateService;

public class CurrencyExchanger extends Exchanger{
    private static final int MAX_SIZE_VISITORS = 10;
    private static final String EXCHANGER_NAME = "Official currency exchanger";

    public CurrencyExchanger(ExchangeRateUpdateService system){
        super(system, MAX_SIZE_VISITORS);
    }

    @Override
    protected String nameExchangerToPrint() {
        return EXCHANGER_NAME;
    }
}
