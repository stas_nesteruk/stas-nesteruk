package by.epamlab.inttraining.app.beans;

import by.epamlab.inttraining.app.utils.Constants;
import org.apache.log4j.Logger;

public class Stock {
    private static final Logger LOGGER = Logger.getLogger(Stock.class);
    private boolean workingStatus;

    public Stock(){
        this.on();
    }
    public boolean isWorkingStatus() {
        return workingStatus;
    }

    public void setWorkingStatus(boolean workingStatus) {
        this.workingStatus = workingStatus;
    }

    public boolean searching(Mobile mobilePhone){
        LOGGER.info("Search in stock");
        for(Mobile mobile : Constants.mobileList){
            if(mobilePhone.equals(mobile)){
                LOGGER.info(mobilePhone.getName() + " is in stock!");
                return true;
            }
        }
        LOGGER.info(mobilePhone.getName() + " is't in stock!");
        return false;
    }

    public void on(){
        LOGGER.info("The stock is working.");
        workingStatus = true;
    }

    public void off(){
        LOGGER.info("The stock is't working.");
        workingStatus = false;
    }
}
