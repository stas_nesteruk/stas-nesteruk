package by.epamlab.inttraining.handlers;

import by.epamlab.inttraining.app.MobileOrder;
import org.apache.log4j.Logger;

public class IsHaveWrapHandler implements Handler{
    private static final Logger LOGGER = Logger.getLogger(IsHaveWrapHandler.class);
    private Handler nextHandler;

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public void handlerRequest(MobileOrder mobileOrder) {
        if(mobileOrder.getWrap().isHavePacking()){
            LOGGER.info("Wrap handler: Packing in stock");
            mobileOrder.setHaveWrap(true);
        }else{
            LOGGER.info("Wrap handler: Packing out of stock");
            mobileOrder.setHaveWrap(false);
        }
        if(nextHandler != null){
            nextHandler.handlerRequest(mobileOrder);
        }
    }
}
