package by.epamlab.inttraining.app.beans;

import org.apache.log4j.Logger;

public class Delivery {
    private static final Logger LOGGER = Logger.getLogger(Delivery.class);
    private boolean isWorking;

    public Delivery(){
        isWorking = true;
    }
    public void deliveringToTheStore(){
        if(isWorking){
            LOGGER.info("Mobile delivered to the store..");
        }else{
            LOGGER.info("Delivery does not work.");
        }

    }

    public void deliveringToTheBuyer(){
        if(isWorking){
            LOGGER.info("Mobile delivered to the buyer..");
        }else{
            LOGGER.info("Delivery does not work.");
        }
    }

    public boolean isWorking() {
        return isWorking;
    }
}
