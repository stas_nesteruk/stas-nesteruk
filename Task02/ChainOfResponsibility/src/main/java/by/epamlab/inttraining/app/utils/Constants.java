package by.epamlab.inttraining.app.utils;

import by.epamlab.inttraining.app.beans.Mobile;

import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static List<Mobile> mobileList = new ArrayList<>();

    private static final Mobile SAMSUNG_S8 = new Mobile("Samsung S8", 700);
    private static final Mobile HONOR_8X = new Mobile("Honor 8X", 549);
    private static final Mobile SUMSUNG_GALAXY_A50 = new Mobile("Samsung Galaxy A50", 554);
    private static final Mobile XIAOMI_REDMI_NOTE_7 = new Mobile("Xiaomi Redmi Note 7", 475);
    private static final Mobile HUAWEI_P30_LITE = new Mobile("Huawei P30 Lite", 600);

    static {
        mobileList.add(SAMSUNG_S8);
        mobileList.add(HONOR_8X);
        mobileList.add(SUMSUNG_GALAXY_A50);
        mobileList.add(XIAOMI_REDMI_NOTE_7);
        mobileList.add(HUAWEI_P30_LITE);
    }
}
