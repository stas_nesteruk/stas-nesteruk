package by.epamlab.inttraining.handlers;

import by.epamlab.inttraining.app.MobileOrder;
import by.epamlab.inttraining.app.beans.Mobile;
import org.apache.log4j.Logger;

public class IsFoundMobileHandler implements Handler{
    private static final Logger LOGGER = Logger.getLogger(IsFoundMobileHandler.class);
    private Handler nextHandler;

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public void handlerRequest(MobileOrder mobileOrder) {
        Mobile mobile = mobileOrder.getMobile();
        if(mobileOrder.getStock().searching(mobile)){
            LOGGER.info("Search mobile handler: mobile is found");
            mobileOrder.setFoundMobile(true);
        }else{
            LOGGER.info("Search mobile handler: mobile is't found");
            mobileOrder.setFoundMobile(false);
        }
        if(nextHandler != null){
            nextHandler.handlerRequest(mobileOrder);
        }
    }
}
