package by.epamlab.inttraining.app.beans;

import org.apache.log4j.Logger;

public class Wrap {
    private static final Logger LOGGER = Logger.getLogger(Wrap.class);
    private boolean isHavePacking;

    public Wrap(){
        isHavePacking = true;
    }

    public void wraping(){
        if(isHavePacking){
            LOGGER.info("Mobile is packing...");
        }else{
            LOGGER.info("Packing out of stock");
        }
    }

    public boolean isHavePacking() {
        return isHavePacking;
    }
}
