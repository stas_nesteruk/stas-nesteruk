package by.epamlab.inttraining.handlers;

import by.epamlab.inttraining.app.MobileOrder;
import org.apache.log4j.Logger;

public class IsWorkingDeliveryHandler implements Handler{
    private static final Logger LOGGER = Logger.getLogger(IsWorkingDeliveryHandler.class);
    private Handler nextHandler;

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public void handlerRequest(MobileOrder mobileOrder) {
        if(mobileOrder.getDelivery().isWorking()){
            LOGGER.info("Check delivery status: delivery is working");
            mobileOrder.setWorkingDelivery(true);
        }else{
            LOGGER.info("Check delivery status: delivery is't working");
            mobileOrder.setWorkingDelivery(false);
        }
        if(nextHandler != null){
            nextHandler.handlerRequest(mobileOrder);
        }
    }
}
