package by.epamlab.inttraining.handlers;

import by.epamlab.inttraining.app.MobileOrder;

public interface Handler {
    void setNextHandler(Handler nextHandler);
    void handlerRequest(MobileOrder mobileOrder);
}
