package by.epamlab.inttraining;

import by.epamlab.inttraining.app.MobileOrder;
import by.epamlab.inttraining.app.utils.Constants;
import by.epamlab.inttraining.handlers.*;

public class Runner {
    public static void main(String[] args) {

        Handler isWorkingStatusHandler = new IsWorkingStockHandler();
        Handler isHaveWrapHandler = new IsHaveWrapHandler();
        Handler isFoundMobileHandler = new IsFoundMobileHandler();
        Handler isWorkingDeliveryHandler = new IsWorkingDeliveryHandler();

        isWorkingStatusHandler.setNextHandler(isHaveWrapHandler);
        isHaveWrapHandler.setNextHandler(isFoundMobileHandler);
        isFoundMobileHandler.setNextHandler(isWorkingDeliveryHandler);

        MobileOrder clientOrder = new MobileOrder(Constants.mobileList.get(2));
        isWorkingStatusHandler.handlerRequest(clientOrder);
        clientOrder.order();
    }
}
