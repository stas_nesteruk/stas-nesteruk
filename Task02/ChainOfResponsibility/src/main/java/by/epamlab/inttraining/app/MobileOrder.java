package by.epamlab.inttraining.app;

import by.epamlab.inttraining.app.beans.*;
import org.apache.log4j.Logger;

public class MobileOrder {
    private static final Logger LOGGER = Logger.getLogger(MobileOrder.class);

    private Stock stock;
    private Wrap wrap;
    private Delivery delivery;
    private Payment payment;
    private Mobile mobile;

    private boolean isFoundMobile;
    private boolean isWorkingStock;
    private boolean isHaveWrap;
    private boolean isWorkingDelivery;

    public MobileOrder(Mobile mobile) {
        stock = new Stock();
        wrap = new Wrap();
        delivery = new Delivery();
        payment = new Payment();
        this.mobile = mobile;
    }

    public void order(){
        if(isWorkingStock && isFoundMobile && isHaveWrap && isWorkingDelivery) {
            wrap.wraping();
            delivery.deliveringToTheStore();
            payment.processPayment();
            delivery.deliveringToTheBuyer();
        }else{
            LOGGER.info("Mobile order: denied.");
        }
    }

    public Mobile getMobile() {
        return mobile;
    }

    public Stock getStock() {
        return stock;
    }

    public Wrap getWrap() {
        return wrap;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public Payment getPayment() {
        return payment;
    }

    public boolean isFoundMobile() {
        return isFoundMobile;
    }

    public void setFoundMobile(boolean foundMobile) {
        isFoundMobile = foundMobile;
    }

    public boolean isWorkingStock() {
        return isWorkingStock;
    }

    public void setWorkingStock(boolean workingStock) {
        isWorkingStock = workingStock;
    }

    public boolean isHaveWrap() {
        return isHaveWrap;
    }

    public void setHaveWrap(boolean haveWrap) {
        isHaveWrap = haveWrap;
    }

    public boolean isWorkingDelivery() {
        return isWorkingDelivery;
    }

    public void setWorkingDelivery(boolean workingDelivery) {
        isWorkingDelivery = workingDelivery;
    }
}
