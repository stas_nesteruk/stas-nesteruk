package by.epamlab.inttraining.handlers;

import by.epamlab.inttraining.app.MobileOrder;
import org.apache.log4j.Logger;

public class IsWorkingStockHandler implements Handler{
    private static final Logger LOGGER = Logger.getLogger(IsWorkingStockHandler.class);
    private Handler nextHandler;

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public void handlerRequest(MobileOrder mobileOrder) {
        if(mobileOrder.getStock().isWorkingStatus()){
            LOGGER.info("Check working status: stock is working");
            mobileOrder.setWorkingStock(true);
        }else{
            LOGGER.info("Check working status: stock is't working");
            mobileOrder.setWorkingStock(false);
        }
        if(nextHandler != null){
            nextHandler.handlerRequest(mobileOrder);
        }
    }
}
