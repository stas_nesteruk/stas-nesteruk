package by.epamlab.inttraining.interfaces;

public interface Order {
    int getPrice();
    String getDescription();
}
