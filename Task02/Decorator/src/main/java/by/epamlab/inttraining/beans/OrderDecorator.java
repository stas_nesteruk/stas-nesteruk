package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.interfaces.Order;

public abstract class OrderDecorator implements Order {
    protected final Order order;

    public OrderDecorator(Order order) {
        this.order = order;
    }

    public int getPrice() {
        return order.getPrice();
    }

    public String getDescription() {
        return order.getDescription();
    }
}
