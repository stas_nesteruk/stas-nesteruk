package by.epamlab.inttraining;

import by.epamlab.inttraining.beans.Tea;
import by.epamlab.inttraining.beans.filling.Lemon;
import by.epamlab.inttraining.beans.filling.Sugar;
import by.epamlab.inttraining.interfaces.Order;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        Order order = new Tea(Constants.GREENFIELD_GREEN);
        LOGGER.info(String.format(Constants.ORDER_DESCRIPTIONS, order.getDescription(),order.getPrice()));
        order = new Sugar(order);
        LOGGER.info(String.format(Constants.ORDER_DESCRIPTIONS, order.getDescription(),order.getPrice()));
        order = new Lemon(order);
        LOGGER.info(String.format(Constants.ORDER_DESCRIPTIONS, order.getDescription(),order.getPrice()));


    }
}
