package by.epamlab.inttraining.beans.filling;

import by.epamlab.inttraining.beans.OrderDecorator;
import by.epamlab.inttraining.interfaces.Order;
import by.epamlab.inttraining.utils.Constants;

public class Milk extends OrderDecorator {
    public Milk(Order order) {
        super(order);
    }

    @Override
    public int getPrice() {
        return super.getPrice() + Constants.MILK_PRICE;
    }

    @Override
    public String getDescription() {
        return super.getDescription() + Constants.MILK_DESCRIPTION;
    }
}
