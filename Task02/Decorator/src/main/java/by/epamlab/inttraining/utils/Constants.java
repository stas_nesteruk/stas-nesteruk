package by.epamlab.inttraining.utils;

import by.epamlab.inttraining.beans.Tea;

public class Constants {
    public static final int SUGAR_PRICE = 100;
    public static final String SUGAR_DESCRIPTION = " with sugar";

    public static final int LEMON_PRICE = 100;
    public static final String LEMON_DESCRIPTION = " with lemon";

    public static final int CINNAMON_PRICE = 200;
    public static final String CINNAMON_DESCRIPTION = " with cinnamon";

    public static final int MILK_PRICE = 150;
    public static final String MILK_DESCRIPTION = " with milk";

    public static final Tea GREENFIELD_GREEN = new Tea("Greenfield", 150, "Green tea");
    public static final Tea GREENFIELD_BLAСK = new Tea("Greenfield", 150, "Black tea");
    public static final Tea GREENFIELD_MELISSA = new Tea("Greenfield", 150, "Melissa");
    public static final Tea TESS = new Tea("Tess", 100, "Green tea");
    public static final Tea CURTIS = new Tea("Curtis", 100, "Green tea");
    public static final Tea AKBAR = new Tea("Akbar", 100, "Black tea");

    public static final String ORDER_DESCRIPTIONS = "Descriptions: %s; Price %d;";



}
