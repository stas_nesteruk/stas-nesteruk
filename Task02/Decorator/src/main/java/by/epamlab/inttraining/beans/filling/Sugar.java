package by.epamlab.inttraining.beans.filling;

import by.epamlab.inttraining.beans.OrderDecorator;
import by.epamlab.inttraining.interfaces.Order;
import by.epamlab.inttraining.utils.Constants;

public class Sugar extends OrderDecorator {

    public Sugar(Order order) {
        super(order);
    }

    @Override
    public int getPrice() {
        return super.getPrice() + Constants.SUGAR_PRICE;
    }

    @Override
    public String getDescription() {
        return super.getDescription() + Constants.SUGAR_DESCRIPTION;
    }
}
