package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.interfaces.Order;

public class Coffee implements Order {

    private String name;
    private int price;
    private String description;

    public Coffee(String name, int price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public Coffee(Coffee coffee){
        this.name = coffee.getName();
        this.price = coffee.getPrice();
        this.description = coffee.getDescription();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }
}
