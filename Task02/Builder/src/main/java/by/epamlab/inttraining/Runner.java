package by.epamlab.inttraining;

import by.epamlab.inttraining.app.beans.Account;
import by.epamlab.inttraining.app.beans.AccountBuilder;
import by.epamlab.inttraining.app.beans.Role;
import org.apache.log4j.Logger;


public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        Account studentAccount = new AccountBuilder()
                                .buildRole(Role.STUDENT)
                                .buildName("Ivan Ivanov")
                                .buildPassword("123sda32")
                                .buildDateOfBirth("19.05.1990")
                                .buildDateCreated("08.07.2019")
                                .build();
        LOGGER.info(studentAccount);
        Account teacherAccount = new AccountBuilder()
                                .buildRole(Role.TEACHER)
                                .buildName("Oleg Petrov")
                                .buildPassword("professor")
                                .buildDateOfBirth("23.08.1976")
                                .buildAddress("Victory avenue, 13/15")
                                .buildMobileNumber("8-033-654-23-48")
                                .buildPosition("Associate Professor, Candidate of Technical Sciences")
                                .buildEmail("oleg.petrov@gmail.com")
                                .buildDateCreated("10.09.2005")
                                .build();
        LOGGER.info(teacherAccount);
    }
}
