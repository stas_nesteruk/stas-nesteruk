package by.epamlab.inttraining.app.beans;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Account {
    private Role role;
    private String name;
    private String password;
    private Date dateOfBirth;
    private String address;
    private String mobileNumber;
    private String position;
    private String email;
    private Date dateCreated;

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    public void setRole(Role role) {
        this.role = role;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Role: ").append(role).append(";\n");
        sb.append("Name: ").append(name).append(";\n");
        sb.append("Password: ").append(password).append(";\n");
        sb.append("Date of birth: ").append(DATE_FORMAT.format(dateOfBirth)).append(";\n");
        sb.append("Address: ").append(address).append(";\n");
        sb.append("Mobile number: ").append(mobileNumber).append(";\n");
        sb.append("Position: ").append(position).append(";\n");
        sb.append("Email: ").append(email).append(";\n");
        sb.append("Date created: ").append(DATE_FORMAT.format(dateCreated)).append(".\n");
        return sb.toString();
    }
}
