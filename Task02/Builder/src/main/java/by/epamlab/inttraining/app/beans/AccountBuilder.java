package by.epamlab.inttraining.app.beans;

import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AccountBuilder {
    private static final Logger LOGGER = Logger.getLogger(AccountBuilder.class);
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    private Role role = Role.VISITOR;
    private String name = "";
    private String pass = "";
    private static Date dateOfBirth;
    private String address = "";
    private String mobileNumber = "";
    private String position = "";
    private String email = "";
    private Date dateCreated = new Date();


    static{
        try{
            dateOfBirth = DATE_FORMAT.parse("01.01.1960");
        }catch (ParseException ex){
            LOGGER.error("Date parse exeption: " + ex);
        }
    }

    public AccountBuilder buildRole(Role role) {
        this.role = role;
        return this;
    }

    public AccountBuilder buildName(String name) {
        this.name = name;
        return this;
    }

    public AccountBuilder buildPassword(String pass) {
        this.pass = pass;
        return this;
    }

    public AccountBuilder buildDateOfBirth(String dateOfBirth) {
        try{
            this.dateOfBirth = DATE_FORMAT.parse(dateOfBirth);
        }catch (ParseException ex){
            LOGGER.error("Date parse exeption: " + ex);
        }
        return this;
    }

    public AccountBuilder buildAddress(String address) {
        this.address = address;
        return this;
    }

    public AccountBuilder buildMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
        return this;
    }

    public AccountBuilder buildPosition(String position) {
        this.position = position;
        return this;
    }

    public AccountBuilder buildEmail(String email) {
        this.email = email;
        return this;
    }

    public AccountBuilder buildDateCreated(String dateCreated) {
        try{
            this.dateCreated = DATE_FORMAT.parse(dateCreated);
        }catch (ParseException ex){
            LOGGER.error("Date parse exeption: " + ex);
        }
        return this;
    }

    public Account build(){
        Account account = new Account();
        account.setRole(role);
        account.setName(name);
        account.setPassword(pass);
        account.setDateOfBirth(dateOfBirth);
        account.setAddress(address);
        account.setMobileNumber(mobileNumber);
        account.setPosition(position);
        account.setEmail(email);
        account.setDateCreated(dateCreated);
        return account;
    }


}
