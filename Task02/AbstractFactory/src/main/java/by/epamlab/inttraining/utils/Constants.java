package by.epamlab.inttraining.utils;

import by.epamlab.inttraining.beans.Material;

import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static final Material PLASTIC = new Material("Plastic");
    public static final Material WOOD = new Material("Wood");
    public static final Material SPECIAL = new Material("Special");


    public static final List<String> FIRST_SECTION_MENU = new ArrayList<String>();
    public static final List<String> SECOND_SECTION_MENU = new ArrayList<String>();

    static {
        FIRST_SECTION_MENU.add("Select window model:");
        FIRST_SECTION_MENU.add("\n1.Plastic");
        FIRST_SECTION_MENU.add("\n2.Wood");
        FIRST_SECTION_MENU.add("\n3.Special");

        SECOND_SECTION_MENU.add("Choose a manufacturing factory:");
        SECOND_SECTION_MENU.add("\n1.Gomel");
        SECOND_SECTION_MENU.add("\n2.Minsk");
    }
}
