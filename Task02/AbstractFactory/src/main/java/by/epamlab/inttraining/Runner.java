package by.epamlab.inttraining;

import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class);
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        LOGGER.info("Welcome to the window store!!!");
        int windowModel = selectSectionMenu(Constants.FIRST_SECTION_MENU);
        int manufacturingFactory = selectSectionMenu(Constants.SECOND_SECTION_MENU);
        Client.execute(manufacturingFactory, windowModel);
        LOGGER.info("Thank you for your purchase!");
    }

    private static int selectSectionMenu(List<String> menuDescription){
        LOGGER.info(menuDescription);
        return insertData(menuDescription.size() - 1);
    }

    private static int insertData(int maxValue){
        int insertData;
        while(true){
            try{
                insertData = scanner.nextInt();
                if(insertData < 1 || insertData > maxValue){
                    LOGGER.info("Select an option from the list!\n");
                    continue;
                }else{
                    break;
                }
            }catch (InputMismatchException ex){
                LOGGER.error("Try again!");
                scanner.nextLine();
            }
        }
        return insertData;
    }
}
