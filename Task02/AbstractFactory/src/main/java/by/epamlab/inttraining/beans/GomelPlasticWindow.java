package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.interfaces.PlasticWindow;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

public class GomelPlasticWindow extends Window implements PlasticWindow {
    private static final Logger LOGGER = Logger.getLogger(GomelPlasticWindow.class);

    public GomelPlasticWindow(){
        super(Constants.PLASTIC);
    }

    public void create() {
        LOGGER.info("Gomel: " + super.getMaterial().getName() + " window is created and send..");
    }
}
