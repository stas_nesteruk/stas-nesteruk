package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.interfaces.PlasticWindow;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

public class MinskPlasticWindow extends Window implements PlasticWindow {
    private static final Logger LOGGER = Logger.getLogger(MinskPlasticWindow.class);

    public MinskPlasticWindow() {
        super(Constants.PLASTIC);
    }

    public void create() {
        LOGGER.info("Minsk: " + super.getMaterial().getName() + " window is created and send..");
    }

}
