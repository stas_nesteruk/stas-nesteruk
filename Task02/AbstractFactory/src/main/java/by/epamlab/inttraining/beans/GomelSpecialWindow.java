package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.interfaces.SpecialWindow;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

public class GomelSpecialWindow extends Window implements SpecialWindow {
    private static final Logger LOGGER = Logger.getLogger(GomelSpecialWindow.class);

    public GomelSpecialWindow(){
        super(Constants.SPECIAL);
    }

    public void create() {
        LOGGER.info("Gomel: " + super.getMaterial().getName() + " window is created and send..");
    }
}
