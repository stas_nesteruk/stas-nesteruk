package by.epamlab.inttraining.factories;

import by.epamlab.inttraining.beans.MinskPlasticWindow;
import by.epamlab.inttraining.beans.MinskSpecialWindow;
import by.epamlab.inttraining.beans.MinskWoodenWindow;
import by.epamlab.inttraining.interfaces.PlasticWindow;
import by.epamlab.inttraining.interfaces.SpecialWindow;
import by.epamlab.inttraining.interfaces.WoodenWindow;

public class MinskWindowFactory extends WindowFactory {
    public PlasticWindow createPlasticWindow(){
        return new MinskPlasticWindow();
    }

    public SpecialWindow createSpecialWindow() {
        return new MinskSpecialWindow();
    }

    public WoodenWindow createWoodenWindow() {
        return new MinskWoodenWindow();
    }
}
