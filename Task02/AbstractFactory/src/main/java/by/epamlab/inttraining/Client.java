package by.epamlab.inttraining;


import by.epamlab.inttraining.factories.GomelWindowFactory;
import by.epamlab.inttraining.factories.MinskWindowFactory;
import by.epamlab.inttraining.factories.WindowFactory;
import by.epamlab.inttraining.interfaces.PlasticWindow;
import by.epamlab.inttraining.interfaces.SpecialWindow;
import by.epamlab.inttraining.interfaces.WoodenWindow;
import org.apache.log4j.Logger;

public class Client {
    private static final Logger LOGGER = Logger.getLogger(Client.class);

    private static final GomelWindowFactory GOMEL_WINDOW_FACTORY = new GomelWindowFactory();
    private static final MinskWindowFactory MINSK_WINDOW_FACTORY = new MinskWindowFactory();

    public static void execute(int factory, int windowModel){
        if(factory == 1){
            createWindow(GOMEL_WINDOW_FACTORY, windowModel);
        }else{
            createWindow(MINSK_WINDOW_FACTORY, windowModel);
        }
    }

    private static void createWindow(WindowFactory factory, int windowModel) {
        switch (windowModel){
            case 1:
                PlasticWindow plasticWindow = factory.createPlasticWindow();
                plasticWindow.create();
                break;
            case 2:
                WoodenWindow woodenWindow = factory.createWoodenWindow();
                woodenWindow.create();
                break;
            case 3:
                SpecialWindow specialWindow = factory.createSpecialWindow();
                specialWindow.create();
                break;
            default:
                LOGGER.error("Wrong action!!!");
        }
    }

}
