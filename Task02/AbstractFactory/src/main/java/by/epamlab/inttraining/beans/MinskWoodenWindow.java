package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.interfaces.WoodenWindow;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

public class MinskWoodenWindow extends Window implements WoodenWindow {
    private static final Logger LOGGER = Logger.getLogger(MinskWoodenWindow.class);

    public MinskWoodenWindow() {
        super(Constants.WOOD);
    }

    public void create() {
        LOGGER.info("Minsk: " + super.getMaterial().getName() + " window is created and send..");
    }
}
