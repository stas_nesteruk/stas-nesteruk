package by.epamlab.inttraining.factories;

import by.epamlab.inttraining.beans.GomelPlasticWindow;
import by.epamlab.inttraining.beans.GomelSpecialWindow;
import by.epamlab.inttraining.beans.GomelWoodenWindow;
import by.epamlab.inttraining.interfaces.PlasticWindow;
import by.epamlab.inttraining.interfaces.SpecialWindow;
import by.epamlab.inttraining.interfaces.WoodenWindow;

public class GomelWindowFactory extends WindowFactory {

    public PlasticWindow createPlasticWindow(){
        return new GomelPlasticWindow();
    }

    public SpecialWindow createSpecialWindow() {
        return new GomelSpecialWindow();
    }

    public WoodenWindow createWoodenWindow() {
        return new GomelWoodenWindow();
    }
}
