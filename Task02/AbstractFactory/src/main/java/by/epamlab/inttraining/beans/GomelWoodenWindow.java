package by.epamlab.inttraining.beans;

import by.epamlab.inttraining.interfaces.WoodenWindow;
import by.epamlab.inttraining.utils.Constants;
import org.apache.log4j.Logger;

public class GomelWoodenWindow extends Window implements WoodenWindow {
    private static final Logger LOGGER = Logger.getLogger(GomelWoodenWindow.class);

    public GomelWoodenWindow(){
        super(Constants.WOOD);
    }

    public void create() {
        LOGGER.info("Gomel: " + super.getMaterial().getName() + " window is created and send..");
    }
}
