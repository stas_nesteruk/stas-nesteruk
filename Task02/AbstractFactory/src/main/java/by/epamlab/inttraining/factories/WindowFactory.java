package by.epamlab.inttraining.factories;

import by.epamlab.inttraining.interfaces.PlasticWindow;
import by.epamlab.inttraining.interfaces.SpecialWindow;
import by.epamlab.inttraining.interfaces.WoodenWindow;

public abstract class WindowFactory {
    public abstract PlasticWindow createPlasticWindow();
    public abstract SpecialWindow createSpecialWindow();
    public abstract WoodenWindow createWoodenWindow();
}
