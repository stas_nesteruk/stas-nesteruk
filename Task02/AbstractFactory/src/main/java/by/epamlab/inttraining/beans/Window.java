package by.epamlab.inttraining.beans;

public abstract class Window {
    private Material material;

    public Window(Material material) {
        this.material = material;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }
}
