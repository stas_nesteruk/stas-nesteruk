package by.epamlab.inttraining.app.beans;

public class Originator {
    private BankAccount account;

    public void setData(BankAccount account) {
        this.account = account;
    }

    public Memento backup(){
        return new Memento(account);
    }

    public void restore(Memento account){
        this.account.setAmountOfMoney(account.getAmountOfMoney());
    }
}
