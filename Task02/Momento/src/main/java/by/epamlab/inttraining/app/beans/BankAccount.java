package by.epamlab.inttraining.app.beans;

import by.epamlab.inttraining.app.exceptions.BankAccountException;
import by.epamlab.inttraining.app.exceptions.TransactionException;
import org.apache.log4j.Logger;

public class BankAccount {
    private static final Logger LOGGER = Logger.getLogger(BankAccount.class);
    private final static int LOWER_ACCOUN_NUMBER_LEMIT = 111111111;
    private final static int UPPER_ACCOUN_NUMBER_LEMIT = 999999999;
    private final int accountNumber;
    private int amountOfMoney;

    public BankAccount(int accountNumber, int amountOfMoney) {
        this.accountNumber = accountNumber;
        this.amountOfMoney = amountOfMoney;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(int amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public void makeTransaction(Transaction transaction) throws BankAccountException, TransactionException {

        this.amountOfMoney -= transaction.getAmountOfMoney();
        LOGGER.info(paymentMessage(transaction));

        if(transaction.getReceiverAccountNumber() < LOWER_ACCOUN_NUMBER_LEMIT
                || transaction.getReceiverAccountNumber()>UPPER_ACCOUN_NUMBER_LEMIT)
            throw new BankAccountException("Invalid receiver account number!");
        if(amountOfMoney < transaction.getAmountOfMoney())
            throw new TransactionException("Insufficient funds in the account");
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("\nAccount number: ").append(accountNumber).append("\n");
        sb.append("Account balance: ").append(amountOfMoney).append("\n");
        return sb.toString();
    }

    private String paymentMessage(Transaction transaction){
        final StringBuilder sb = new StringBuilder();
        sb.append("\nFrom your account was made a payment to the number ");
        sb.append(transaction.getReceiverAccountNumber()).append(" ");
        sb.append("in the amount of ").append(transaction.getAmountOfMoney()).append(".\n");
        sb.append("Account balance: ").append(amountOfMoney);
        return sb.toString();
    }
}
