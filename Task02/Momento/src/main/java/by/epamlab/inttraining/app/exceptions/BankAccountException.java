package by.epamlab.inttraining.app.exceptions;

public class BankAccountException extends Exception{
    public BankAccountException(){
        super();
    }

    public BankAccountException(String message){
        super(message);
    }
}
