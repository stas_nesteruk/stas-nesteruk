package by.epamlab.inttraining;

import by.epamlab.inttraining.app.beans.*;
import by.epamlab.inttraining.app.exceptions.BankAccountException;
import by.epamlab.inttraining.app.exceptions.TransactionException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


public class Runner {
    private static final Logger LOGGER = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount(888999888, 100000);
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(new Transaction(123323321,
                                            25000));
        transactions.add(new Transaction(32457,
                                            10000));
        transactions.add(new Transaction(432244212,
                                             83021));
        Originator originator = new Originator();
        Memento backup;
        CareTaker storage = new CareTaker();

        for (Transaction transaction: transactions) {
            try{
                LOGGER.info(bankAccount.toString());
                originator.setData(bankAccount);
                backup = originator.backup();
                storage.setMomento(backup);
                bankAccount.makeTransaction(transaction);
            } catch (BankAccountException | TransactionException ex) {
                LOGGER.error(ex.getMessage());
                backup = storage.getMomento();
                originator.restore(backup);
                LOGGER.info(bankAccount.toString());
            }
        }
    }

}
