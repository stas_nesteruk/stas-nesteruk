package by.epamlab.inttraining.app.beans;

public class Transaction {
    private int receiverAccountNumber;
    private int amountOfMoney;

    public Transaction(int receiverAccountNumber, int amountOfMoney) {
        this.receiverAccountNumber = receiverAccountNumber;
        this.amountOfMoney = amountOfMoney;
    }

    public int getReceiverAccountNumber() {
        return receiverAccountNumber;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }
}
