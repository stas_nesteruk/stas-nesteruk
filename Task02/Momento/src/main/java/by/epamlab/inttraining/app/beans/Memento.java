package by.epamlab.inttraining.app.beans;

public class Memento {
    private final int accountNumber;
    private final int amountOfMoney;

    public Memento(BankAccount account) {
        this.accountNumber = account.getAccountNumber();
        this.amountOfMoney = account.getAmountOfMoney();
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }
}
