package by.epamlab.inttraining.app.beans;

import org.apache.log4j.Logger;

public class TicketOrder extends Order{
    private static final Logger LOGGER = Logger.getLogger(TicketOrder.class);
    private boolean validate = false;
    public TicketOrder(String passengerName, int numberOfSeats) {
        super(passengerName, numberOfSeats);
    }

    public int getPrice() {
        if(getNumberOfSeats() > 0){
            return getNumberOfSeats() * super.getPrice();
        }else {
            LOGGER.error("Wrong count of seats!");
            return 0;
        }
    }

    public void validate() {
        int aviableSeats = getAviableSeats();
        int numberOfSeats = getNumberOfSeats();
        if((aviableSeats > numberOfSeats) && (numberOfSeats > 0)){
            this.validate = true;
            aviableSeats = aviableSeats - 1;
            super.setAviableSeats(aviableSeats);
        }else{
            this.validate = false;
        }
    }

    public void bookTicket() {
        if(validate){
            LOGGER.info(getPassengerName() + ", " + getNumberOfSeats() + " seats booked successfully!");
        }else{
            LOGGER.info("Seats not available!");
        }

    }
}
