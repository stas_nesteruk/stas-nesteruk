package by.epamlab.inttraining.app.beans;


public class Order {
    private static final int AVIABLE_SEATS = 30;
    private static final int PRICE = 500;

    private int aviableSeats = AVIABLE_SEATS;

    private String passengerName;
    private int numberOfSeats;

    public Order(String passengerName, int numberOfSeats) {
        this.passengerName = passengerName;
        this.numberOfSeats = numberOfSeats;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public void setAviableSeats(int aviableSeats) {
        this.aviableSeats = aviableSeats;
    }

    public int getAviableSeats() {
        return aviableSeats;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public int getPrice() {
        return PRICE;
    }
}
