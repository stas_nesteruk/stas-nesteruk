package by.epamlab.inttraining.app;

public interface BookingSystem {
    int calculate();
    void validateTicket();
    void book();
}
