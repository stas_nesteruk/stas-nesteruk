package by.epamlab.inttraining.app;

import by.epamlab.inttraining.app.beans.TicketOrder;

public class TicketOrderAdapter extends TicketOrder implements BookingSystem{

    public TicketOrderAdapter(String passengerName, int numberOfSeats) {
        super(passengerName, numberOfSeats);
    }

    public int calculate(){
        return getPrice();
    }

    public void validateTicket(){
        validate();
    }

    public void book(){
        bookTicket();
    }
}
