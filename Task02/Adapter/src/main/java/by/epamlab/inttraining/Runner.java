package by.epamlab.inttraining;

import by.epamlab.inttraining.app.TicketOrderAdapter;
import org.apache.log4j.Logger;

public class Runner {
    public static final Logger LOGGER = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        TicketOrderAdapter ticketOrder = new TicketOrderAdapter("Ivy Terner", 2);
        ticketOrder.validateTicket();
        ticketOrder.book();
        LOGGER.info("Price: " + ticketOrder.calculate());
    }
}
