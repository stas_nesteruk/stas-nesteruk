package by.epamlab.inttraining.app.beans;

import org.apache.log4j.Logger;

public class Delivery {
    private static final Logger LOGGER = Logger.getLogger(Delivery.class);

    public void deliveryToTheStore(){
        LOGGER.info("Mobile delivered to the store..");
    }

    public void deliveryToTheBuyer(){
        LOGGER.info("Mobile delivered to the buyer..");
    }
}
