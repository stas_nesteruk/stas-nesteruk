package by.epamlab.inttraining.app.beans;

import org.apache.log4j.Logger;

public class Payment {
    private static final Logger LOGGER = Logger.getLogger(Payment.class);

    public void processPayment(){
        LOGGER.info("Payment is confirmed...");
    }
}
