package by.epamlab.inttraining.app.beans;

import org.apache.log4j.Logger;

public class Stock {
    private static final Logger LOGGER = Logger.getLogger(Stock.class);

    public void searching(){
        LOGGER.info("Search in stock");
    }
}
