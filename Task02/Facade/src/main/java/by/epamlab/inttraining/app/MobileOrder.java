package by.epamlab.inttraining.app;

import by.epamlab.inttraining.app.beans.Delivery;
import by.epamlab.inttraining.app.beans.Payment;
import by.epamlab.inttraining.app.beans.Stock;
import by.epamlab.inttraining.app.beans.Wrap;

public class MobileOrder {
    Stock stock = new Stock();
    Wrap wrap = new Wrap();
    Delivery delivery = new Delivery();
    Payment payment = new Payment();

    public void order(){
        stock.searching();
        wrap.wraping();
        delivery.deliveryToTheStore();
        payment.processPayment();
        delivery.deliveryToTheBuyer();
    }
}
