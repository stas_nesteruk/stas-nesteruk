package by.epamlab.inttraining.beans.weapons;


import by.epamlab.inttraining.beans.parts.Belt;
import by.epamlab.inttraining.beans.parts.Magazine;
import by.epamlab.inttraining.beans.parts.Optic;

import org.apache.log4j.Logger;
/**
 * Class Shotgun describes shotgun model of weapon
 * - extends – Weapon.
 * @author – Stas Nesteruk.
 */
public class ShotgunWeapon extends Weapon{
    /**
     * Global logger
     */
    private static final Logger LOGGER = Logger.getLogger(ShotgunWeapon.class.getName());
    /**
     * Field contains information about optic for shotgun
     */
    private Optic optic;
    /**
     * Field contains information about belt for weapon
     */
    private Belt belt;

    public ShotgunWeapon(String model,
                         String caliber,
                         int weightWithAmmo,
                         int weightWithOutAmmo,
                         int combatRateOfFireSingleShooting,
                         int combatRateOfFireAutoShooting,
                         Magazine magazine,
                         Optic optic,
                         Belt belt) {
        super(model,
              caliber,
              weightWithAmmo,
              weightWithOutAmmo,
              combatRateOfFireSingleShooting,
              combatRateOfFireAutoShooting,
              magazine);
        this.optic = optic;
        this.belt = belt;
    }

    /**
     * Retrieve optic for weapon.
     * @return A Optic data type.
     */
    public Optic getOptic() {
        return optic;
    }

    /**
     * Set optic for weapon.
     * @param optic A variable of type Optic.
     */
    public void setOptic(Optic optic) {
        this.optic = optic;
    }

    /**
     * Retrieve belt for weapon.
     * @return A Belt data type.
     */
    public Belt getBelt() {
        return belt;
    }

    /**
     * Set belt for weapon.
     * @param belt A variable of type Belt.
     */
    public void setBelt(Belt belt) {
        this.belt = belt;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void singleShooting() {
        LOGGER.info("Fire mod: single!");
        LOGGER.info(getModel() + " is shoting..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void autoShooting() {
        LOGGER.info("Fire mod: auto!");
        LOGGER.info(getModel() + " is shoting..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load() {
        LOGGER.info(getModel() + " is loading..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        LOGGER.info(getModel() + " is reload..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void defuse() {
        LOGGER.info(getModel() + " is defused..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dismantle() {
        LOGGER.info("Algirithm dismantle " + getModel() + "..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void assembly() {
        LOGGER.info("Algirithm assembly " + getModel() + "..");
    }

    /**
     * Returns a string representation of the shotgun weapon object.
     * @return A String data type.
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append("Equipment shotgun: ");
        sb.append(getMagazine());
        sb.append(optic);
        sb.append(belt);
        return sb.toString();
    }
}
