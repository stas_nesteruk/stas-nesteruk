package by.epamlab.inttraining.beans.parts;
/**
 * Class Belt describes part of weapon - belt.
 * @author – Stas Nesteruk.
 */
public class Belt {
    /**
     * This field contains information about model of belt
     */
    private String model;
    /**
     * This field contains information about material of belt
     */
    private String material;

    /**
     *Constructor - create new belt object
     * @param model A variable of type String.
     * @param material A variable of type String.
     */
    public Belt(String model, String material) {
        this.model = model;
        this.material = material;
    }

    /**
     * Retrieve the value of belt model.
     * @return A String data type.
     */
    public String getModel() {
        return model;
    }

    /**
     * Set the model of belt.
     * @param model A variable of type String.
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Retrieve the value of belt material.
     * @return A String data type.
     */
    public String getMaterial() {
        return material;
    }

    /**
     * Set the material of belt.
     * @param material A variable of type String.
     */
    public void setMaterial(String material) {
        this.material = material;
    }

    /**
     * Returns a string representation of the object.
     * @return A String data type.
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Belt: ");
        sb.append("\nModel: ").append(model);
        sb.append(";\nMaterial: ").append(material);
        sb.append(";\n");
        return sb.toString();
    }
}
