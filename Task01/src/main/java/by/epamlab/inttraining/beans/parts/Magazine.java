package by.epamlab.inttraining.beans.parts;
/**
 * Class Magazine describes part of weapon - magazine.
 * @author – Stas Nesteruk.
 */
public class Magazine {
    /**
     * This field contains information about magazine weight without ammo
     */
    private int weightEmpty;
    /**
     * This field contains information about magazine weight with full ammo
     */
    private int weightFull;
    /**
     * This field contains information about magazine capasity
     */
    private int capasity;

    public Magazine(int weightEmpty, int weightFull, int capasity) {
        this.weightEmpty = weightEmpty;
        this.weightFull = weightFull;
        this.capasity = capasity;
    }

    /**
     * Retrieve the value of magazine weight without ammo.
     * @return A int data type.
     */
    public int getWeightEmpty() {
        return weightEmpty;
    }

    /**
     * Set the weight of empty magazine.
     * @param weightEmpty A variable of type int.
     */
    public void setWeightEmpty(int weightEmpty) {
        this.weightEmpty = weightEmpty;
    }

    /**
     * Retrieve the value of magazine weight with ammo.
     * @return A int data type.
     */
    public int getWeightFull() {
        return weightFull;
    }

    /**
     * Set the weight of full magazine.
     * @param weightFull A variable of type int.
     */
    public void setWeightFull(int weightFull) {
        this.weightFull = weightFull;
    }

    /**
     * Retrieve the value of magazine capasity.
     * @return A int data type.
     */
    public int getCapasity() {
        return capasity;
    }

    /**
     * Set the capasity of magazine.
     * @param capasity A variable of type int.
     */
    public void setCapasity(int capasity) {
        this.capasity = capasity;
    }

    /**
     * Returns a string representation of the object.
     * @return A String data type.
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Magazine: ");
        sb.append("\nWeight empty: ").append(weightEmpty);
        sb.append(";\nWeight full: ").append(weightFull);
        sb.append(";\nCapasity: ").append(capasity);
        sb.append(";\n");
        return sb.toString();
    }
}
