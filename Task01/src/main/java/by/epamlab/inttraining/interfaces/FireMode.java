package by.epamlab.inttraining.interfaces;
/**
 * Interface FireMode describes fire mode for weapon.
 * @author – Stas Nesteruk.
 */
public interface FireMode {
    /**
     * This method describes single shoting mode
     */
    void singleShooting();
    /**
     * This method describes auto shoting mode
     */
    void autoShooting();
}
