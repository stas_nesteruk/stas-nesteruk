package by.epamlab.inttraining.utils;

import by.epamlab.inttraining.beans.parts.Belt;
import by.epamlab.inttraining.beans.parts.Optic;
import by.epamlab.inttraining.beans.weapons.HandgunWeapon;
import by.epamlab.inttraining.beans.weapons.MachinegunWeapon;
import by.epamlab.inttraining.beans.parts.Magazine;
import by.epamlab.inttraining.beans.weapons.ShotgunWeapon;
import by.epamlab.inttraining.beans.weapons.Weapon;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Constants Utility class describes constants
 * @author – Stas Nesteruk.
 */
public final class Constants {

    private Constants() {
        throw new IllegalStateException("Utility class");
    }

    public static final int SECOND_IN_MINUTE = 60;
    public static final int MILLISECOND_TO_SECOND = 1000;

    private static final Optic OPTIC_CB_99 = new Optic("Base","3,5х17,5П");

    private static final Belt BELT_AK_105 = new Belt("Standart", "Synthetics");
    private static final Belt BELT_M_16 = new Belt("Standart","Synthetics");
    private static final Belt BELT_CB_99 = new Belt("Standart", "Synthetics");

    private static final Magazine MAGAZINE_CB_99 = new Magazine(150, 300,10);
    private static final Magazine MAGAZINE_M_16 = new Magazine(113, 282, 30);
    private static final Magazine MAGAZINE_AK_105 = new Magazine(230, 536, 30);
    private static final Magazine MAGAZINE_PP_19 = new Magazine(150,400, 30);

    private static final Weapon CB_99 = new ShotgunWeapon(
            "CB-99",
            "5,6",
            4500,
            4200,
            15,
            35,
            MAGAZINE_CB_99,
            OPTIC_CB_99,
            BELT_CB_99);
    private static final Weapon PP_19 = new HandgunWeapon(
            "PP-19",
            "9",
            2900,
            2500,
            60,
            135,
            MAGAZINE_PP_19);
    private static final Weapon AK_105 = new MachinegunWeapon(
            "AK-105",
            "5,45х39",
            3500,
            3200,
            40,
            100,
            600,
            MAGAZINE_AK_105,
            BELT_AK_105);
    private static final Weapon M_16 = new MachinegunWeapon(
            "M-16",
            "5,56×45",
            3330,
            2880,
            50,
            120,
            650,
            MAGAZINE_M_16,
            BELT_M_16);

    public static final List<Weapon> WEAPON_LIST = new ArrayList<>();
    public static final List<String> FIRST_SECTION_MENU = new ArrayList<>();
    public static final List<String> SECOND_SECTION_MENU = new ArrayList<>();
    public static final List<String> THIRD_SECTION_MENU = new ArrayList<>();

    static{
        WEAPON_LIST.add(AK_105);
        WEAPON_LIST.add(M_16);
        WEAPON_LIST.add(PP_19);
        WEAPON_LIST.add(CB_99);

        FIRST_SECTION_MENU.add("Select a weapon from the list");
        int count = 1;
        for(Weapon weapon : WEAPON_LIST) {
            FIRST_SECTION_MENU.add("\n" + count + ". " + weapon.getModel());
            count++;
        }

        SECOND_SECTION_MENU.add("Select an option from the list");
        SECOND_SECTION_MENU.add("\n1. Shoot");
        SECOND_SECTION_MENU.add("\n2. Algirithm dismantle");
        SECOND_SECTION_MENU.add("\n3. Algorithm assembly");
        SECOND_SECTION_MENU.add("\n4. Information about weapon");
        SECOND_SECTION_MENU.add("\n5. Exit");

        THIRD_SECTION_MENU.add("Select fire mode");
        THIRD_SECTION_MENU.add("\n1. Single");
        THIRD_SECTION_MENU.add("\n2. Automatic");
        THIRD_SECTION_MENU.add("\n3. Exit");
    }

}
