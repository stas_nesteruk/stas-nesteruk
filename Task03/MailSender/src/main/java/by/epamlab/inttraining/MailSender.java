package by.epamlab.inttraining;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Mojo(name = "mailsender")
public class MailSender extends AbstractMojo {
    @Parameter(property = "emailLogin")
    private String emailLogin;
    @Parameter(property = "emailPassword")
    private String emailPassword;
    @Parameter(property = "toEmail")
    private String toEmail;
    @Parameter(property = "subject")
    private String subject;
    @Parameter(property = "text")
    private String text;
    @Parameter(property = "smtpPort")
    private String smtpPort;
    @Parameter(property = "smtpHost")
    private String smtpHost;

    public void execute() throws MojoExecutionException {
        Properties properties = System.getProperties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", smtpHost);
        properties.put("mail.smtp.port", smtpPort);

        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailLogin, emailPassword);
            }
        });
        getLog().info("Sending email...");
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailLogin));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
            getLog().info("Email sent successfully!");
        }catch (MessagingException ex){
            getLog().error("Sending failed", ex);
            throw new MojoExecutionException(ex.getMessage());
        }
    }
}
