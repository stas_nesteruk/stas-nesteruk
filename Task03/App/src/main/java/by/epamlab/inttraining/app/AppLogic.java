package by.epamlab.inttraining.app;

import by.epamlab.inttraining.app.beans.weapons.Weapon;
import by.epamlab.inttraining.app.utils.Constants;


import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
/**
 * Class AppLogic Utility class describes logic of the program
 * @author – Stas Nesteruk.
 */
public final class AppLogic {
    /**
     * Global logger
     */
    private static final Logger LOGGER = Logger.getLogger(AppLogic.class);
    /**
     * Global Scanner
     */
    private static Scanner scanner = new Scanner(System.in);

    private AppLogic() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * This main method describes logic of the program
     * implemented a primitive menu
     */
    public static void execute(){
        int selectedWeapon  = selectSectionMenu(Constants.FIRST_SECTION_MENU);
        Weapon currentWeapon = Constants.WEAPON_LIST.get(selectedWeapon-1);
        int action;
        long shootingTime;
        do {
            action = selectSectionMenu(Constants.SECOND_SECTION_MENU);
            switch (action) {
                case 1:
                    currentWeapon.load();
                    int fireMod = selectSectionMenu(Constants.THIRD_SECTION_MENU);
                    switch (fireMod) {
                        case 1:
                            currentWeapon.singleShooting();
                            LOGGER.info("0. Stop shooting.");
                            shootingTime = currentWeapon.shootingImitation(scanner);
                            LOGGER.info(currentWeapon.shootingInforamition(shootingTime, fireMod));
                            break;
                        case 2:
                            currentWeapon.autoShooting();
                            LOGGER.info("0. Stop shooting.");
                            shootingTime = currentWeapon.shootingImitation(scanner);
                            LOGGER.info(currentWeapon.shootingInforamition(shootingTime, fireMod));
                            break;
                        case 3:
                            System.exit(0);
                            break;
                        default:
                            LOGGER.error("Wrong action!!!");
                            break;
                    }
                    break;
                case 2:
                    currentWeapon.dismantle();
                    action = backToMenu();
                    break;
                case 3:
                    currentWeapon.assembly();
                    action = backToMenu();
                    break;
                case 4:
                    LOGGER.info(currentWeapon.toString());
                    action = backToMenu();
                    break;
                case 5:
                    System.exit(0);
                    break;
                default:
                    LOGGER.error("Wrong action!!!");
                    break;
            }
        }while(action!=5);
    }

    /**
     * This method  describes validation of entered data(menu section)
     * @param maxValue – maximum number of menu sections
     * @return – A int data type. return number of the selected menu section
     */
    private static int insertData(int maxValue){
        int insertData;
        while(true){
            try{
                insertData = scanner.nextInt();
                if(insertData < 1 || insertData > maxValue){
                    LOGGER.info("Select an option from the list!\n");
                    continue;
                }else{
                    break;
                }
            }catch (InputMismatchException ex){
                LOGGER.error("Try again!");
                scanner.nextLine();
            }
        }
        return insertData;
    }

    /**
     * This utility method shows section menu
     *
     * @return – A int data type. number of selected section
     */
    private static int selectSectionMenu(List<String> menuDescription){
        LOGGER.info(menuDescription);
        return insertData(menuDescription.size() - 1);
    }

    /**
     * This utility method return to menu or exit
     *
     * @return – A int data type. number of selected section
     */
    private static int backToMenu(){
        int action;
        LOGGER.info("1. Back");
        LOGGER.info("2. Exit");
        int menuAction = insertData( 2);
        if(menuAction == 1 ){
            action = 1;
        }else{
            action = 5;
        }
        return action;
    }
}
