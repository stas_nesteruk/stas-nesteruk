package by.epamlab.inttraining.app.beans.weapons;
/**
 * Interface GunAction describes action for weapon.
 * @author – Stas Nesteruk.
 */
public interface GunAction {
    /**
     * This method describes loading weapon
     */
    void load();
    /**
     * This method describes reloading weapon
     */
    void reload();
    /**
     * This method describes defuse weapon
     */
    void defuse();
}
