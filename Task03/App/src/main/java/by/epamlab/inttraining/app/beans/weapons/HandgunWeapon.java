package by.epamlab.inttraining.app.beans.weapons;

import by.epamlab.inttraining.app.beans.parts.Magazine;

import org.apache.log4j.Logger;
/**
 * Class HandgunWeapon describes handgun model of weapon
 * - extends – Weapon.
 * @author – Stas Nesteruk.
 */
public class HandgunWeapon extends Weapon{
    /**
     * Global logger
     */
    private static final Logger LOGGER = Logger.getLogger(HandgunWeapon.class);

    public HandgunWeapon(String model,
                         String caliber,
                         int weightWithAmmo,
                         int weightWithOutAmmo,
                         int combatRateOfFireSingleShooting,
                         int combatRateOfFireAutoShooting,
                         Magazine magazine) {
        super(model,
              caliber,
              weightWithAmmo,
              weightWithOutAmmo,
              combatRateOfFireSingleShooting,
              combatRateOfFireAutoShooting,
              magazine);
    }

    /**
    * {@inheritDoc}
    */
    @Override
    public void singleShooting() {
        LOGGER.info("Fire mod: single!");
        LOGGER.info(getModel() + " is shoting..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void autoShooting() {
        LOGGER.info( "Fire mod: auto!");
        LOGGER.info(getModel() + " is shoting..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load() {
        LOGGER.info(getModel() + " is load..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        LOGGER.info(getModel() + " is reload");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void defuse() {
        LOGGER.info(getModel() + " is defused..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dismantle() {
        LOGGER.info("Algirithm dismantle " + getModel() + "..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void assembly() {
        LOGGER.info("Algirithm assembly " + getModel() + "..");
    }

    /**
     * Returns a string representation of the handgun weapon object.
     * @return A String data type.
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append("Equipment handgun: ");
        sb.append(getMagazine());
        return sb.toString();
    }
}
