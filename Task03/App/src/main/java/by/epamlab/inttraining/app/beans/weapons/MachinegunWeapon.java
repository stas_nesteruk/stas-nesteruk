package by.epamlab.inttraining.app.beans.weapons;

import by.epamlab.inttraining.app.beans.parts.Belt;
import by.epamlab.inttraining.app.beans.parts.Magazine;

import org.apache.log4j.Logger;
/**
 * Class MachinegunWeapon describes machinegun model of weapon
 * - extends – Weapon.
 * @author – Stas Nesteruk.
 */
public class MachinegunWeapon extends Weapon{
    /**
     * Global logger
     */
    private static final Logger LOGGER = Logger.getLogger(MachinegunWeapon.class);
    /**
     * Field contains information about rape of fire
     */
    private int rateOfFire;
    /**
     * Field contains information about belt for weapon
     */
    private Belt belt;

    public MachinegunWeapon(String model,
                            String caliber,
                            int weigthWithAmmo,
                            int weigthWithOutAmmo,
                            int combatRateOfFireSingleShooting,
                            int combatRateOfFireAutoShooting,
                            int rateOfFire,
                            Magazine magazine,
                            Belt belt) {
        super(model,
              caliber,
              weigthWithAmmo,
              weigthWithOutAmmo,
              combatRateOfFireSingleShooting,
              combatRateOfFireAutoShooting,
              magazine);
        this.rateOfFire = rateOfFire;
        this.belt = belt;
    }

    /**
     * Retrieve rate of fire for machinegun.
     * @return A int data type.
     */
    public int getRateOfFire() {
        return rateOfFire;
    }

    /**
     * Set rate of fire for machinegun.
     * @param rateOfFire A variable of type int.
     */
    public void setRateOfFire(int rateOfFire) {
        this.rateOfFire = rateOfFire;
    }

    /**
     * Retrieve the belt of weapon.
     * @return A Belt data type.
     */
    public Belt getBelt() {
        return belt;
    }

    /**
     * Set the belt for weapon.
     * @param belt A variable of type Belt.
     */
    public void setBelt(Belt belt) {
        this.belt = belt;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void singleShooting() {
        LOGGER.info("Fire mod: single!");
        LOGGER.info(getModel() + " is shooting..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void autoShooting() {
        LOGGER.info("Fire mod: automatic!");
        LOGGER.info(getModel() + " is shooting..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load() {
        LOGGER.info(getModel() + " is loaded..");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        LOGGER.info(getModel() + " is reloaded");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void defuse() {
        LOGGER.info(getModel() + " is defused");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dismantle() {
        LOGGER.info("Algirithm dismantle " + getModel() + "...");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void assembly() {
        LOGGER.info("Algirithm assembly " + getModel() + "...");
    }

    /**
     * Returns a string representation of the machinegun weapon object.
     * @return A String data type.
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(super.toString());
        sb.append("Rate of fire: ").append(rateOfFire).append(";\n");
        sb.append("Equipment machinegun:\n");
        sb.append(getMagazine());
        sb.append(belt);
        return sb.toString();
    }
}
