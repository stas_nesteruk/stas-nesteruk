package by.epamlab.inttraining.app.beans.weapons;

import by.epamlab.inttraining.app.beans.parts.Magazine;
import by.epamlab.inttraining.app.utils.Constants;

import java.util.Scanner;
/**
 * Class Weapon describes abstract model of weapon.
 * - implements – GunAction, FireMode.
 * @author – Stas Nesteruk.
 */
public abstract class Weapon implements GunAction, FireMode {
    /**
     * Field contains information about model of weapon
     */
    private String model;
    /**
     * Field contains information about caliber of weapon
     */
    private String caliber;
    /**
     * Field contains information about weight of weapon with ammo
     */
    private int weightWithAmmo;
    /**
     * Field contains information about weight of weapon without ammo
     */
    private int weightWithOutAmmo;
    /**
     * Field contains information about combat rate of fire(single shooting)
     */
    private int combatRateOfFireSingleShooting;
    /**
     * Field contains information about combat rate of fire(automatic shooting)
     */
    private int combatRateOfFireAutoShooting;
    /**
     * Field contains information about magazine for weapon
     */
    private Magazine magazine;

    Weapon(String model,
                  String caliber,
                  int weightWithAmmo,
                  int weightWithOutAmmo,
                  int combatRateOfFireSingleShooting,
                  int combatRateOfFireAutoShooting,
                  Magazine magazine) {
        this.model = model;
        this.caliber = caliber;
        this.weightWithAmmo = weightWithAmmo;
        this.weightWithOutAmmo = weightWithOutAmmo;
        this.combatRateOfFireSingleShooting = combatRateOfFireSingleShooting;
        this.combatRateOfFireAutoShooting = combatRateOfFireAutoShooting;
        this.magazine = magazine;
    }

    /**
     * Retrieve the model name of weapon
     * @return A String data type.
     */
    public String getModel() {
        return model;
    }

    /**
     * Set the model name of weapon.
     * @param model A variable of type String.
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Retrieve the caliber of weapon
     * @return A String data type.
     */
    public String getCaliber() {
        return caliber;
    }

    /**
     * Set the caliber of weapon.
     * @param caliber A variable of type String.
     */
    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    /**
     * Retrieve the combat rate of fire(single shooting)
     * @return A int data type.
     */
    public int getCombatRateOfFireSingleShooting() {
        return combatRateOfFireSingleShooting;
    }

    /**
     * Set the combat rate of fire(single shooting).
     * @param combatRateOfFireSingleShooting A variable of type int.
     */
    public void setCombatRateOfFireSingleShooting(int combatRateOfFireSingleShooting) {
        this.combatRateOfFireSingleShooting = combatRateOfFireSingleShooting;
    }

    /**
     * Retrieve the combat rate of fire(automatic shooting)
     * @return A int data type.
     */
    public int getCombatRateOfFireAutoShooting() {
        return combatRateOfFireAutoShooting;
    }

    /**
     * Set the combat rate of fire(automatic shooting).
     * @param combatRateOfFireAutoShooting A variable of type int.
     */
    public void setCombatRateOfFireAutoShooting(int combatRateOfFireAutoShooting) {
        this.combatRateOfFireAutoShooting = combatRateOfFireAutoShooting;
    }

    /**
     * Retrieve the weight of magazine with ammo.
     * @return A int data type.
     */
    public int getWeightWithAmmo() {
        return weightWithAmmo;
    }

    /**
     * Set the weight of magazine with ammo.
     * @param weightWithAmmo A variable of type int.
     */
    public void setWeightWithAmmo(int weightWithAmmo) {
        this.weightWithAmmo = weightWithAmmo;
    }

    /**
     * Retrieve the weight of magazine without ammo.
     * @return A int data type.
     */
    public int getWeightWithOutAmmo() {
        return weightWithOutAmmo;
    }

    /**
     * Set the weight of magazine without ammo.
     * @param weightWithOutAmmo A variable of type int.
     */
    public void setWeightWithOutAmmo(int weightWithOutAmmo) {
        this.weightWithOutAmmo = weightWithOutAmmo;
    }

    /**
     * Retrieve the magazine of weapon.
     * @return A Magazine data type.
     */
    Magazine getMagazine() {
        return magazine;
    }

    /**
     * Set the magazine for weapon.
     * @param magazine A variable of type Magazine.
     */
    public void setMagazine(Magazine magazine) {
        this.magazine = magazine;
    }

    /**
     * This method describes shooting imitation for weapon
     * @param scanner A variable of type Scanner. Entering 0 stops shooting.
     * @return A Long data type. Time of shooting in seconds
     */
    public long shootingImitation(Scanner scanner){
        long startTime = System.currentTimeMillis();
        while (true){
            if(scanner.nextInt() == 0){
                break;
            }
        }
        long stopTime = System.currentTimeMillis();
        return (stopTime-startTime) / Constants.MILLISECOND_TO_SECOND;
    }

    /**
     * This method return information about shooting
     * @param shootingTime A variable of type long, contains time of shooting
     * @param fireMod A variable of type int, contains fire mod
     * @return A String data type.
     */
    public String shootingInforamition(long shootingTime, int fireMod) {
        int shotsFired;
        if(fireMod == 1){
            shotsFired = (int)(shootingTime * combatRateOfFireSingleShooting) / Constants.SECOND_IN_MINUTE;
        }else{
            shotsFired = (int)(shootingTime * combatRateOfFireAutoShooting) / Constants.SECOND_IN_MINUTE;
        }
        final StringBuilder sb = new StringBuilder("Shoting information:").append("\n");
        sb.append("Shooting time: ").append(shootingTime).append("\n");
        sb.append("Shots fired: ").append(shotsFired).append("\n");
        if(shotsFired < magazine.getCapasity()){
            sb.append("Bullets left: ").append(magazine.getCapasity() - shotsFired).append("\n");
        }else{
            int countReload = shotsFired / magazine.getCapasity();
            int bulletsLeft = shotsFired-countReload * magazine.getCapasity();
            sb.append("Bullets left: ").append(bulletsLeft).append("\n");
            sb.append("Count of reload: ").append(countReload).append("\n");
        }
        return sb.toString();
    }

    /**
     * This abstract method describes dismantle algorithm for weapon
     */
    public abstract void dismantle();

    /**
     * This abstract method describes assembly algorithm for weapon
     */
    public abstract void assembly();

    /**
     * Returns a string representation of the weapon object.
     * @return A String data type.
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Model: ").append(model).append(";\n");
        sb.append("Caliber: ").append(caliber).append(";\n");
        sb.append("Weight with cartridges: ").append(weightWithAmmo).append(";\n");
        sb.append("Weight without cartridges: ").append(weightWithOutAmmo).append(":\n");
        sb.append("Combat rate of fire(Single shooting): ");
        sb.append(combatRateOfFireSingleShooting).append(";\n");
        sb.append("Combat rate of fire(Auto shooting): ");
        sb.append(combatRateOfFireAutoShooting).append(";\n");
        return sb.toString();
    }

}