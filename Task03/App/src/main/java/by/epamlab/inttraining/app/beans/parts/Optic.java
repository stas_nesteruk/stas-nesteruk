package by.epamlab.inttraining.app.beans.parts;
/**
 * Class Optic describes part of weapon - optic.
 * @author – Stas Nesteruk.
 */
public class Optic {
    /**
     * This field contains information about model of optic
     */
    private String model;
    /**
     * This field contains information about increase of optic
     */
    private String increase;

    public Optic(String model, String increase) {
        this.model = model;
        this.increase = increase;
    }

    /**
     * Get the value of optic model.
     * @return A String data type.
     */
    public String getModel() {
        return model;
    }

    /**
     * Set the model of optic.
     * @param model A variable of type String.
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Get the value of optic increase.
     * @return A String data type.
     */
    public String getIncrease() {
        return increase;
    }

    /**
     * Set the increase of optic.
     * @param increase A variable of type String.
     */
    public void setIncrease(String increase) {
        this.increase = increase;
    }

    /**
     * Returns a string representation of the object.
     * @return A String data type.
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Optic: ");
        sb.append("\nModel: ").append(model);
        sb.append(";\nIncrease ").append(increase);
        sb.append(";\n");
        return sb.toString();
    }
}
