package by.epamlab.inttraining;

import by.epamlab.inttraining.app.AppLogic;
/**
 * Class Runner running app
 * @author – Stas Nesteruk.
 */
public class Runner {

    public static void main(String[] args) {
        AppLogic.execute();
    }
}
